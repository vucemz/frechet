#pragma once

#include <LEDA/core/list.h>
using leda::list;

namespace parametric_search {
	template<class R>
	class function
	{
	public:
		virtual list<R> roots(function<R>* p) =0;
		virtual int compare(function<R>* p, R low, R high) =0;
	};

}