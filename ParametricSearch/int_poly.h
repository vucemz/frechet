#pragma once
#include "function.h"
#include "decision_problem.h"

#include <LEDA\numbers\real.h>

using leda::real;

using parametric_search::function;
using parametric_search::decision_problem;

class int_poly : public function<real>{
public:
	real k;
	real h;	//y = kx+h

	int_poly() {
	}

	int_poly(real k, real h) { 
		this->k = k;
		this->h = h;
	}

	real eval(real x) {
		return k*x+h;
	}

	int compare(function<real>* i, real low, real high) {
		int_poly* p = static_cast<int_poly*>(i);

		real mid = low + (high-low)/2;

		real v1 = this->eval(mid);
		real v2 = p->eval(mid);

		if (v1 > v2) return 1;
		if (v1 < v2) return -1;
		return 0;
	}

	//k1*x+h1 = k2*x+h2
	list<real> roots(function<real>* v)  {
		int_poly* p = static_cast<int_poly*>(v);

		real h1 = h; real k1 = k;
		real h2 = p->h; real k2 = p->k;

		real intrs = (-h1+h2)/(k1-k2);
		
		list<real> lst; lst.append(intrs);
		return lst;
	}
};

class int_poly_dec : public decision_problem<real> {
private:
	list<int_poly*> lst;
public:
	int_poly_dec(list<int_poly*> lst) {
		this->lst = lst;
	}

	int solve(real& eps) {
		list<real> val;

		int_poly* p;
		forall(p, lst) {
			val.append(p->eval(eps));
		}

		val.sort();
		real res = val.contents(val[val.size()/2]);
		real border = 0;
		return (res > border) ? 1 : (res < border) ? -1 : 0;
	}
};