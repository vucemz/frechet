#pragma once
#include <iostream>
using std::cout;

#include "test.h";

class c2 : public c {
public:
	void oi(c* a) ;
	void oi(c1* a);
	void oi(c2* a);

	void oi_(c1* a);
	void oi_(c2* a);
};