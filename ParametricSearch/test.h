#pragma once
#include <iostream>
using std::cout;


class c1;
class c2;

class c {
public:
	virtual void oi(c* a) =0;

	virtual void oi_(c1* a) =0;
	virtual void oi_(c2* a) =0;
};


class c1 : public c {
public:
	void oi(c* a);

	void oi(c1* a);
	void oi(c2* a);

	void oi_(c1* a);
	void oi_(c2* a);
};