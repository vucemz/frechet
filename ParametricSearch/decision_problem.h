#ifndef DECISION_PROBLEM_H_INCLUDED
#define DECISION_PROBLEM_H_INCLUDED

namespace parametric_search {
	template<class R>
	class decision_problem {
	public:
		virtual int solve(R& eps) =0;
	};
}
#endif