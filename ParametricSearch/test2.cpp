#pragma once
#include "test2.h"

	void c2::oi(c* a) {
		a->oi_(this);
	}

	void c2::oi(c1* a) {
		cout << "c2 with offset of c1\n";
	}

	void c2::oi(c2* a) {
		cout << "c2 with offset of c2\n";
	}

	void c2::oi_(c1* a) {
		a->oi(this);
	}

	void c2::oi_(c2* a) {
		a->oi(this);
	}