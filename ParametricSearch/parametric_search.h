#ifndef QUICKSORT_H_INCLUDED
#define QUICKSORT_H_INCLUDED

#include "function.h"
#include "decision_problem.h"

#include <LEDA/core/list.h>
#include <LEDA/core/tuple.h>
#include <LEDA/core/dictionary.h>

using leda::list;
using leda::two_tuple;
using leda::dictionary;

namespace parametric_search {
	template<class R>
	class quicksort
	{
	private:
		dictionary<R, int> cached_decisions;
		
		bool find_root(list<R>& roots, decision_problem<R>& dec, two_tuple<R, R>& res) {
			cached_decisions.clear();
			// convert to array for speed
			R* roots_arr = to_array(roots);
			bool r = find_root(roots_arr, dec, res, 0, roots.size()-1);
			delete[] roots_arr;

			return r;
		}

		// finds consecutive roots where the decision problem result changes using quick_select algorithm
		// O(n) for median finding, O((log n)*dec(n)) for running the decision problem
		bool find_root(R roots[], decision_problem<R>& dec, two_tuple<R, R>& res, int l, int h) {
			while (h - l > 1) {
				int m = l + (h-l)/2;
				int m_ = quick_select(roots, l, h, m+1);
				R median = roots[m_];
		
				//int val = dec.solve(median);
				leda::dic_item it = cached_decisions.lookup(median);
				int val = (it) ? cached_decisions[it] : dec.solve(median);
				if (!it) cached_decisions.insert(median, val);

				if (it) cout << "\nGOT CACHED DECISION!\n";
				

				if (val == 0) {
					res = two_tuple<R,R>(median, median);
					return true;
				}
				else if (val == 1) h = m;
				else if (val == -1) l = m;
			}

			R root_1 = roots[l];
			R root_2 = roots[h];
			res = two_tuple<R,R>(root_1, root_2);
			return true;
		}

		int partition(R lst[], int left, int right, int pivot_index) {
			R pivot_value = lst[pivot_index];
			R tmp = lst[pivot_index]; lst[pivot_index] = lst[right]; lst[right] = tmp;
			int store_index = left;

			for (int i = left; i < right; i++) {
				if (lst[i] < pivot_value) {
					R tmp = lst[store_index]; lst[store_index] = lst[i]; lst[i] = tmp;
					store_index++;
				}
			}

			tmp = lst[right]; lst[right] = lst[store_index]; lst[store_index] = tmp;
			return store_index;
		}

		// selects k-th lowest element, partitions k-1 elements smaller thank k-th to it's left
		int quick_select(R lst[], int left, int right, int k) {
			if (left >= right) return left;
			int pivot_index = (rand()%(right - left)) + left;

			int pivot_new_index = partition(lst, left, right, pivot_index);

			if (pivot_new_index == k - 1) return pivot_new_index;
			else if (pivot_new_index > k - 1) return quick_select(lst, left, pivot_new_index - 1, k);
			else return quick_select(lst, pivot_new_index + 1, right, k);
		}

		R* to_array(list<R> lst) {
			R* arr = new R[lst.size()];
			for (int i = 0; i < lst.size(); i++) {
				arr[i] = lst.contents(lst[i]);
			}

			return arr;
		}


		list<R> collect_roots(list<function<R>*>& lst, decision_problem<R>& dec) {
			list<R> roots;
			int pivot_index = 0;
			function<R>* pivot = lst.contents(lst[pivot_index]);

			// collect roots of all pairs that can be compared in parallel
			for (int i = 0; i < lst.size(); i++) {
				if (i == pivot_index) continue;
				function<R>* elem = lst.contents(lst[i]);

				// filter roots outside of interval, O(n)
				list<R> collected = elem->roots(pivot);
				R r; forall(r, collected) if (r > opt_low && r < opt_high) roots.append(r);
			}
		
			return roots;
		}

		// partition the table
		void sort2(list<function<R>*>& lst, decision_problem<R>& dec, list<function<R>*>& less, list<function<R>*>& greater, function<R>* pivot) {
			int pivot_index = 0; // use the same pivot as with collecting roots
			pivot = lst.contents(lst[pivot_index]);

			for (int i = 0; i < lst.size(); i++) {
				if (i == pivot_index) continue;

				function<R>* elem = lst.contents(lst[i]);

				// make a comparison between functions at interval [opt_low, opt_high]
				if (elem->compare(pivot, opt_low, opt_high) < 0) less.append(elem);
				else greater.append(elem);
			}
		}

		list<function<R>*> sort(list<function<R>*>& lst, decision_problem<R>& dec) {
			// each process is identified with the list it is sorting
			list<list<function<R>*>> processes;
			processes.append(lst);

			bool run = true;
			list<function<R>*> process;
			while (run) { // loop should run O(log n) times
				// collect all roots from all processes at this stage
				list<R> roots;
				forall (process, processes) {
					if (process.size() > 1) {
						roots.conc(collect_roots(process, dec));
					}
				}

				// push min and max to ensure there exist such two consecutive roots r1, r2 where dec(r1) != dec(r2)
				roots.push_front(min); roots.push_back(max);

				// update interval
				two_tuple<R,R> res;
				// O(n + (log n)*dec(n)), where dec(n) is time complexity of the decision problem
				if (find_root(roots, dec, res)) {
					if (res.first() > opt_low) opt_low = res.first();
					if (res.second() < opt_high) opt_high = res.second();
				}

				list<list<function<R>*>> processes_n;
				// spawn new processes
				run = false;
				forall (process, processes) {
					list<function<R>*> less; list<function<R>*> greater; function<R>* pivot = 0;
					if (process.size() > 1) {
						run = true; // continue to run if any of the lists are still unsorted
						sort2(process, dec, less, greater, pivot);

						processes_n.append(less);
						list<function<R>*> pivot_l; pivot_l.append(pivot);
						processes_n.append(pivot_l);
						processes_n.append(greater);
					}
					else processes_n.append(process);	// retain process
				}

				processes = processes_n;
			}

			list<function<R>*> lst_joined; 

			forall (process, processes) lst_joined.conc(process);
			return lst_joined;
		}

		two_tuple<R,R> interval() {
			return two_tuple<R,R>(opt_low, opt_high);
		}

	private:
		R opt_low; R opt_high;
		R min; R max;

	public:
		quicksort(R l, R h) {
			this->opt_low=l;
			this->opt_high=h;
			this->min = l;
			this->max = h;
		}

		two_tuple<R, R> parametric_search(list<function<R>*>& lst, decision_problem<R>& dec) {
			list<function<R>*> res = sort(lst, dec); 
			return interval();
		}
	};
}
#endif