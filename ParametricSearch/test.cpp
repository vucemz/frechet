#pragma once
#include "test.h"
#include "test2.h"

	void c1::oi(c* a) {
		a->oi_(this);
	}

	void c1::oi(c1* a) {
		cout << "c1 with offset of c1\n";
	}

	void c1::oi(c2* a) {
		cout << "c1 with offset of c2\n";
	}

	void c1::oi_(c1* a) {
		a->oi(this);
	}

	void c1::oi_(c2* a) {
		a->oi(this);
	}