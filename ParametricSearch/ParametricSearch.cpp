// ParametricSearch.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include "int_poly.h"
#include "parametric_search.h"
#include <LEDA/core/list.h>
#include <LEDA\numbers\real.h>

#include "test.h";
#include "test2.h";

using leda::real;
using std::cout;
using parametric_search::function;
using parametric_search::quicksort;
using parametric_search::decision_problem;

int _tmain(int argc, _TCHAR* argv[])
{
	list<function<real>*> lst;
	list<int_poly*> lst2;

	int_poly* l1 = new int_poly(2,3); lst.append(l1); lst2.append(l1);
	int_poly* l4 = new int_poly(6,7); lst.append(l4); lst2.append(l4);
	int_poly* l2 = new int_poly(3,2); lst.append(l2); lst2.append(l2);
	int_poly* l3 = new int_poly(10,-4); lst.append(l3); lst2.append(l3);
	int_poly* l5 = new int_poly(8,4); lst.append(l5); lst2.append(l5);
	int_poly* l6 = new int_poly(16,7); lst.append(l6); lst2.append(l6);
	int_poly* l7 = new int_poly(42,-64); lst.append(l7); lst2.append(l7);
	int_poly* l8 = new int_poly(9,-3); lst.append(l8); lst2.append(l8);

	int_poly_dec dec = int_poly_dec(lst2);
	quicksort<real> qs = quicksort<real>(-100, 100);

	//qs.sort(lst, dec);
	two_tuple<real, real> res = qs.parametric_search(lst, dec);
	double d1 = res.first().to_double();
	double d2 = res.second().to_double();
	cout << "[" << d1 << ", " << d2 << "]\n";

	system("pause");
	return 0;
}

