#ifndef FREE_SPACE_CELL_H_INCLUDED
#define FREE_SPACE_CELL_H_INCLUDED

//=================================
// forward declared dependencies
class interval;
template<class curve> class curve_segment;
//class real_interval;
//class real_sp_interval;
//class real_tp_interval;

//=================================
// included dependencies
#include "stdafx.h"
#include <typeinfo>
#include <LEDA/core/tuple.h>
#include <LEDA/numbers/real.h>
#include <LEDA/system/stream.h >
#include <LEDA/core/string.h>
#include "transition_manager.h"
#include "curve_segment.h"

using leda::list;
using leda::real;
using leda::two_tuple;

typedef two_tuple<real, real> real_point;
typedef two_tuple<real, real_point> real_num_point;
namespace frechet {
enum side {
	UP,
	DOWN
};

enum point_type_ {
	X_POINT, X_INTERVAL,
	Y_POINT,
	REGULAR
};

struct point_type {};
struct point_type_x_point : point_type {
	real v;

	point_type_x_point(real v) {
		this->v = v;
	}
};
struct point_type_x_interval : point_type {
	real_tp_interval interval;

	point_type_x_interval(real_tp_interval interval) {
		this->interval = interval;
	}
};
struct point_type_top_intersection : point_type {
	real v;
};
struct point_type_botom_intersection : point_type {
	real v;
};
struct point_type_regular : point_type {
	real v;
};

struct chk_exception {};
struct unfollowable_interval : chk_exception {};
struct critical_error : chk_exception {};

template<class curve1, class curve2>
class free_space_cell {
private:
	template<class T> static list<T> remove_duplicates(list<T> lst);

public:
	curve_segment<curve1>* segment_x; curve2* segment_xc;
	curve_segment<curve2>* segment_y; curve1* segment_yc;
public: 
	bool lp1b; real lp1;
	bool lp2b; real lp2;
	bool bp1b; real bp1;
	bool bp2b; real bp2;

	template<class c1, class c2> list<interval> to_intervals(curve_segment<c1>* sx, list<real>& x, curve_segment<c2>* sy, real& y, real eps);
	template<class c1, class c2> list<interval> to_intervals(curve_segment<c1>* sx, real& x, curve_segment<c2>* sy, list<real>& y, real eps);
	template<class c1, class c2> list<interval> to_intervals(curve_segment<c1>* sx, list<real_interval*>& x, curve_segment<c2>* sy, real& y, real eps);
	template<class c1, class c2> list<interval> to_intervals(curve_segment<c1>* sx, real& x, curve_segment<c2>* sy, list<real_interval*>& y, real eps);
	
	template<class c1, class c2> interval to_interval(curve_segment<c1>* sx, real_tp_interval x, curve_segment<c2>* sy, real& y, real eps);
	template<class c1, class c2> interval to_interval(curve_segment<c1>* sx, real_sp_interval x, curve_segment<c2>* sy, real& y, real eps);
	template<class c1, class c2> interval to_interval(curve_segment<c1>* sx, real&x, curve_segment<c2>* sy, real_tp_interval y, real eps);
	template<class c1, class c2> interval to_interval(curve_segment<c1>* sx, real&x, curve_segment<c2>* sy, real_sp_interval y, real eps);

	template<class c1, class c2> list<interval> intersection_int(curve_segment<c1>* s1, real t, curve_segment<c2>* s2, real eps);
	template<class c1, class c2> list<interval> intersection_int(curve_segment<c1>* s1, curve_segment<c2>* s2, real eps);

	template<class c1, class c2> list<interval> intersection_int2(curve_segment<c1>* s1, real t, curve_segment<c2>* s2, real eps);
	
	template<class c1, class c2> list<two_tuple<real, list<interval>>> control_points2(curve_segment<c1>* seg_x, curve_segment<c2>* seg_y, real min_x, real max_x, real eps, bool positive_side = true, bool add_middle = true, bool for_intersection = true, bool debug = false);

	template<class c1, class c2> int check_intersection2(curve_segment<c1>* s1, curve_segment<c2>* s2, list<interval>& lst, real& start_point, list<two_tuple<real, list<interval>>>& other_points, int follow_index, two_tuple<real, side> intersection_details, bool& right_point,  real& extreme_point, real eps, bool check_for_intersection = true, int i = 0, bool debug = false);

	template<class c1, class c2> bool adjacent2(curve_segment<c1>* s1, curve_segment<c2>* s2, real p1, bool& r1e, real& r1, bool& r2e, real& r2, real eps, bool test_corner_reach = false);
	template<class c1, class c2> bool opposite2(curve_segment<c1>* s1, curve_segment<c2>* s2,real p1, bool& r1e, real& r1, bool& r2e, real& r2, real eps, bool test_corner_reach = false);
	
	bool left2top(real p1, bool& r1e, real& r1, bool& r2e, real& r2, real eps, bool test_corner_reach = false);
	bool left2right(real p1, bool& r1e, real& r1, bool& r2e, real& r2, real eps, bool test_corner_reach = false);
	bool bottom2right(real p1, bool& r1e, real& r1, bool& r2e, real& r2, real eps, bool test_corner_reach = false);
	bool bottom2top(real p1, bool& r1e, real& r1, bool& r2e, real& r2, real eps, bool test_corner_reach = false);


public:
	free_space_cell<curve1, curve2>* right;
	free_space_cell<curve1, curve2>* up;


	free_space_cell();
	free_space_cell(curve2* x, curve1* y);
	~free_space_cell();

	bool solve_propagate(
		bool& t1b, real& t1, 
		bool& t2b, real& t2, 
		bool& r1b, real& r1,
		bool& r2b, real& r2, real eps);

	template<class T> list<T> lst_union(list<T>& lst1, list<T>& append);

	list<free_space_cell<curve1, curve2>*> right_hand_most();
	list<free_space_cell<curve1, curve2>*> top_hand_most();
};


template<class curve1, class curve2>
free_space_cell<curve1, curve2>::free_space_cell() {
	this->up = 0; this->right = 0;
	lp1b = lp2b = bp1b = bp2b = false;
}

template<class curve1, class curve2>
free_space_cell<curve1, curve2>::free_space_cell(curve2* x, curve1* y) {
	this->up = 0; this->right = 0;
	lp1b = lp2b = bp1b = bp2b = false;
	this->segment_x = x;
	this->segment_y = y;
	this->segment_xc = x;
	this->segment_yc = y;
}


template<class curve1, class curve2>
free_space_cell<curve1, curve2>::~free_space_cell() {
	if (this->right != 0) delete right;
}

template<class curve1, class curve2>
template<class c1, class c2>
list<interval> free_space_cell<curve1, curve2>::to_intervals(curve_segment<c1>* s1, list<real>& x, curve_segment<c2>* s2, real& y, real eps) {
	list<interval> lst;

	for (int i = 0; i < x.size() - 1; i++) {
		real i1 = x.contents(x[i]);
		real i2 = x.contents(x[i+1]);

		real_point rp = real_point(i1, i2);
		int tp = s1->inside(rp, y, s2, eps);

		/*lst.append(interval(tp, tp, rp));*/
		lst.append(interval(tp, rp));
	}

	return lst;
}

template<class curve1, class curve2>
template<class c1, class c2>
list<interval> free_space_cell<curve1, curve2>::to_intervals(curve_segment<c1>* s1, real&x, curve_segment<c2>* s2, list<real>& y, real eps) {
	list<interval> lst;

	for (int i = 0; i < y.size() - 1; i++) {
		real i1 = y.contents(y[i]);
		real i2 = y.contents(y[i+1]);

		real_point rp = real_point(i1, i2);
		int tp = s1->inside(x, rp, s2, eps);

		lst.append(interval(tp, tp, rp));
	}

	return lst;
}

template<class curve1, class curve2>
template<class c1, class c2>
list<interval> free_space_cell<curve1, curve2>::to_intervals(curve_segment<c1>* s1, list<real_interval*>& x, curve_segment<c2>* s2, real& y, real eps) {
	list<interval> lst;

	real_interval* i;
	forall (i, x) {
		if (typeid(*i) == typeid(real_sp_interval)) {
			lst.append(to_interval(s1, (static_cast<real_sp_interval&>(*i)), s2, y, eps));

		}
		else if (typeid(*i) == typeid(real_tp_interval)) {
			lst.append(to_interval(s1, (static_cast<real_tp_interval&>(*i)), s2, y, eps));
		}
	}

	return lst;
}

template<class curve1, class curve2>
template<class c1, class c2>
list<interval> free_space_cell<curve1, curve2>::to_intervals(curve_segment<c1>* s1, real& x, curve_segment<c2>* s2, list<real_interval*>& y, real eps) {
	list<interval> lst;

	real_interval* i;
	forall (i, y) {
		if (typeid(*i) == typeid(real_sp_interval)) {
			lst.append(to_interval(s1, x, s2, (static_cast<real_sp_interval&>(*i)), eps));
		}
		else if (typeid(*i) == typeid(real_tp_interval)) {
			lst.append(to_interval(s1, x, s2, (static_cast<real_tp_interval&>(*i)), eps));
		}
	}


	return lst;
}

template<class curve1, class curve2>
template<class c1, class c2>
interval free_space_cell<curve1, curve2>::to_interval(curve_segment<c1>* s1, real_tp_interval x, curve_segment<c2>* s2, real& y, real eps) {
	real value1 = x.v1;
	real value2 = x.v2;

	real_point rp = real_point(value1, value2);
	int tp = s1->inside(rp, y, s2, eps);
	return interval(tp, rp);
}

template<class curve1, class curve2>
template<class c1, class c2>
interval free_space_cell<curve1, curve2>::to_interval(curve_segment<c1>* s1, real& x, curve_segment<c2>* s2, real_tp_interval y, real eps) {
	real value1 = y.v1;
	real value2 = y.v2;

	real_point rp = real_point(value1, value2);
	int tp = s1->inside(x, rp, s2, eps);
	return interval(tp, rp);
}

template<class curve1, class curve2>
template<class c1, class c2>
interval free_space_cell<curve1, curve2>::to_interval(curve_segment<c1>* s1, real_sp_interval x, curve_segment<c2>* s2, real& y, real eps) {
	real value = x.v;
	return interval(3, real_point(value, value));
}

template<class curve1, class curve2>
template<class c1, class c2>
interval free_space_cell<curve1, curve2>::to_interval(curve_segment<c1>* s1, real& x, curve_segment<c2>* s2, real_sp_interval y, real eps) {
	real value = y.v;
	return interval(3, real_point(value, value));
}


//int sort_cmp(const real& a , const real& b) {
//	return (a-b).sign(20);
//	//return   (a.to_double() == b.to_double()) ? 0 : (a.to_double() > b.to_double()) ? 1 : -1;
//}

// x_intersection_int : segment_y, ty, segment_x
// y_intersection_int : segment_x, tx, segment_y
template<class curve1, class curve2>
template<class c1, class c2> 
list<interval> free_space_cell<curve1, curve2>::intersection_int2(curve_segment<c1>* s1, real t, curve_segment<c2>* s2, real eps) {
	list<real> intersections = s2->circle_intersection(s1->coordinates(t), eps);
	intersections.push_front(s2->min_t());
	intersections.push_back(s2->max_t());
	//intersections.sort();
	//TODO
	//intersections.sort(&sort_cmp);

	return to_intervals(s2, intersections, s1, t, eps);
}


// x_intersection_int : segment_y, ty, segment_x
// y_intersection_int : segment_x, tx, segment_y
template<class curve1, class curve2>
template<class c1, class c2> 
list<interval> free_space_cell<curve1, curve2>::intersection_int(curve_segment<c1>* s1, real t, curve_segment<c2>* s2, real eps) {
	list<real_interval*> inters = s2->circle_intersection_int(s1->coordinates(t), eps);
	list<interval> intervals = to_intervals(s2, inters, s1, t, eps);

	// clean up
	real_interval* p; forall (p, inters) delete p;
	return intervals;
}


template<class curve1, class curve2>
template<class c1, class c2> 
list<interval> free_space_cell<curve1, curve2>::intersection_int(curve_segment<c1>* s1, curve_segment<c2>* s2, real eps) {
	return intersection_int(s1, s1->min_t(), s2, eps);
}


int sort_cmp2(const two_tuple<real, list<interval>>& a , const two_tuple<real, list<interval>>& b) {
	return (a.first()-b.first()).sign();
}

void filter_top_bottom(list<real>& top, list<real>& bottom, list<real>& top_o,  list<real>& bottom_o,  list<real>& top_bottom_o) {
	real r;
	forall (r, top) {
		if (bottom.search(r) == 0) top_o.append(r);
		else top_bottom_o.append(r);
	}

	forall (r, bottom) {
		if (top_bottom_o.search(r) == 0) bottom_o.append(r);
	}
}


// to increase speed and keep comparisons down to a minimum provide intervals for points of extremes in x direction, otherwise provide empty list
template<class curve1, class curve2>
template<class c1, class c2> 
list<two_tuple<real, list<interval>>> free_space_cell<curve1, curve2>::control_points2(
																						curve_segment<c1>* seg_x, 
																						curve_segment<c2>* seg_y, 
																						real min_x, 
																						real max_x,
																						real eps,
																						bool add_middle = true, 
																						bool positive_side = true,
																						bool for_intersection = true,
																						bool debug = false) {
	list<two_tuple<real, list<interval>>> ret;
	list<interval> tmp;

	list<two_tuple<real, real_interval*>> ext_x = seg_x->offset_intersection3(static_cast<c1*>(seg_y), eps);
	list<two_tuple<real, real_interval*>> ext_y = seg_y->offset_intersection3(static_cast<c2*>(seg_x), eps);

	if (debug) {
		cout << "ext_x: " << "\n";
		two_tuple<real, real_interval*> xy; forall(xy, ext_x) {
			cout << xy.first().to_double() << ": ";
			real_interval* s = xy.second();
			if (typeid(*s) == typeid(real_sp_interval))  cout << (static_cast<real_sp_interval*>(s))->v.to_double() << "\n";
			else  cout << (static_cast<real_tp_interval*>(s))->v1.to_double() << " " << (static_cast<real_tp_interval*>(s))->v2.to_double()<<   "\n";
		}
		cout << "\n";

		cout << "ext_y: " << "\n";
		forall(xy, ext_y) {
			cout << xy.first().to_double() << ": ";
			real_interval* s = xy.second();
			if (typeid(*s) == typeid(real_sp_interval))  cout << (static_cast<real_sp_interval*>(s))->v.to_double() << "\n";
			else  cout << (static_cast<real_tp_interval*>(s))->v1.to_double() << " " << (static_cast<real_tp_interval*>(s))->v2.to_double()<<   "\n";
		}
		cout << "\n";
	}

	list<real> bottom = seg_x->circle_intersection(seg_y->coordinates(seg_y->min_t()), eps);
	list<real> top = seg_x->circle_intersection(seg_y->coordinates(seg_y->max_t()), eps);

	list<real> lst;

	two_tuple<real, real_interval*> p;
	
	forall (p, ext_x) {
		real t = p.first();
		
		if (lst.search(t) == 0 && t >= min_x && t <= max_x) {  
			lst.append(t);

			// point interval of type 3
			if (typeid(*(p.second())) == typeid(real_sp_interval)) {
				tmp.clear();
				real val = (static_cast<real_sp_interval*>(p.second()))->v;

				if (val == seg_y->min_t()) tmp.append(to_interval(seg_x, t, seg_y, real_sp_interval(val), eps));
				else tmp.append(to_interval(seg_x, t, seg_y, real_tp_interval(seg_y->min_t(), val), eps));

				tmp.append(interval(3, real_point(val, val)));

				if (val == seg_y->max_t()) tmp.append(to_interval(seg_x, t, seg_y, real_sp_interval(val), eps));
				else tmp.append(to_interval(seg_x, t, seg_y, real_tp_interval(val, seg_y->max_t()), eps));

				ret.append(two_tuple<real, list<interval>>(t, tmp));
			}
			// only a single interval of type 3 at that point, check if it is made of several parts
			else if (typeid(*(p.second())) == typeid(real_tp_interval)) {
				//TODO can remove to_intervals, as all intervals are of type 3
				tmp.clear();
				list<real> intersections;

				two_tuple<real, real_interval*> pt;
				forall (pt, ext_y) {
					real_interval* ri = pt.second();
					// point has to be on ray that goes through point t on x-axis
					if (typeid(*ri) == typeid(real_sp_interval) && t == (static_cast<real_sp_interval*>(ri))->v) {
						intersections.append(pt.first());
					}
					else if (typeid(*ri) == typeid(real_tp_interval) && t >= (static_cast<real_tp_interval*>(ri))->v1 && t <= (static_cast<real_tp_interval*>(ri))->v2) {
						intersections.append(pt.first());
					}
				}

				intersections.sort();
				list<real> intersections_ = remove_duplicates(intersections);
				intersections_.push_front(seg_y->min_t());
				intersections_.push_back(seg_y->max_t());
				tmp = to_intervals(seg_y, intersections_, seg_x, t, eps);

				ret.append(two_tuple<real, list<interval>>(t, tmp));
			}
		}
		delete p.second();	// clean up
	}

	tmp.clear();

	if (for_intersection) {
		forall (p, ext_y) {
			real_interval* rp = p.second();
			if (typeid(*rp) == typeid(real_sp_interval)) {
				if ((static_cast<real_sp_interval*>(rp))->v > min_x && (static_cast<real_sp_interval*>(rp))->v < max_x &&
					lst.search((static_cast<real_sp_interval*>(rp))->v) == 0) {
					lst.append( (static_cast<real_sp_interval*>(rp))->v );
					ret.append( two_tuple<real, list<interval>>((static_cast<real_sp_interval*>(rp))->v, tmp));
				}
			}
		}
	}
	else if (debug) cout << "profited from not adding " << ext_y.size() << " items!\n";

	forall (p, ext_y) delete p.second();	// clean up

	if (debug) {
		cout << "*************************************************************************\n";
		cout << "ALL0-2: ";
		two_tuple<real, list<interval>> r2;
		forall (r2, ret) cout << r2.first().to_double() << ": [" << r2.second() << "]\n";
		cout << "\n";
		real k; forall(k, lst) cout << k.to_double() << " "; cout <<  "\n"; 
	}

	real r; 
	list<real> top_filtered; list<real> top_bottom_filtered; list<real> bottom_filtered; 
	filter_top_bottom(top, bottom, top_filtered, bottom_filtered, top_bottom_filtered);
	
	// top and bottom intervals will be of type 3
	forall(r, top_bottom_filtered) {
		if (lst.search(r) == 0 && r > min_x && r < max_x) {
			list<real> ins = seg_y->circle_intersection(seg_x->coordinates(r), eps);
			list<interval> ins_int = to_intervals(seg_y, ins, seg_x, r, eps);

			ins_int.push_front(interval(3, real_point(seg_y->min_t(), seg_y->min_t())));
			ins_int.push_back(interval(3, real_point(seg_y->max_t(), seg_y->max_t())));

			lst.append( r );
			ret.append(two_tuple<real, list<interval>>(r, ins_int));
		}
	}
	// bottom interval will be of type 3
	forall(r, bottom_filtered) {
		if (lst.search(r) == 0 && r > min_x && r < max_x) {
			list<real> ins = seg_y->circle_intersection(seg_x->coordinates(r), eps);
			ins.push_back(seg_y->max_t());
			list<interval> ins_int = to_intervals(seg_y, ins, seg_x, r, eps);

			ins_int.push_front(interval(3, real_point(seg_y->min_t(), seg_y->min_t())));

			lst.append( r );
			ret.append(two_tuple<real, list<interval>>(r, ins_int));
		}
	}
	// top interval will be of type 3
	forall(r, top_filtered) {
		if (lst.search(r) == 0 && r > min_x && r < max_x) {
			list<real> ins = seg_y->circle_intersection(seg_x->coordinates(r), eps);
			ins.push_front(seg_y->min_t());
			list<interval> ins_int = to_intervals(seg_y, ins, seg_x, r, eps);

			ins_int.push_back(interval(3, real_point(seg_y->max_t(), seg_y->max_t())));

			lst.append( r );
			ret.append(two_tuple<real, list<interval>>(r, ins_int));
		}
	}

	if (debug) {
		cout << "*************************************************************************\n";
		cout << "ALL1-2: ";
		two_tuple<real, list<interval>> r2;
		forall (r2, ret) cout << r2.first().to_double() << ": [" << r2.second() << "]\n";
		cout << "\n";
		real k; forall(k, lst) cout << k.to_double() << " "; cout << "\n"; 
	}

	ret.sort(sort_cmp2); // all different, can sort quickly

	
	//ret.push_front(two_tuple<real, list<interval>>(min_x, tmp));
	//ret.push_back(two_tuple<real, list<interval>>(max_x, tmp));

	if (ret.empty() || ret.head().first() > min_x)
		ret.push_front(two_tuple<real, list<interval>>(min_x, tmp));
	if (ret.empty() || ret.back().first() < max_x)
		ret.push_back(two_tuple<real, list<interval>>(max_x, tmp));

	if (!positive_side) {
		ret.reverse();
	}


	if (add_middle) {
		list<two_tuple<real, list<interval>>> result2; result2.append(ret.head());

		for (int i = 1; i < ret.size(); i++) {
			real r1 = ret.contents(ret[i-1]).first();
			real r2 = ret.contents(ret[i]).first();

			//if (r1 != r2) {
			result2.append(two_tuple<real, list<interval>>((r1+r2)/2, tmp));
			//}

			result2.append(ret.contents(ret[i]));
		}
		
		ret = result2;
	}
	if (debug) {
		cout << "*************************************************************************\n";
		cout << "ALL2-2: ";
		two_tuple<real, list<interval>> r2;
		forall (r2, ret) cout << r2.first().to_double() << ": [" << r2.second() << "]\n";
		cout << "\n";
	}

	return ret;
}



template<class curve1, class curve2>
template<class T> 
list<T> free_space_cell<curve1, curve2>::remove_duplicates(list<T> lst) {
	if (lst.size() == 0) return lst;

	list<real> lstf;
	lstf.append(lst.pop());
	while (lst.size() > 0) {
		while (lst.size() > 0 && lst.front() == lstf.back()) lst.pop();
		if (lst.size() > 0) lstf.append(lst.pop());
	}

	return lstf;
}

template<class curve1, class curve2>
template<class c1, class c2> 
int free_space_cell<curve1, curve2>::check_intersection2(
													curve_segment<c1>* s1,
													curve_segment<c2>* s2,
													list<interval>& lst, 
													real& start_point, 
													list<two_tuple<real, list<interval>>>& other_points, 
													int follow_index, 
													two_tuple<real, side> intersection_details, 
													bool& extreme_point_exists,
													real& extreme_point, 
													real eps,
													bool check_for_intersection = true,
													int i = 0,
													bool debug = false
													) {

	int intersection_result = -1;

	if (i >= other_points.size()) {
		extreme_point_exists = false;
		return intersection_result;
	}

	if (lst.contents(lst[follow_index]).type == 3) throw unfollowable_interval();
	
	if (debug) cout << "==========================================================\n";
	two_tuple<real, list<interval>> k; forall(k, other_points) if (debug) cout << k.first().to_double() << " ";
	if (debug) cout << "\n";
	list<interval> intvt_p = lst;
	two_tuple<real, list<interval>> r = other_points.contents(other_points[i]);
	list<interval> intvt = (r.second().size() != 0) ? r.second() : intersection_int(s1, r.first(), s2, eps);

	list<int> result_ind;
	list<interval> result_type;

	if (debug) cout << "checking " << intvt_p << " -> " << intvt << "\n";
	if (debug) cout << " r : " << start_point.to_double() << " -> " << r.first().to_double() << "\n";

	if (!transition_manager::contains(intvt_p, intvt, follow_index, result_ind, result_type)) throw critical_error();	//TODO throw exception!
	
	if (debug) cout << intvt_p << " -> " << intvt << "\n";
	if (debug) cout << "results: " << result_ind << ", " << result_type << "\n";
	
	extreme_point_exists = true;

	if (result_ind.size() > 0) {
		//	interval we're following gets divided into one (or several) new intervals	
		real candidate; bool candidate_f = false;
		int id;
		//list<real> candidates; int id;
		forall (id, result_ind) {
			if (check_for_intersection) {
				if (intersection_details.second() == UP) {
					if ((intersection_result < 1 && intvt.contents(intvt[id]).limits.second() > intersection_details.first()) || 
						(id == intvt.size() -1 && intvt.contents(intvt[id]).limits.second() == intersection_details.first())) {	// if interval is the last one then there is full intersection
						intersection_result = 1; return intersection_result;
					}
					else if (intersection_result < 0 && intvt.contents(intvt[id]).limits.second() == intersection_details.first()) {
						intersection_result = 0;
					}
				}
				else if (intersection_details.second() == DOWN) {
					if ((intersection_result < 1 && intvt.contents(intvt[id]).limits.first() < intersection_details.first()) || 
						(id == 0 && intvt.contents(intvt[id]).limits.first() == intersection_details.first())) {				// if interval is the last one then there is full intersection
						intersection_result = 1; return intersection_result;
					}
					else if (intersection_result < 0 && intvt.contents(intvt[id]).limits.first() == intersection_details.first()) {
						intersection_result = 0;
					}
				}
			}

			// interval came to an end
			if (result_type.contents(result_type[id]).type == 3) {
				if (!candidate_f) {
					candidate = r.first();
					candidate_f = true;
				}
				else if (r.first() > candidate) candidate = r.first();

				continue;
			}
			else {
				// recursively try to find extreme point of interval at id
				bool rp_; real r_;
				int int_res = check_intersection2(s1, s2, result_type, r.first(), other_points, id, intersection_details, rp_, r_, eps, check_for_intersection, i+1, debug);
				if (int_res > intersection_result ) intersection_result = int_res;

				if (!rp_) extreme_point_exists = false;
				if (!candidate_f) {
					candidate = r_;
					candidate_f = true;
				}
				else if (r_ > candidate) candidate = r_;
			}
		}
		if (extreme_point_exists) {
			extreme_point = candidate;
		}
	}

	return intersection_result;
}

template<class curve1, class curve2>
template<class T>
list<T> free_space_cell<curve1, curve2>::lst_union(list<T>& lst1, list<T>& append) {
	list<T> lst = lst1;

	T i;
	forall (i, append) {
		if (lst1.search(i) == nil)
			lst.append(i);
	}

	return lst;
}


bool is_inside(real r, bool b1, real r1, bool b2, real r2) {
	if (b1 && r1 >= r) return true;
	if (b2 && r2 >= r) return true;
	return false;
}


template<class curve1, class curve2>
template<class c1, class c2> 
bool free_space_cell<curve1, curve2>::adjacent2(curve_segment<c1>* s1, curve_segment<c2>* s2, real p1, bool& r1e, real& r1, bool& r2e, real& r2, real eps, bool test_corner_reach) {
	two_tuple<real,side> intd = two_tuple<real, side>(p1, DOWN);
	bool epe; real ep; list<two_tuple<real, list<interval>>> cp;

part1:
	list<interval> sintervals = intersection_int(s1, s1->min_t(), s2, eps);
	int id = 0;
	for (id = 0; id < sintervals.size(); id++) {
		interval in = sintervals.contents(sintervals[id]);
		if (in.type == 2 && in.limits.first() >= p1) goto f2;
	}
f1:
	// no interval of type 2 upwards from p1
	r1e = true;
	r1 = s1->min_t();
	goto part2;

f2:	// interval of type 2 upwards from p1
	cp = control_points2(s1, s2, s1->min_t(), s1->max_t(), eps, true);
	
	int result = check_intersection2(s1, s2, sintervals, cp.pop().first(), cp, id, intd, epe, ep, eps);
	if (result == 1) {
		r1e = r2e = false; return false;
	}
	else if (epe) {
		r1e = true; r1 = ep;
	}
	else {
		r1e = r2e = false; return false;
	}

part2:
	list<interval> tintervals = intersection_int(s2, s2->max_t(), s1, eps);
	real min_acceptable = s1->max_t();
	if (test_corner_reach) {
		for (int i = tintervals.size() -1; i >= 0; i--) {
			interval in = tintervals.contents(tintervals[i]);
			if (in.type == 2) break;
			if (in.limits.first() < min_acceptable) min_acceptable = in.limits.first();
		}
	}

	for (id = 0; id < tintervals.size(); id++) {
		interval in = tintervals.contents(tintervals[id]);
		if (in.type == 2) goto s2;
	}
s1:
	// no interval of type 2
	r2e = false; return (!test_corner_reach) ? false : is_inside(min_acceptable, r1e, r1, r2e, r2); 
s2:
	real_point in_limits = tintervals.contents(tintervals[id]).limits;
	real middle = (in_limits.first() + in_limits.second()) / 2;
	//list<real> cp2 = control_points(s1, s2, s1->min_t(), middle, true);
	//cp2.reverse();	// go from right to left
	cp = control_points2(s1, s2, s1->min_t(), middle, eps, true, false); //reverse
	list<interval> mintervals = intersection_int(s1, middle, s2, eps);
	// check if first interval outside intd!
	if (mintervals.back().limits.first() < p1) {
		// intersection with p1 already occurs
		result = 1;
	}
	else result = check_intersection2(s1, s2, mintervals, cp.pop().first(), cp, mintervals.size()-1, intd, epe, ep, eps);	// following the top interval

	if (result == 1 || !epe) {
		// cannot get to the right side of the obstacle OR
		// same obstacle as in part1
		r2e = false;	
	}
	else {	// result != 1, epe = true
		// check intersection from middle to right
		cp = control_points2(s1, s2, middle, s1->max_t(), eps, true);
		//result = check_intersection2(s1, s2, mintervals, cp.pop().first(), cp, mintervals.size()-1, intd, epe, ep, eps);	// following the top interval
		result = check_intersection2(s1, s2, mintervals, cp.pop().first(), cp, mintervals.size()-1, intd, epe, ep, eps, false);	// following the top interval
		if (result == 1) {
			r2e = false; // cannot get to the right side of the obstacle
		}
		else if (epe) {
			r2e = true; r2 = ep;
		}
		else {
			r2e = false;	// obstacle has no right point (reaches all the way to s1->max_t())
		}
	}

	return (!test_corner_reach) ? false : is_inside(min_acceptable, r1e, r1, r2e, r2);
}


template<class curve1, class curve2>
template<class c1, class c2> 
bool free_space_cell<curve1, curve2>::opposite2(curve_segment<c1>* s1, curve_segment<c2>* s2, real p1, bool& r1e, real& r1, bool& r2e, real& r2, real eps, bool test_corner_reach) {
	two_tuple<real,side> intd = two_tuple<real, side>(p1, DOWN);
	bool epe; real ep; list<two_tuple<real, list<interval>>> cp;

part1:
	list<interval> sintervals = intersection_int(s1, s1->min_t(), s2, eps);
	int id = 0;
	for (id = 0; id < sintervals.size(); id++) {
		interval in = sintervals.contents(sintervals[id]);
		if (in.type == 2 && in.limits.first() >= p1) goto f1;
	}
	// no interval of type 2
	goto f2_;

f1:	// interval of type 2
	cp = control_points2(s1, s2, s1->min_t(), s1->max_t(), eps, true);
	
	int result = check_intersection2(s1, s2, sintervals, cp.pop().first(), cp, id, intd, epe, ep, eps);
	if (result == 1) {
		r1e = r2e = false; return false;
	}
f2_:
	bool interval_type2 = false;
	list<interval> p1intervals = intersection_int(s2, p1, s1, eps);
	for (id = 0; id < p1intervals.size(); id++) {
		interval in = p1intervals.contents(p1intervals[id]);
		if (in.type == 2) goto f2;
	}
	// no interval of type 2
	r1e = true; r1 = p1; goto part2;

f2:	// interval of type 2
	interval_type2 = true;
	// control poins upwards of p1
	cp = control_points2(s2, s1, p1, s2->max_t(), eps, true, true, false);
	//check_intersection2(s2, s1, p1intervals, cp.pop().first(), cp, id, intd, epe, ep, eps);
	check_intersection2(s2, s1, p1intervals, cp.pop().first(), cp, id, intd, epe, ep, eps, false);
	if (epe) {
		r1e = true; r1 = ep;
	}
	else {
		r1e = false;
	}

part2:
	list<interval> rintervals = intersection_int(s1, s1->max_t(), s2, eps);
	real min_acceptable = s2->max_t();
	if (test_corner_reach) {
		for (int i = rintervals.size() -1; i >= 0; i--) {
			interval in = rintervals.contents(rintervals[i]);
			if (in.type == 2) break;
			if (in.limits.first() < min_acceptable) min_acceptable = in.limits.first();
		}
	}

	for (id = 0; id < rintervals.size(); id++) {
		interval in = rintervals.contents(rintervals[id]);
		//if (in.type == 2 && in.limits.first() >= p1) goto f3;	// interval upwards from p1		//TODO check if on the bottom, 
		if (in.type == 2) {
			// if there's an interval of type 2 then in.limits.first() > p1. If in.limits.first() == p1 then there would be an interval of type 3, and interval of type 2 could not appear
			if (interval_type2 &&  in.limits.first() > p1) goto f3;
			else if (!interval_type2 &&  in.limits.first() >= p1) goto f3;
		}
	}

	// no interval of type 2 upwards from p1
	r2e = false; return (!test_corner_reach) ? false : is_inside(min_acceptable, r1e, r1, r2e, r2);;

f3:	// interval of type 2 upwards from p1
	real_point in_limits = rintervals.contents(rintervals[id]).limits;
	real middle = (in_limits.first() + in_limits.second()) / 2;
	cp = control_points2(s2, s1, middle, s2->max_t(), eps, true, true, false);
	list<interval> mintervals = intersection_int(s2, middle, s1, eps);
	//check_intersection2(s2, s1, mintervals, cp.pop().first(), cp, mintervals.size()-1, intd, epe, ep, eps);	// following the right interval
	check_intersection2(s2, s1, mintervals, cp.pop().first(), cp, mintervals.size()-1, intd, epe, ep, eps, false);	// following the right interval
	if (!epe) r2e = false;
	else {
		r2e = true; r2 = ep;
	}

	return (!test_corner_reach) ? false : is_inside(min_acceptable, r1e, r1, r2e, r2);
}

template<class curve1, class curve2>
bool free_space_cell<curve1, curve2>::left2top(real p1, bool& r1e, real& r1, bool& r2e, real& r2, real eps, bool test_corner_reach) {
	return adjacent2(segment_x, segment_y, p1, r1e, r1, r2e, r2, eps, test_corner_reach);
}

template<class curve1, class curve2>
bool free_space_cell<curve1, curve2>::left2right(real p1, bool& r1e, real& r1, bool& r2e, real& r2, real eps, bool test_corner_reach) {
	return opposite2(segment_x, segment_y, p1, r1e, r1, r2e, r2, eps, test_corner_reach);
}

template<class curve1, class curve2>
bool free_space_cell<curve1, curve2>::bottom2right(real p1, bool& r1e, real& r1, bool& r2e, real& r2, real eps, bool test_corner_reach) {
	return adjacent2(segment_y, segment_x, p1, r1e, r1, r2e, r2, eps, test_corner_reach);
}

template<class curve1, class curve2>
bool free_space_cell<curve1, curve2>::bottom2top(real p1, bool& r1e, real& r1, bool& r2e, real& r2, real eps, bool test_corner_reach) {
	return opposite2(segment_y, segment_x, p1, r1e, r1, r2e, r2, eps, test_corner_reach);
}

template<class curve1, class curve2>
bool free_space_cell<curve1, curve2>::solve_propagate(
														bool& t1b, real& t1, 
														bool& t2b, real& t2, 
														bool& r1b, real& r1,
														bool& r2b, real& r2, real eps) {
	bool tp1b; real tp1;
	bool tp2b; real tp2;
	bool rp1b; real rp1;
	bool rp2b; real rp2;

	tp1b = tp2b = rp1b = rp2b = false;
	real p1, p2; bool p1b, p2b;

	bool succeeds = false;

	// LEFT TO TOP //
	if ((up != 0 || (up == 0 && right == 0)) && lp1b) {
		succeeds = succeeds | left2top(lp1, p1b, p1, p2b, p2, eps, up == 0 && right == 0);
		if (!tp1b && p1b) {tp1 = p1; tp1b = true;}
		else if (tp1b && p1b && p1 < tp1) tp1 = p1;
		if (!tp2b && p2b) {tp2 = p2; tp2b = true;}
		else if (tp2b && p2b && p2 < tp2) tp2 = p2;
	}

	if ((up != 0 || (up == 0 && right == 0)) && lp2b) {
		succeeds = succeeds | left2top(lp2, p1b, p1, p2b, p2, eps, up == 0 && right == 0);
		if (!tp1b && p1b) {tp1 = p1; tp1b = true;}
		else if (tp1b && p1b && p1 < tp1) tp1 = p1;
		if (!tp2b && p2b) {tp2 = p2; tp2b = true;}
		else if (tp2b && p2b && p2 < tp2) tp2 = p2;
	}

	// LEFT TO RIGHT //
	if ((right != 0 || (up == 0 && right == 0)) && lp1b) {
		succeeds = succeeds | left2right(lp1, p1b, p1, p2b, p2, eps, up == 0 && right == 0);
		if (!rp1b && p1b) {rp1 = p1; rp1b = true;}
		else if (rp1b && p1b && p1 < rp1) rp1 = p1;
		if (!rp2b && p2b) {rp2 = p2; rp2b = true;}
		else if (rp2b && p2b && p2 < rp2) rp2 = p2;
	}

	if ((right != 0 || (up == 0 && right == 0)) && lp2b) {
		succeeds = succeeds | left2right(lp2, p1b, p1, p2b, p2, eps, up == 0 && right == 0);
		if (!rp1b && p1b) {rp1 = p1; rp1b = true;}
		else if (rp1b && p1b && p1 < rp1) rp1 = p1;
		if (!rp2b && p2b) {rp2 = p2; rp2b = true;}
		else if (rp2b && p2b && p2 < rp2) rp2 = p2;
	}


	// BOTTOM TO RIGHT //
	if ((right != 0 || (up == 0 && right == 0)) && bp1b) {
		succeeds = succeeds | bottom2right(bp1, p1b, p1, p2b, p2, eps, up == 0 && right == 0);
		if (!rp1b && p1b) {rp1 = p1; rp1b = true;}
		else if (rp1b && p1b && p1 < rp1) rp1 = p1;
		if (!rp2b && p2b) {rp2 = p2; rp2b = true;}
		else if (rp2b && p2b && p2 < rp2) rp2 = p2;
	}

	if ((right != 0 || (up == 0 && right == 0)) && bp2b) {
		succeeds = succeeds | bottom2right(bp2, p1b, p1, p2b, p2, eps, up == 0 && right == 0);
		if (!rp1b && p1b) {rp1 = p1; rp1b = true;}
		else if (rp1b && p1b && p1 < rp1) rp1 = p1;
		if (!rp2b && p2b) {rp2 = p2; rp2b = true;}
		else if (rp2b && p2b && p2 < rp2) rp2 = p2;
	}


	// BOTTOM TO TOP //
	if ((up != 0 || (up == 0 && right == 0)) && bp1b) {
		succeeds = succeeds | bottom2top(bp1, p1b, p1, p2b, p2, eps, up == 0 && right == 0);
		if (!tp1b && p1b) {tp1 = p1; tp1b = true;}
		else if (tp1b && p1b && p1 < tp1) tp1 = p1;
		if (!tp2b && p2b) {tp2 = p2; tp2b = true;}
		else if (tp2b && p2b && p2 < tp2) tp2 = p2;
	}

	if ((up != 0 || (up == 0 && right == 0)) && bp2b) {
		succeeds = succeeds | bottom2top(bp2, p1b, p1, p2b, p2, eps, up == 0 && right == 0);
		if (!tp1b && p1b) {tp1 = p1; tp1b = true;}
		else if (tp1b && p1b && p1 < tp1) tp1 = p1;
		if (!tp2b && p2b) {tp2 = p2; tp2b = true;}
		else if (tp2b && p2b && p2 < tp2) tp2 = p2;
	}

	if (up != 0) {
		up->bp1b = tp1b;
		up->bp2b = tp2b;
		up->bp1 = tp1;
		up->bp2 = tp2;
	}

	if (right != 0) {
		right->lp1b = rp1b;
		right->lp2b = rp2b;
		right->lp1 = rp1;
		right->lp2 = rp2;
	}

	if (up == 0 && right == 0) {
		t1b = tp1b; t1 = tp1;
		t2b = tp2b; t2 = tp2;
		r1b = rp1b; r1 = rp1;
		r2b = rp2b; r2 = rp2;

		return succeeds;
	}

	return false;
}


template<class curve1, class curve2>
list<free_space_cell<curve1, curve2>*> free_space_cell<curve1, curve2>::right_hand_most() {
	list<free_space_cell<curve1, curve2>*> res;
	if (this->up) res = this->up->right_hand_most();

	free_space_cell<curve1, curve2>* cell = this;
	while (cell->right) cell = cell->right;

	res.append(cell);

	return res;
}

template<class curve1, class curve2>
list<free_space_cell<curve1, curve2>*> free_space_cell<curve1, curve2>::top_hand_most() {
	list<free_space_cell<curve1, curve2>*> res;
	if (this->right) res = this->right->top_hand_most();

	free_space_cell<curve1, curve2>* cell = this;
	while (cell->up) cell = cell->up;

	res.append(cell);

	return res;
}
}
#endif