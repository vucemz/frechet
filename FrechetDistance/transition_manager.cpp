#include "stdafx.h"
#include "transition_manager.h"

namespace frechet {
bool transition_manager::compare(list<interval> lst1, list<interval> lst2) {
	if (lst1.size() != lst2.size()) return false;

	for (int i = 0; i < lst1.size(); i++) {
		if (lst1.contents(lst1[i]) != lst2.contents(lst2[i])) return false;
	}

	return true;
}

bool transition_manager::contains(
									list<interval> lst1, 
									list<interval> lst2, 
									int index, 
									list<int>& result_indexes, 
									list<interval>& result_types,
									bool debug) {
	list<list<interval>> poss;

	// get list of all possibilities
	if (contains3(lst1) && contains3(lst2)) {
		if (debug) cout << "3 -> 3 : " << lst1 << " -> " << lst2 << "\n";
		three2three(lst1, poss);
	}
	else if (contains3(lst1) && !contains3(lst2)) {
		if (debug) cout << "3 -> 2 : " << lst1 << " -> " << lst2 << "\n";
		three2two(lst1, poss);
	}
	else {
		if (debug) cout << "2 -> x : " << lst1 << " -> " << lst2 << "\n";
		two2x(lst1, poss);
	}
		
	// check if any of the possibilities matches the expected list (lst2)
	list<interval> item;
	forall(item, poss) {
		if (compare(item, lst2)) {
			for (int i = 0; i < item.size(); i++) {
				interval tp = item.contents(item[i]);
					
				// check if tp current index
				if (tp.ids.search(index) == 0) goto m;

				// it does, add index as result
				result_indexes.append(i);
				m: ;
			}

			//result_types = item;
			// preserve limits from lst2
			for (int i = 0; i < item.size(); i++) {
				interval in = item.contents(item[i]);
				/*result_types.append(interval(in.type, in.behaves_as, in.ids, lst2.contents(lst2[i]).limits));*/
				//TODO could result_types be lst2 itself?
				result_types.append(interval(in.type, in.ids, lst2.contents(lst2[i]).limits));

			}

			return true;
		}
	}
	return false;
}

bool transition_manager::contains3(list<interval>& lst) {
	interval i;
	forall(i, lst) {
		if (i.type == 3) return true;
	}
	return false;
}

////TODO add 3(3),3(3) -> ...
//// input can't be called by reference because it might get reversed
//void transition_manager::three2three(list<interval> input, list<list<interval>>& result) {
//	if (input.size() == 2) {
//		int i = 0;
//		interval tp0 = input.contents(input[i++]);
//		interval tp1 = input.contents(input[i]);
//
//		// x,3 -> x,3; 3,3;
//		if (tp0.type != 3 && tp1.type == 3) {
//			list<interval> possible;
//
//			// x,3
//			possible.append(interval(tp0.type, tp0.behaves_as, 0));
//			possible.append(interval(3, tp1.behaves_as));
//			result.append(possible); possible.clear();
//
//			// 3,3
//			possible.append(interval(3, tp0.behaves_as, 0));
//			possible.append(interval(3, tp1.behaves_as, 1));
//			result.append(possible); possible.clear();
//		}
//
//		// 3,x -> 3,x y,3;
//		else if (tp1.type != 3 && tp0.type == 3) {
//			list<interval> possible;
//
//			// 3,x
//			possible.append(interval(3, tp0.behaves_as));
//			possible.append(interval(tp1.type, tp1.behaves_as, 1));
//			result.append(possible); possible.clear();
//
//			// 3,3
//			possible.append(interval(3, tp0.behaves_as, 0));
//			possible.append(interval(3, tp1.behaves_as, 1));
//			result.append(possible); possible.clear();
//		}
//
//		//// 3,x -> 3,x 3,3;
//		//else if (tp1.type != 3 && tp0.type == 3) {
//		//	input.reverse();
//		//	list<list<interval>> result_tmp;
//		//	three2three(input, result_tmp);
//		//	list<interval> tmp;
//		//	forall(tmp, result_tmp) {
//		//		tmp.reverse(); result.append(tmp);
//		//	}
//		//}
//
//		//TODO 3(1),3(2) -> 3(2),3(1)?
//		// 3(1),3(2) -> 1,3; 3,2;
//		// 3(2),3(1) -> 2,3; 3,1;
//		if (tp0.type == 3 && tp1.type == 3) {
//			list<interval> possible;
//			possible.append(interval(tp0.type, tp0.behaves_as));
//			possible.append(interval(tp1.behaves_as, tp1.behaves_as, 1));
//			result.append(possible); possible.clear();
//
//			possible.append(interval(tp0.behaves_as, tp0.behaves_as, 0));
//			possible.append(interval(tp1.type, tp1.behaves_as));
//			result.append(possible); possible.clear();
//
//			// 3(x), 3(y) -> 3(y), 3(x)
//			possible.append(interval(tp0.type, tp1.behaves_as));
//			possible.append(interval(tp1.type, tp0.behaves_as));
//			result.append(possible); possible.clear();
//		}
//	}
//	else if (input.size() == 3) {
//		int i = 0;
//		interval tp0 = input.contents(input[i++]);
//		interval tp1 = input.contents(input[i++]);
//		interval tp2 = input.contents(input[i]);
//
//		//3,y,x -> 3,y,x; 3,y,3; 3,3,x;
//		if (tp0.type == 3 && tp1.type != 3 && tp2.type != 3) {
//			list<interval> possible;
//			possible.append(interval(tp0.type, tp0.behaves_as));
//			possible.append(interval(tp1.type, tp1.behaves_as, 1));
//			possible.append(interval(tp2.type, tp2.behaves_as, 2));
//			result.append(possible); possible.clear();
//
//			possible.append(interval(tp0.type, tp0.behaves_as));
//			possible.append(interval(tp1.type, tp1.behaves_as, 1));
//			possible.append(interval(3, tp2.behaves_as, 2));
//			result.append(possible); possible.clear();
//
//			possible.append(interval(tp0.type, tp0.behaves_as));
//			possible.append(interval(3, tp1.behaves_as, 1));
//			possible.append(interval(tp2.type, tp2.behaves_as, 2));
//			result.append(possible); possible.clear();
//		}
//
//		//x,y,3 -> x,y,3; 3,y,3; x,3,3;
//		else if (tp0.type != 3 && tp1.type != 3 && tp2.type == 3) {
//			list<interval> possible;
//
//			possible.append(interval(tp0.type, tp0.behaves_as, 0));
//			possible.append(interval(tp1.type, tp1.behaves_as, 1));
//			possible.append(interval(tp2.type, tp2.behaves_as));
//			result.append(possible); possible.clear();
//
//			possible.append(interval(3, tp0.behaves_as, 0));
//			possible.append(interval(tp1.type, tp1.behaves_as, 1));
//			possible.append(interval(tp2.type, tp2.behaves_as));
//			result.append(possible); possible.clear();
//
//			possible.append(interval(tp0.type, tp0.behaves_as, 0));
//			possible.append(interval(3, tp1.behaves_as, 1));
//			possible.append(interval(tp2.type, tp2.behaves_as));
//			result.append(possible); possible.clear();
//		}
//
//		////x,y,3 -> x,y,3; 3,y,3; x,3,3;
//		//else if (tp0.type != 3 && tp1.type != 3 && tp2.type == 3) {
//		//	input.reverse();
//		//	list<list<interval>> result_tmp;
//		//	three2three(input, result_tmp);
//		//	list<interval> tmp;
//		//	forall(tmp, result_tmp) {
//		//		tmp.reverse(); result.append(tmp);
//		//	}
//		//}
//
//		// x,3,x -> x,3,x; 3,3,x; x,3,3;
//		else if (tp0.type != 3 && tp1.type == 3 && tp2.type != 3) {
//			list<interval> possible;
//			possible.append(interval(tp0.type, tp0.behaves_as, 0));
//			possible.append(interval(tp1.type, tp1.behaves_as));
//			possible.append(interval(tp2.type, tp2.behaves_as, 2));
//			result.append(possible); possible.clear();
//
//			possible.append(interval(3, tp0.behaves_as, 0));
//			possible.append(interval(tp1.type, tp1.behaves_as));
//			possible.append(interval(tp2.type, tp2.behaves_as, 2));
//			result.append(possible); possible.clear();
//
//			possible.append(interval(tp0.type, tp0.behaves_as, 0));
//			possible.append(interval(tp1.type, tp1.behaves_as));
//			possible.append(interval(3, tp2.behaves_as, 2));
//			result.append(possible); possible.clear();
//		}
//
//		//3,3,x -> 3,3,x; x,3,x; 3,y,x; 3,x; x,3,3; 3,y,3;
//		else if (tp0.type == 3 && tp1.type == 3 && tp2.type != 3) {
//			list<interval> possible;
//			possible.append(interval(tp0.type, tp0.behaves_as));
//			possible.append(interval(tp1.type, tp1.behaves_as));
//			possible.append(interval(tp2.type, tp2.behaves_as, 2));
//			result.append(possible); possible.clear();
//
//			possible.append(interval(tp0.behaves_as, tp0.behaves_as, 0));
//			possible.append(interval(tp1.type, tp1.behaves_as));
//			possible.append(interval(tp2.type, tp2.behaves_as, 2));
//			result.append(possible); possible.clear();
//
//			possible.append(interval(tp0.type, tp0.behaves_as));
//			possible.append(interval(tp1.behaves_as, tp1.behaves_as, 1));
//			possible.append(interval(tp2.type, tp2.behaves_as, 2));
//			result.append(possible); possible.clear();
//
//			possible.append(interval(tp1.type, tp1.behaves_as));
//			possible.append(interval(tp2.type, tp2.behaves_as, 2));
//			result.append(possible); possible.clear();
//				
//			possible.append(interval(tp0.behaves_as, tp0.behaves_as, 0));
//			possible.append(interval(tp1.type, tp1.behaves_as));
//			possible.append(interval(3, tp2.behaves_as, 2));
//			result.append(possible); possible.clear();
//				
//			possible.append(interval(tp0.type, tp0.behaves_as));
//			possible.append(interval(tp1.behaves_as, tp1.behaves_as, 1));
//			possible.append(interval(3, tp2.behaves_as, 2));
//			result.append(possible); possible.clear();
//		}
//
//		//x,3,3 -> x,3,3; x,3,x; x,y,3; x,3; 3,3,x; 3,y,3;
//		else if (tp0.type != 3 && tp1.type == 3 && tp2.type == 3) {
//			list<interval> possible;
//
//			possible.append(interval(tp0.type, tp0.behaves_as, 0));
//			possible.append(interval(tp1.type, tp1.behaves_as));
//			possible.append(interval(tp2.type, tp2.behaves_as));
//			result.append(possible); possible.clear();
//
//			possible.append(interval(tp0.type, tp0.behaves_as, 0));
//			possible.append(interval(tp1.type, tp1.behaves_as));
//			possible.append(interval(tp2.behaves_as, tp2.behaves_as, 2));
//			result.append(possible); possible.clear();
//
//			possible.append(interval(tp0.type, tp0.behaves_as, 0));
//			possible.append(interval(tp1.behaves_as, tp1.behaves_as, 1));
//			possible.append(interval(tp2.type, tp2.behaves_as));
//			result.append(possible); possible.clear();
//
//			possible.append(interval(tp0.type, tp0.behaves_as, 0));
//			possible.append(interval(tp1.type, tp1.behaves_as));
//			result.append(possible); possible.clear();
//				
//			possible.append(interval(3, tp0.behaves_as, 0));
//			possible.append(interval(tp1.type, tp1.behaves_as));
//			possible.append(interval(tp2.behaves_as, tp2.behaves_as, 2));
//			result.append(possible); possible.clear();
//				
//			possible.append(interval(3, tp0.behaves_as, 0));
//			possible.append(interval(tp1.behaves_as, tp1.behaves_as, 1));
//			possible.append(interval(tp2.type, tp2.behaves_as));
//			result.append(possible); possible.clear();
//		}
//
//		////x,3,3 -> x,3,3; x,3,x; x,y,3; x,3; 3,3,x; 3,y,3;
//		//else if (tp0.type != 3 && tp1.type == 3 && tp2.type == 3) {
//		//	input.reverse();
//		//	list<list<interval>> result_tmp;
//		//	three2three(input, result_tmp);
//		//	list<interval> tmp;
//		//	forall(tmp, result_tmp) {
//		//		tmp.reverse(); result.append(tmp);
//		//	}
//		//}
//
//		// 3,x,3 -> 3,x,y; 3,x,3; 3,3,y; y,x,3; y,3,3;
//		else if (tp0.type == 3 && tp1.type != 3 && tp2.type == 3) {
//			list<interval> possible;
//			possible.append(interval(tp0.type, tp0.behaves_as));
//			possible.append(interval(tp1.type, tp1.behaves_as, 1));
//			possible.append(interval(tp2.behaves_as, tp2.behaves_as, 2));
//			result.append(possible); possible.clear();
//
//			possible.append(interval(tp0.type, tp0.behaves_as));
//			possible.append(interval(tp1.type, tp1.behaves_as, 1));
//			possible.append(interval(tp2.type, tp2.behaves_as));
//			result.append(possible); possible.clear();
//
//			possible.append(interval(tp0.type, tp0.behaves_as));
//			possible.append(interval(3, tp1.behaves_as, 1));
//			possible.append(interval(tp2.behaves_as, tp2.behaves_as, 2));
//			result.append(possible); possible.clear();
//
//			possible.append(interval(tp0.behaves_as, tp0.behaves_as, 0));
//			possible.append(interval(tp1.type, tp1.behaves_as, 1));
//			possible.append(interval(tp2.type, tp2.behaves_as));
//			result.append(possible); possible.clear();
//				
//			possible.append(interval(tp0.behaves_as, tp0.behaves_as, 0));
//			possible.append(interval(3, tp1.behaves_as, 1));
//			possible.append(interval(tp2.type, tp2.behaves_as));
//			result.append(possible); possible.clear();
//		}
//	}
//}
//
//
//
//// input can't be called by reference because it might get reversed
//void transition_manager::three2two(list<interval> input, list<list<interval>>& result) {
//	if (input.size() == 2) {
//		int i = 0;
//		interval tp0 = input.contents(input[i++]);
//		interval tp1 = input.contents(input[i]);
//
//		// x,3 -> x; x,y;
//		if (tp0.type != 3 && tp1.type == 3) {
//			list<interval> possible;
//
//			// x
//			possible.append(interval(tp0.type, tp0.behaves_as, 0));
//			result.append(possible); possible.clear();
//
//			// x,y
//			possible.append(interval(tp0.type, tp0.behaves_as, 0));
//			possible.append(interval(tp1.behaves_as, tp1.behaves_as, 1));
//			result.append(possible); possible.clear();
//		}
//		// 3,x -> x; y,x;
//		else if (tp1.type != 3 && tp0.type == 3) {
//			list<interval> possible;
//
//			// x
//			possible.append(interval(tp1.type, tp1.behaves_as, 1));
//			result.append(possible); possible.clear();
//
//			// y,x
//			possible.append(interval(tp0.behaves_as, tp0.behaves_as, 0));
//			possible.append(interval(tp1.type, tp1.behaves_as, 1));
//			result.append(possible); possible.clear();
//		}
//		
//		//// 3,x
//		//else if (tp1.type != 3 && tp0.type == 3) {
//		//	input.reverse();
//		//	list<list<interval>> result_tmp;
//		//	three2two(input, result_tmp);
//		//	list<interval> tmp;
//		//	forall(tmp, result_tmp) {
//		//		tmp.reverse(); result.append(tmp);
//		//	}
//		//}
//
//		// 3(x),3(y) -> x; x,y; y;
//		if (tp0.type == 3 && tp1.type == 3) {
//			list<interval> possible;
//
//			// x,y
//			possible.append(interval(tp0.behaves_as, tp0.behaves_as, 0));
//			possible.append(interval(tp1.behaves_as, tp1.behaves_as, 1));
//			result.append(possible); possible.clear();
//
//			// x
//			possible.append(interval(tp0.behaves_as, tp0.behaves_as, 0));
//			result.append(possible); possible.clear();
//
//			// y
//			possible.append(interval(tp1.behaves_as, tp1.behaves_as, 1));
//			result.append(possible); possible.clear();
//		}
//	}
//	else if (input.size() == 3) {
//		int i = 0;
//		interval tp0 = input.contents(input[i++]);
//		interval tp1 = input.contents(input[i++]);
//		interval tp2 = input.contents(input[i]);
//
//		//3,y,x -> x,y,x; y,x; 
//		if (tp0.type == 3 && tp1.type != 3 && tp2.type != 3) {
//			list<interval> possible;
//
//			// x,y,x
//			possible.append(interval(tp0.behaves_as, tp0.behaves_as, 0));
//			possible.append(interval(tp1.type, tp1.behaves_as, 1));
//			possible.append(interval(tp2.type, tp2.behaves_as, 2));
//			result.append(possible); possible.clear();
//
//			//y,x
//			possible.append(interval(tp1.type, tp1.behaves_as, 1));
//			possible.append(interval(tp2.type, tp2.behaves_as, 2));
//			result.append(possible); possible.clear();
//		}
//
//		//x,y,3
//		else if (tp0.type != 3 && tp1.type != 3 && tp2.type == 3) {
//			list<interval> possible;
//
//			// x,y,x
//			possible.append(interval(tp0.type, tp0.behaves_as, 0));
//			possible.append(interval(tp1.type, tp1.behaves_as, 1));
//			possible.append(interval(tp2.behaves_as, tp2.behaves_as, 2));
//			result.append(possible); possible.clear();
//
//			//x,y
//			possible.append(interval(tp0.type, tp0.behaves_as, 0));
//			possible.append(interval(tp1.type, tp1.behaves_as, 1));
//			result.append(possible); possible.clear();
//		}
//
//		////x,y,3
//		//else if (tp0.type != 3 && tp1.type != 3 && tp2.type == 3) {
//		//	input.reverse();
//		//	list<list<interval>> result_tmp;
//		//	three2two(input, result_tmp);
//		//	list<interval> tmp;
//		//	forall(tmp, result_tmp) {
//		//		tmp.reverse(); result.append(tmp);
//		//	}
//		//}
//
//		//x,3,x -> x,y,x; x;
//		else if (tp0.type != 3 && tp1.type == 3 && tp2.type != 3) {
//			list<interval> possible;
//
//			// x,y,x
//			possible.append(interval(tp0.type, tp0.behaves_as, 0));
//			possible.append(interval(tp1.behaves_as, tp1.behaves_as, 1));
//			possible.append(interval(tp2.type, tp2.behaves_as, 2));
//			result.append(possible); possible.clear();
//
//			// x
//			possible.append(interval(tp0.behaves_as, tp0.behaves_as, 0, 2));
//			result.append(possible); possible.clear();
//		}
//
//		//x,3,3 -> x (2?); x,y; x,y,x;		x,3,3[-1] -> x(1,3); x,3,3(m) -> x(1,2)
//		else if (tp0.type != 3 && tp1.type == 3 && tp2.type == 3) {
//			list<interval> possible;
//
//			// x,y
//			possible.append(interval(tp0.type, tp0.behaves_as, 0));
//			possible.append(interval(tp1.behaves_as, tp1.behaves_as, 1));
//			result.append(possible); possible.clear();
//
//			// x,y,x
//			possible.append(interval(tp0.type, tp0.behaves_as, 0));
//			possible.append(interval(tp1.behaves_as, tp1.behaves_as, 1));
//			possible.append(interval(tp2.behaves_as, tp2.behaves_as, 2));
//			result.append(possible); possible.clear();
//
//			
//			int k = 0;
//			//if (tp2.ids.size() == 1 && tp2.ids.contents(tp2.ids[k]) == -1) {
//			if (tp2.ids.size() == 0) {
//				// x,3(y),3(x)[-1] -> x(1,3)
//				possible.append(interval(tp0.type, tp0.behaves_as, 0, 2));
//			}
//			else {
//				// x,3(y),3(x)[m] -> x(1)
//				possible.append(interval(tp0.type, tp0.behaves_as, 0));
//			}
//			result.append(possible); possible.clear();
//		}
//
//		//3,3,x
//		else if (tp0.type == 3 && tp1.type == 3 && tp2.type != 3) {
//			list<interval> possible;
//
//			// y,x
//			possible.append(interval(tp1.behaves_as, tp1.behaves_as, 1));
//			possible.append(interval(tp2.type, tp2.behaves_as, 2));
//			result.append(possible); possible.clear();
//
//			// x,y,x
//			possible.append(interval(tp0.behaves_as, tp0.behaves_as, 0));
//			possible.append(interval(tp1.behaves_as, tp1.behaves_as, 1));
//			possible.append(interval(tp2.type, tp2.behaves_as, 2));
//			result.append(possible); possible.clear();
//
//			
//			int k = 0;
//			//if (tp0.ids.size() == 1 && tp0.ids.contents(tp0.ids[k]) == -1) {
//			if (tp0.ids.size() == 0) {
//				// 3(x)[-1],3(y),x -> x(1,3)
//				possible.append(interval(tp2.type, tp2.behaves_as, 2, 0));
//			}
//			else {
//				// 3(x)[m],3(y),x -> x(3)
//				possible.append(interval(tp2.type, tp2.behaves_as, 2));
//			}
//			result.append(possible); possible.clear();
//		}
//
//		////3,3,x
//		//else if (tp0.type == 3 && tp1.type == 3 && tp2.type != 3) {
//		//	input.reverse();
//		//	list<list<interval>> result_tmp;
//		//	three2two(input, result_tmp);
//		//	list<interval> tmp;
//		//	forall(tmp, result_tmp) {
//		//		tmp.reverse(); result.append(tmp);
//		//	}
//		//}
//
//		// 3,x,3 -> x; x,y; y,x; y,x,y;
//		else if (tp0.type == 3 && tp1.type != 3 && tp2.type == 3) {
//			list<interval> possible;
//
//			// x
//			possible.append(interval(tp1.type, tp1.behaves_as, 1));
//			result.append(possible); possible.clear();
//
//			// x,y
//			possible.append(interval(tp1.type, tp1.behaves_as, 1));
//			possible.append(interval(tp2.behaves_as, tp2.behaves_as, 2));
//			result.append(possible); possible.clear();
//
//			// y,x
//			possible.append(interval(tp0.behaves_as, tp0.behaves_as, 0));
//			possible.append(interval(tp1.type, tp1.behaves_as, 1));
//			result.append(possible); possible.clear();
//
//			// y,x,y
//			possible.append(interval(tp0.behaves_as, tp0.behaves_as, 0));
//			possible.append(interval(tp1.type, tp1.behaves_as, 1));
//			possible.append(interval(tp2.behaves_as, tp2.behaves_as, 2));
//			result.append(possible); possible.clear();
//		}
//	}
//
//}
//
//void transition_manager::two2x(list<interval> input, list<list<interval>>& result) {
//	
//	if (input.size() == 1) {
//		int i = 0;
//		interval tp0 = input.contents(input[i]);
//
//		int x = tp0.type;
//		int y;
//		if (x == 1) y = 2; else y = 1;
//
//		// x -> x; x,3,x; x,3,3; x,3; 3,x; 3,3; 3,3; 3,x,3; 3,3,x;
//		list<interval> possible;
//
//		// x
//		possible.append(interval(tp0.type, tp0.behaves_as, 0));
//		result.append(possible); possible.clear();
//
//		// x,3,x
//		possible.append(interval(tp0.type, tp0.behaves_as, 0));
//		possible.append(interval(3, y));
//		possible.append(interval(tp0.type, tp0.behaves_as, 0));
//		result.append(possible); possible.clear();
//
//		// x,3,3
//		possible.append(interval(tp0.type, tp0.behaves_as, 0));
//		possible.append(interval(3, y));
//		possible.append(interval(3, x));
//		result.append(possible); possible.clear();
//
//		// 3,3,x
//		possible.append(interval(3, x));
//		possible.append(interval(3, y));
//		possible.append(interval(tp0.type, tp0.behaves_as, 0));
//		result.append(possible); possible.clear();
//
//		// x,3
//		possible.append(interval(tp0.type, tp0.behaves_as, 0));
//		possible.append(interval(3, y));
//		result.append(possible); possible.clear();
//
//		// 3,x
//		possible.append(interval(3, y));
//		possible.append(interval(tp0.type, tp0.behaves_as, 0));
//		result.append(possible); possible.clear();
//
//		// 3,3
//		possible.append(interval(3, x, 0));
//		possible.append(interval(3, y));
//		result.append(possible); possible.clear();
//
//		// 3,3
//		possible.append(interval(3, y));
//		possible.append(interval(3, x, 0));
//		result.append(possible); possible.clear();
//
//		// 3,x,3
//		possible.append(interval(3, y));
//		possible.append(interval(tp0.type, tp0.behaves_as, 0));
//		possible.append(interval(3, y));
//		result.append(possible); possible.clear();
//	}
//	else if (input.size() == 2) {
//	
//	//if (input.size() == 2) {
//		int i = 0;
//		interval tp0 = input.contents(input[i++]);
//		interval tp1 = input.contents(input[i]);
//
//		// x,y -> x,y; x,3; 3,y; 3,3; 3,x,y; 3,x,3; 3,3,y; x,y,3; x,3,3; 3,y,3;
//		list<interval> possible;
//
//		// x,y
//		possible.append(interval(tp0.type, tp0.behaves_as, 0));
//		possible.append(interval(tp1.type, tp1.behaves_as, 1));
//		result.append(possible); possible.clear();
//
//		// x,3
//		possible.append(interval(tp0.type, tp0.behaves_as, 0));
//		possible.append(interval(3, tp1.behaves_as, 1));
//		result.append(possible); possible.clear();
//
//		// 3,y
//		possible.append(interval(3, tp0.behaves_as, 0));
//		possible.append(interval(tp1.type, tp1.behaves_as, 1));
//		result.append(possible); possible.clear();
//
//		// 3,3
//		possible.append(interval(3, 3, 0));
//		possible.append(interval(3, 3, 1));
//		result.append(possible); possible.clear();
//
//		// 3,x,y
//		possible.append(interval(3, tp1.behaves_as));
//		possible.append(interval(tp0.type, tp0.behaves_as, 0));
//		possible.append(interval(tp1.type, tp1.behaves_as, 1));
//		result.append(possible); possible.clear();
//
//		// 3,x,3
//		possible.append(interval(3, tp1.behaves_as));
//		possible.append(interval(tp0.type, tp0.behaves_as, 0));
//		possible.append(interval(3, tp1.behaves_as, 1));
//		result.append(possible); possible.clear();
//
//		// 3,3,y
//		possible.append(interval(3, tp1.behaves_as));
//		possible.append(interval(3, tp0.behaves_as, 0));
//		possible.append(interval(tp1.type, tp1.behaves_as, 1));
//		result.append(possible); possible.clear();
//
//		// x,y,3
//		possible.append(interval(tp0.type, tp0.behaves_as, 0));
//		possible.append(interval(tp1.type, tp1.behaves_as, 1));
//		possible.append(interval(3, tp0.behaves_as));
//		result.append(possible); possible.clear();
//
//		// x,3,3
//		possible.append(interval(tp0.type, tp0.behaves_as, 0));
//		possible.append(interval(3, tp1.behaves_as, 1));
//		possible.append(interval(3, tp0.behaves_as));
//		result.append(possible); possible.clear();
//
//		// 3,y,3
//		possible.append(interval(3, tp0.behaves_as, 0));
//		possible.append(interval(tp1.type, tp1.behaves_as, 1));
//		possible.append(interval(3, tp0.behaves_as));
//		result.append(possible); possible.clear();
//	}
//
//	else if (input.size() == 3) {
//		int i = 0;
//		interval tp0 = input.contents(input[i++]);
//		interval tp1 = input.contents(input[i++]);
//		interval tp2 = input.contents(input[i]);
//
//		//x,y,x -> x,y,x; x,y,3; x,3,x; x,3,3; 3,y,x; 3,y,3; 3,3,x;
//		list<interval> possible;
//
//		// x,y,x
//		possible.append(interval(tp0.type, tp0.behaves_as, 0));
//		possible.append(interval(tp1.type, tp1.behaves_as, 1));
//		possible.append(interval(tp2.type, tp2.behaves_as, 2));
//		result.append(possible); possible.clear();
//
//		// x,y,3
//		possible.append(interval(tp0.type, tp0.behaves_as, 0));
//		possible.append(interval(tp1.type, tp1.behaves_as, 1));
//		possible.append(interval(3, tp2.behaves_as, 2));
//		result.append(possible); possible.clear();
//
//		// x,3,x
//		possible.append(interval(tp0.type, tp0.behaves_as, 0));
//		possible.append(interval(3, tp1.behaves_as, 1));
//		possible.append(interval(tp2.type, tp2.behaves_as, 2));
//		result.append(possible); possible.clear();
//
//
//
//		// x,3,3
//		possible.append(interval(tp0.type, tp0.behaves_as, 0));
//		possible.append(interval(3, tp1.behaves_as, 1));
//		possible.append(interval(3, tp2.behaves_as, 2));
//		result.append(possible); possible.clear();
//
//
//		// 3,y,x
//		possible.append(interval(3, tp0.behaves_as, 0));
//		possible.append(interval(tp1.type, tp1.behaves_as, 1));
//		possible.append(interval(tp2.type, tp2.behaves_as, 2));
//		result.append(possible); possible.clear();
//
//		// 3,y,3
//		possible.append(interval(3, tp0.behaves_as, 0));
//		possible.append(interval(tp1.type, tp1.behaves_as, 1));
//		possible.append(interval(3, tp2.behaves_as, 2));
//		result.append(possible); possible.clear();
//
//		// 3,3,x
//		possible.append(interval(3, tp0.behaves_as, 0));
//		possible.append(interval(3, tp1.behaves_as, 1));
//		possible.append(interval(tp2.type, tp2.behaves_as, 2));
//		result.append(possible); possible.clear();
//	}
//}

//111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111//
//===================================================================================================================================================================================================//
//===================================================================================================================================================================================================//


//TODO add 3(3),3(3) -> ...
// input can't be called by reference because it might get reversed
void transition_manager::three2three(list<interval> input, list<list<interval>>& result) {
	if (input.size() == 2) {
		int i = 0;
		interval tp0 = input.contents(input[i++]);
		interval tp1 = input.contents(input[i]);

		// x,3 -> x,3; 3,3;
		if (tp0.type != 3 && tp1.type == 3) {
			list<interval> possible;

			// x,3
			possible.append(interval(tp0.type, 0));
			possible.append(interval(3));
			result.append(possible); possible.clear();

			// 3,3
			possible.append(interval(3, 0));
			possible.append(interval(3, 1));
			result.append(possible); possible.clear();
		}

		// 3,x -> 3,x y,3;
		else if (tp1.type != 3 && tp0.type == 3) {
			list<interval> possible;

			// 3,x
			possible.append(interval(3));
			possible.append(interval(tp1.type, 1));
			result.append(possible); possible.clear();

			// 3,3
			possible.append(interval(3, 0));
			possible.append(interval(3, 1));
			result.append(possible); possible.clear();
		}


		//TODO 3(1),3(2) -> 3(2),3(1)?
		// 3(1),3(2) -> 1,3; 3,2;
		// 3(2),3(1) -> 2,3; 3,1;
		// cannot follow!
		//if (tp0.type == 3 && tp1.type == 3) {
		//	list<interval> possible;

		//	possible.append(interval(tp0.type));
		//	possible.append(interval(tp1.behaves_as, 1));
		//	result.append(possible); possible.clear();

		//	possible.append(interval(tp0.behaves_as, 0));
		//	possible.append(interval(tp1.type));
		//	result.append(possible); possible.clear();

		//	// 3(x), 3(y) -> 3(y), 3(x)
		//	possible.append(interval(tp0.type));
		//	possible.append(interval(tp1.type));
		//	result.append(possible); possible.clear();
		//}
	}
	else if (input.size() == 3) {
		int i = 0;
		interval tp0 = input.contents(input[i++]);
		interval tp1 = input.contents(input[i++]);
		interval tp2 = input.contents(input[i]);

		//3,y,x -> 3,y,x; 3,y,3; 3,3,x;
		if (tp0.type == 3 && tp1.type != 3 && tp2.type != 3) {
			list<interval> possible;
			possible.append(interval(tp0.type));
			possible.append(interval(tp1.type, 1));
			possible.append(interval(tp2.type, 2));
			result.append(possible); possible.clear();

			possible.append(interval(tp0.type));
			possible.append(interval(tp1.type, 1));
			possible.append(interval(3, 2));
			result.append(possible); possible.clear();

			possible.append(interval(tp0.type));
			possible.append(interval(3, 1));
			possible.append(interval(tp2.type, 2));
			result.append(possible); possible.clear();
		}

		//x,y,3 -> x,y,3; 3,y,3; x,3,3;
		else if (tp0.type != 3 && tp1.type != 3 && tp2.type == 3) {
			list<interval> possible;

			possible.append(interval(tp0.type, 0));
			possible.append(interval(tp1.type, 1));
			possible.append(interval(tp2.type));
			result.append(possible); possible.clear();

			possible.append(interval(3, 0));
			possible.append(interval(tp1.type, 1));
			possible.append(interval(tp2.type));
			result.append(possible); possible.clear();

			possible.append(interval(tp0.type, 0));
			possible.append(interval(3, 1));
			possible.append(interval(tp2.type));
			result.append(possible); possible.clear();
		}

		// x,3,x -> x,3,x; 3,3,x; x,3,3;
		else if (tp0.type != 3 && tp1.type == 3 && tp2.type != 3) {
			list<interval> possible;
			possible.append(interval(tp0.type, 0));
			possible.append(interval(tp1.type));
			possible.append(interval(tp2.type, 2));
			result.append(possible); possible.clear();

			possible.append(interval(3, 0));
			possible.append(interval(tp1.type));
			possible.append(interval(tp2.type, 2));
			result.append(possible); possible.clear();

			possible.append(interval(tp0.type, 0));
			possible.append(interval(tp1.type));
			possible.append(interval(3, 2));
			result.append(possible); possible.clear();
		}

		//3,3,x -> 3,3,x; x,3,x; 3,y,x; 3,x; x,3,3; 3,y,3;
		else if (tp0.type == 3 && tp1.type == 3 && tp2.type != 3) {
			int tp0_behaves_as = tp2.type;
			int tp1_behaves_as = tp2.type == 1 ? 2 : 1;

			list<interval> possible;
			possible.append(interval(tp0.type));
			possible.append(interval(tp1.type));
			possible.append(interval(tp2.type, 2));
			result.append(possible); possible.clear();

			possible.append(interval(tp0_behaves_as, 0));
			possible.append(interval(tp1.type));
			possible.append(interval(tp2.type, 2));
			result.append(possible); possible.clear();

			possible.append(interval(tp0.type));
			possible.append(interval(tp1_behaves_as, 1));
			possible.append(interval(tp2.type, 2));
			result.append(possible); possible.clear();

			possible.append(interval(tp1.type));
			possible.append(interval(tp2.type, 2));
			result.append(possible); possible.clear();
				
			possible.append(interval(tp0_behaves_as, 0));
			possible.append(interval(tp1.type));
			possible.append(interval(3, 2));
			result.append(possible); possible.clear();
				
			possible.append(interval(tp0.type));
			possible.append(interval(tp1_behaves_as, 1));
			possible.append(interval(3, 2));
			result.append(possible); possible.clear();
		}

		//x,3,3 -> x,3,3; x,3,x; x,y,3; x,3; 3,3,x; 3,y,3;
		else if (tp0.type != 3 && tp1.type == 3 && tp2.type == 3) {
			int tp1_behaves_as = tp0.type == 1 ? 2 : 1;
			int tp2_behaves_as = tp0.type;

			list<interval> possible;

			possible.append(interval(tp0.type, 0));
			possible.append(interval(tp1.type));
			possible.append(interval(tp2.type));
			result.append(possible); possible.clear();

			possible.append(interval(tp0.type, 0));
			possible.append(interval(tp1.type));
			possible.append(interval(tp2_behaves_as, 2));
			result.append(possible); possible.clear();

			possible.append(interval(tp0.type, 0));
			possible.append(interval(tp1_behaves_as, 1));
			possible.append(interval(tp2.type));
			result.append(possible); possible.clear();

			possible.append(interval(tp0.type, 0));
			possible.append(interval(tp1.type));
			result.append(possible); possible.clear();
				
			possible.append(interval(3, 0));
			possible.append(interval(tp1.type));
			possible.append(interval(tp2_behaves_as, 2));
			result.append(possible); possible.clear();
				
			possible.append(interval(3, 0));
			possible.append(interval(tp1_behaves_as, 1));
			possible.append(interval(tp2.type));
			result.append(possible); possible.clear();
		}

		// 3,x,3 -> 3,x,y; 3,x,3; 3,3,y; y,x,3; y,3,3;
		else if (tp0.type == 3 && tp1.type != 3 && tp2.type == 3) {
			int tp0_behaves_as = tp1.type == 1 ? 2 : 1;
			int tp2_behaves_as = tp0_behaves_as;

			list<interval> possible;
			possible.append(interval(tp0.type));
			possible.append(interval(tp1.type, 1));
			possible.append(interval(tp2_behaves_as, 2));
			result.append(possible); possible.clear();

			possible.append(interval(tp0.type));
			possible.append(interval(tp1.type, 1));
			possible.append(interval(tp2.type));
			result.append(possible); possible.clear();

			possible.append(interval(tp0.type));
			possible.append(interval(3, 1));
			possible.append(interval(tp2_behaves_as, 2));
			result.append(possible); possible.clear();

			possible.append(interval(tp0_behaves_as, 0));
			possible.append(interval(tp1.type, 1));
			possible.append(interval(tp2.type));
			result.append(possible); possible.clear();
				
			possible.append(interval(tp0_behaves_as, 0));
			possible.append(interval(3, 1));
			possible.append(interval(tp2.type));
			result.append(possible); possible.clear();
		}
	}
}

//222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222//
//===================================================================================================================================================================================================//
//===================================================================================================================================================================================================//

// input can't be called by reference because it might get reversed
void transition_manager::three2two(list<interval> input, list<list<interval>>& result) {
	if (input.size() == 2) {
		int i = 0;
		interval tp0 = input.contents(input[i++]);
		interval tp1 = input.contents(input[i]);

		// x,3 -> x; x,y;
		if (tp0.type != 3 && tp1.type == 3) {
			int tp1_behaves_as = tp0.type == 1 ? 2 : 1;
			list<interval> possible;

			// x
			possible.append(interval(tp0.type, 0));
			result.append(possible); possible.clear();

			// x,y
			possible.append(interval(tp0.type, 0));
			possible.append(interval(tp1_behaves_as, 1));
			result.append(possible); possible.clear();
		}
		// 3,x -> x; y,x;
		else if (tp1.type != 3 && tp0.type == 3) {
			int tp0_behaves_as = tp1.type == 1 ? 2 : 1;
			list<interval> possible;

			// x
			possible.append(interval(tp1.type, 1));
			result.append(possible); possible.clear();

			// y,x
			possible.append(interval(tp0_behaves_as, 0));
			possible.append(interval(tp1.type, 1));
			result.append(possible); possible.clear();
		}


		// 3(x),3(y) -> x; x,y; y;
		// cannot follow
		//if (tp0.type == 3 && tp1.type == 3) {
		//	list<interval> possible;

		//	// x,y
		//	possible.append(interval(tp0.behaves_as, 0));
		//	possible.append(interval(tp1.behaves_as, 1));
		//	result.append(possible); possible.clear();

		//	// x
		//	possible.append(interval(tp0.behaves_as, 0));
		//	result.append(possible); possible.clear();

		//	// y
		//	possible.append(interval(tp1.behaves_as, 1));
		//	result.append(possible); possible.clear();
		//}
	}
	else if (input.size() == 3) {
		int i = 0;
		interval tp0 = input.contents(input[i++]);
		interval tp1 = input.contents(input[i++]);
		interval tp2 = input.contents(input[i]);

		//3,y,x -> x,y,x; y,x; 
		if (tp0.type == 3 && tp1.type != 3 && tp2.type != 3) {
			int tp0_behaves_as = tp1.type == 1 ? 2 : 1;
			list<interval> possible;

			// x,y,x
			possible.append(interval(tp0_behaves_as, 0));
			possible.append(interval(tp1.type, 1));
			possible.append(interval(tp2.type, 2));
			result.append(possible); possible.clear();

			//y,x
			possible.append(interval(tp1.type, 1));
			possible.append(interval(tp2.type, 2));
			result.append(possible); possible.clear();
		}

		//x,y,3
		else if (tp0.type != 3 && tp1.type != 3 && tp2.type == 3) {
			int tp2_behaves_as = tp1.type == 1 ? 2 : 1;
			list<interval> possible;

			// x,y,x
			possible.append(interval(tp0.type, 0));
			possible.append(interval(tp1.type, 1));
			possible.append(interval(tp2_behaves_as, 2));
			result.append(possible); possible.clear();

			//x,y
			possible.append(interval(tp0.type, 0));
			possible.append(interval(tp1.type, 1));
			result.append(possible); possible.clear();
		}


		//x,3,x -> x,y,x; x;
		else if (tp0.type != 3 && tp1.type == 3 && tp2.type != 3) {
			int tp1_behaves_as = tp0.type == 1 ? 2 : 1;
			list<interval> possible;

			// x,y,x
			possible.append(interval(tp0.type, 0));
			possible.append(interval(tp1_behaves_as, 1));
			possible.append(interval(tp2.type, 2));
			result.append(possible); possible.clear();

			// x
			possible.append(interval(tp0.type, 0, 2));
			result.append(possible); possible.clear();
		}

		//x,3,3 -> x (2?); x,y; x,y,x;		x,3,3[-1] -> x(1,3); x,3,3(m) -> x(1,2)
		else if (tp0.type != 3 && tp1.type == 3 && tp2.type == 3) {
			int tp1_behaves_as = tp0.type == 1 ? 2 : 1;
			int tp2_behaves_as = tp0.type;

			list<interval> possible;

			// x,y
			possible.append(interval(tp0.type, 0));
			possible.append(interval(tp1_behaves_as, 1));
			result.append(possible); possible.clear();

			// x,y,x
			possible.append(interval(tp0.type, 0));
			possible.append(interval(tp1_behaves_as, 1));
			possible.append(interval(tp2_behaves_as, 2));
			result.append(possible); possible.clear();

			
			int k = 0;
			//if (tp2.ids.size() == 1 && tp2.ids.contents(tp2.ids[k]) == -1) {
			if (tp2.ids.size() == 0) {
				// x,3(y),3(x)[-1] -> x(1,3)
				possible.append(interval(tp0.type, 0, 2));
			}
			else {
				// x,3(y),3(x)[m] -> x(1)
				possible.append(interval(tp0.type, 0));
			}
			result.append(possible); possible.clear();
		}

		//3,3,x
		else if (tp0.type == 3 && tp1.type == 3 && tp2.type != 3) {
			int tp1_behaves_as = tp2.type == 1 ? 2 : 1;
			int tp0_behaves_as = tp2.type;

			list<interval> possible;

			// y,x
			possible.append(interval(tp1_behaves_as, 1));
			possible.append(interval(tp2.type, 2));
			result.append(possible); possible.clear();

			// x,y,x
			possible.append(interval(tp0_behaves_as, 0));
			possible.append(interval(tp1_behaves_as, 1));
			possible.append(interval(tp2.type, 2));
			result.append(possible); possible.clear();

			
			int k = 0;
			//if (tp0.ids.size() == 1 && tp0.ids.contents(tp0.ids[k]) == -1) {
			if (tp0.ids.size() == 0) {
				// 3(x)[-1],3(y),x -> x(1,3)
				possible.append(interval(tp2.type, 2, 0));
			}
			else {
				// 3(x)[m],3(y),x -> x(3)
				possible.append(interval(tp2.type, 2));
			}
			result.append(possible); possible.clear();
		}

		// 3,x,3 -> x; x,y; y,x; y,x,y;
		else if (tp0.type == 3 && tp1.type != 3 && tp2.type == 3) {
			int tp0_behaves_as = tp1.type == 1 ? 2 : 1;
			int tp2_behaves_as = tp0_behaves_as;
			list<interval> possible;

			// x
			possible.append(interval(tp1.type, 1));
			result.append(possible); possible.clear();

			// x,y
			possible.append(interval(tp1.type, 1));
			possible.append(interval(tp2_behaves_as, 2));
			result.append(possible); possible.clear();

			// y,x
			possible.append(interval(tp0_behaves_as, 0));
			possible.append(interval(tp1.type, 1));
			result.append(possible); possible.clear();

			// y,x,y
			possible.append(interval(tp0_behaves_as, 0));
			possible.append(interval(tp1.type, 1));
			possible.append(interval(tp2_behaves_as, 2));
			result.append(possible); possible.clear();
		}
	}

}

//333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333//
//===================================================================================================================================================================================================//
//===================================================================================================================================================================================================//

void transition_manager::two2x(list<interval> input, list<list<interval>>& result) {
	
	if (input.size() == 1) {
		int i = 0;
		interval tp0 = input.contents(input[i]);

		int x = tp0.type;
		int y;
		if (x == 1) y = 2; else y = 1;

		// x -> x; x,3,x; x,3,3; x,3; 3,x; 3,3; 3,3; 3,x,3; 3,3,x;
		list<interval> possible;

		// x
		possible.append(interval(tp0.type, 0));
		result.append(possible); possible.clear();

		// x,3,x
		possible.append(interval(tp0.type, 0));
		possible.append(interval(3, y));
		possible.append(interval(tp0.type, 0));
		result.append(possible); possible.clear();

		// x,3,3
		possible.append(interval(tp0.type, 0));
		possible.append(interval(3, y));
		possible.append(interval(3, x));
		result.append(possible); possible.clear();

		// 3,3,x
		possible.append(interval(3, x));
		possible.append(interval(3, y));
		possible.append(interval(tp0.type, 0));
		result.append(possible); possible.clear();

		// x,3
		possible.append(interval(tp0.type, 0));
		possible.append(interval(3, y));
		result.append(possible); possible.clear();

		// 3,x
		possible.append(interval(3, y));
		possible.append(interval(tp0.type, 0));
		result.append(possible); possible.clear();

		// 3,3
		possible.append(interval(3, x, 0));
		possible.append(interval(3, y));
		result.append(possible); possible.clear();

		// 3,3
		possible.append(interval(3, y));
		possible.append(interval(3, x, 0));
		result.append(possible); possible.clear();

		// 3,x,3
		possible.append(interval(3, y));
		possible.append(interval(tp0.type, 0));
		possible.append(interval(3, y));
		result.append(possible); possible.clear();
	}
	else if (input.size() == 2) {
	
	//if (input.size() == 2) {
		int i = 0;
		interval tp0 = input.contents(input[i++]);
		interval tp1 = input.contents(input[i]);

		// x,y -> x,y; x,3; 3,y; 3,3; 3,x,y; 3,x,3; 3,3,y; x,y,3; x,3,3; 3,y,3;
		list<interval> possible;

		// x,y
		possible.append(interval(tp0.type, 0));
		possible.append(interval(tp1.type, 1));
		result.append(possible); possible.clear();

		// x,3
		possible.append(interval(tp0.type, 0));
		possible.append(interval(3, 1));
		result.append(possible); possible.clear();

		// 3,y
		possible.append(interval(3, 0));
		possible.append(interval(tp1.type, 1));
		result.append(possible); possible.clear();

		// 3,3
		possible.append(interval(3, 3, 0));
		possible.append(interval(3, 3, 1));
		result.append(possible); possible.clear();

		// 3,x,y
		possible.append(interval(3));
		possible.append(interval(tp0.type, 0));
		possible.append(interval(tp1.type, 1));
		result.append(possible); possible.clear();

		// 3,x,3
		possible.append(interval(3));
		possible.append(interval(tp0.type, 0));
		possible.append(interval(3, 1));
		result.append(possible); possible.clear();

		// 3,3,y
		possible.append(interval(3));
		possible.append(interval(3, 0));
		possible.append(interval(tp1.type, 1));
		result.append(possible); possible.clear();

		// x,y,3
		possible.append(interval(tp0.type, 0));
		possible.append(interval(tp1.type, 1));
		possible.append(interval(3));
		result.append(possible); possible.clear();

		// x,3,3
		possible.append(interval(tp0.type, 0));
		possible.append(interval(3, 1));
		possible.append(interval(3));
		result.append(possible); possible.clear();

		// 3,y,3
		possible.append(interval(3, 0));
		possible.append(interval(tp1.type, 1));
		possible.append(interval(3));
		result.append(possible); possible.clear();
	}

	else if (input.size() == 3) {
		int i = 0;
		interval tp0 = input.contents(input[i++]);
		interval tp1 = input.contents(input[i++]);
		interval tp2 = input.contents(input[i]);

		//x,y,x -> x,y,x; x,y,3; x,3,x; x,3,3; 3,y,x; 3,y,3; 3,3,x;
		list<interval> possible;

		// x,y,x
		possible.append(interval(tp0.type, 0));
		possible.append(interval(tp1.type, 1));
		possible.append(interval(tp2.type, 2));
		result.append(possible); possible.clear();

		// x,y,3
		possible.append(interval(tp0.type, 0));
		possible.append(interval(tp1.type, 1));
		possible.append(interval(3, 2));
		result.append(possible); possible.clear();

		// x,3,x
		possible.append(interval(tp0.type, 0));
		possible.append(interval(3, 1));
		possible.append(interval(tp2.type, 2));
		result.append(possible); possible.clear();



		// x,3,3
		possible.append(interval(tp0.type, 0));
		possible.append(interval(3, 1));
		possible.append(interval(3, 2));
		result.append(possible); possible.clear();


		// 3,y,x
		possible.append(interval(3, 0));
		possible.append(interval(tp1.type, 1));
		possible.append(interval(tp2.type, 2));
		result.append(possible); possible.clear();

		// 3,y,3
		possible.append(interval(3, 0));
		possible.append(interval(tp1.type, 1));
		possible.append(interval(3, 2));
		result.append(possible); possible.clear();

		// 3,3,x
		possible.append(interval(3, 0));
		possible.append(interval(3, 1));
		possible.append(interval(tp2.type, 2));
		result.append(possible); possible.clear();
	}
}
}