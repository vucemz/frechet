#ifndef REAL_INTERVAL_H_INCLUDED
#define REAL_INTERVAL_H_INCLUDED

#include "stdafx.h"
#include <LEDA/core/tuple.h>
#include <LEDA/numbers/real.h>

using namespace leda;

struct real_interval {
	virtual void Member(){}
};

struct real_tp_interval : public real_interval {
	real v1, v2;

	real_tp_interval() {
	}

	real_tp_interval(real v1, real v2) {
		this->v1 = v1; this->v2 = v2;
	}

	int type() {return 2;}
};

struct real_sp_interval : public real_interval {
	real v;

	real_sp_interval() {
	}

	real_sp_interval(real v) {
		this->v = v;
	}

	int type() {return 1;}
};

#endif