#ifndef TRANSITION_MANAGER_H_INCLUDED
#define TRANSITION_MANAGER_H_INCLUDED

//=================================
// included dependencies
#include "stdafx.h"
#include <LEDA/core/tuple.h>
#include <LEDA/numbers/real.h>
#include "interval.h"

using std::cout;

using leda::list;
using leda::real;
using leda::two_tuple;

typedef two_tuple<real, real> real_point;

namespace frechet {
class transition_manager {
public:
	static bool compare(list<interval> lst1, list<interval> lst2);

	static bool contains(list<interval> lst1, list<interval> lst2, int index, list<int>& result_indexes, list<interval>& result_types, bool debug = false);
	static bool contains3(list<interval>& lst);

	static void two2x(list<interval> input, list<list<interval>>& result);
	static void three2three(list<interval> input, list<list<interval>>& result);
	static void three2two(list<interval> input, list<list<interval>>& result);
	
	//static void two2x2(list<interval> input, list<list<interval>>& result);
	//static void three2three2(list<interval> input, list<list<interval>>& result);
	//static void three2two2(list<interval> input, list<list<interval>>& result);
};
}

#endif