#define BOOST_TEST_MODULE stringtest
#include <boost/test/included/unit_test.hpp>
#include "interval.h"


BOOST_AUTO_TEST_SUITE (stringtest) // name of the test suite is stringtest

BOOST_AUTO_TEST_CASE (test1)
{
	interval inter;
}

BOOST_AUTO_TEST_CASE (test2)
{
	interval inter = interval(2,2,0);
	BOOST_REQUIRE_EQUAL (inter.behaves_as, 2); // basic test 
}

BOOST_AUTO_TEST_SUITE_END( )