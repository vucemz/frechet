#ifndef FREE_SPACE_CELL_H_INCLUDED
#define FREE_SPACE_CELL_H_INCLUDED

//=================================
// forward declared dependencies
class interval;
template<class curve> class curve_segment;
//class real_interval;
//class real_sp_interval;
//class real_tp_interval;

//=================================
// included dependencies
#include "stdafx.h"
#include <typeinfo>
#include <LEDA/core/tuple.h>
#include <LEDA/numbers/real.h>
#include <LEDA/system/stream.h >
#include <LEDA/core/string.h>
#include "transition_manager.h"
//#include "curve_segment.h"
#include "segment.h"
#include "offset_intersection.h"

#if defined(LEDA_STD_IO_HEADERS)
using std::ifstream;
using std::ofstream;
using std::cin;
using std::cout;
using std::endl;
using std::flush;
#endif

using namespace leda;

typedef two_tuple<real, real> real_point;
typedef two_tuple<real, real_point> real_num_point;

enum side {
	UP,
	DOWN
};

enum point_type_ {
	X_POINT, X_INTERVAL,
	Y_POINT,
	REGULAR
};

struct point_type {};
struct point_type_x_point : point_type {
	real v;

	point_type_x_point(real v) {
		this->v = v;
	}
};
struct point_type_x_interval : point_type {
	real_tp_interval interval;

	point_type_x_interval(real_tp_interval interval) {
		this->interval = interval;
	}
};
struct point_type_top_intersection : point_type {
	real v;
};
struct point_type_botom_intersection : point_type {
	real v;
};
struct point_type_regular : point_type {
	real v;
};

struct chk_exception {};
struct unfollowable_interval : chk_exception {};
struct critical_error : chk_exception {};


class free_space_cell2_base {
public:
	free_space_cell2_base* right;
	free_space_cell2_base* up;

public:
	virtual void solve(
		bool lp1b, real lp1,
		bool lp2b, real lp2,
		bool bp1b, real bp1,
		bool bp2b, real bp2,
		bool& tp1b, real& tp1,
		bool& tp2b, real& tp2,
		bool& rp1b, real& rp1,
		bool& rp2b, real& rp2) =0;

	virtual string matlab_plot_cell(float step) =0;
	virtual string matlab_plot_curve_x(float step, int i) =0;
	virtual string matlab_plot_curve_y(float step, int i) =0;

	//template<class curve> virtual void add_curve_x(curve_segment<curve> seg) =0;
};

//template<class curve>
template<class curve1, class curve2>
class free_space_cell2 : public free_space_cell2_base {
private:
	template<class T> static list<T> remove_duplicates(list<T> lst);

private:
	curve2* segment_x;
	curve1* segment_y;

	real eps;

	two_tuple<real,real> x_minima;
	two_tuple<real,real> y_minima;

	list<interval> to_intervals(segment* sx, list<real>& x, segment* sy, real& y);
	list<interval> to_intervals(segment* sx, real& x, segment* sy, list<real>& y);
	list<interval> to_intervals(segment* sx, list<real_interval*>& x, segment* sy, real& y);
	list<interval> to_intervals(segment* sx, real& x, segment* sy, list<real_interval*>& y);
	
	interval to_interval(segment* sx, real_tp_interval x, segment* sy, real& y);
	interval to_interval(segment* sx, real_sp_interval x, segment* sy, real& y);
	interval to_interval(segment* sx, real&x, segment* sy, real_tp_interval y);
	interval to_interval(segment* sx, real&x, segment* sy, real_sp_interval y);

	list<interval> intersection_int(segment* s1, real t, segment* s2);
	list<interval> intersection_int(segment* s1, segment* s2);

	list<interval> intersection_int2(segment* s1, real t, segment* s2);
	
	list<real> control_points(segment* seg_x, segment* seg_y, real min, real max, bool positive_side = true, bool add_middle = true);
	template<class c1, class c2> list<two_tuple<real, list<interval>>> control_points2(c1* seg_x, c2* seg_y, real min_x, real max_x, bool positive_side = true, bool add_middle = true, bool for_intersection = true, bool debug = false);

	int check_intersection(segment* s1, segment* s2, list<interval>& lst, real& start_point, list<real>& other_points, int follow_index, two_tuple<real, side> intersection_details, bool& right_point,  real& extreme_point, int i = 0, bool debug = false);
	int check_intersection2(segment* s1, segment* s2, list<interval>& lst, real& start_point, list<two_tuple<real, list<interval>>>& other_points, int follow_index, two_tuple<real, side> intersection_details, bool& right_point,  real& extreme_point, int i = 0, bool debug = false);

	template<class c1, class c2> void adjacent(c1* s1, c2* s2, real p1, bool& r1e, real& r1, bool& r2e, real& r2);
	template<class c1, class c2> void adjacent2(c1* s1, c2* s2, real p1, bool& r1e, real& r1, bool& r2e, real& r2);
	template<class c1, class c2> void opposite(c1* s1, c2* s2,real p1, bool& r1e, real& r1, bool& r2e, real& r2);
	template<class c1, class c2> void opposite2(c1* s1, c2* s2,real p1, bool& r1e, real& r1, bool& r2e, real& r2);
	
	void left2top(real p1, bool& r1e, real& r1, bool& r2e, real& r2);
	void left2right(real p1, bool& r1e, real& r1, bool& r2e, real& r2);
	void bottom2right(real p1, bool& r1e, real& r1, bool& r2e, real& r2);
	void bottom2top(real p1, bool& r1e, real& r1, bool& r2e, real& r2);


public:
	free_space_cell2(curve2* x, curve1* y, const real& eps);
	~free_space_cell2();

	void solve(
		bool lp1b, real lp1,
		bool lp2b, real lp2,
		bool bp1b, real bp1,
		bool bp2b, real bp2,
		bool& tp1b, real& tp1,
		bool& tp2b, real& tp2,
		bool& rp1b, real& rp1,
		bool& rp2b, real& rp2);


	string matlab_plot_cell(float step);
	string matlab_plot_curve_x(float step, int i);
	string matlab_plot_curve_y(float step, int i);

	template<class T> list<T> lst_union(list<T>& lst1, list<T>& append);
};

//template<class curve1, class curve2>
//free_space_cell2<curve1, curve2>::free_space_cell(curve_segment<curve1>* x, curve_segment<curve2>* y, const real& eps) {
//	this->segment_x = x;
//	this->segment_y = y;
//	this->eps = eps;
//}

template<class curve1, class curve2>
free_space_cell2<curve1, curve2>::free_space_cell2(curve2* x, curve1* y, const real& eps) {
	this->segment_x = x;
	this->segment_y = y;
	this->eps = eps;
}


template<class curve1, class curve2>
free_space_cell2<curve1, curve2>::~free_space_cell2() {

}

template<class curve1, class curve2>
list<interval> free_space_cell2<curve1, curve2>::to_intervals(segment* s1, list<real>& x, segment* s2, real& y) {
	list<interval> lst;

	for (int i = 0; i < x.size() - 1; i++) {
		real i1 = x.contents(x[i]);
		real i2 = x.contents(x[i+1]);


		real_point rp = real_point(i1, i2);
		int tp = s1->inside(rp, y, s2, eps);

		/*lst.append(interval(tp, tp, rp));*/
		lst.append(interval(tp, rp));
	}
	

	return lst;
}

template<class curve1, class curve2>
list<interval> free_space_cell2<curve1, curve2>::to_intervals(segment* s1, real&x, segment* s2, list<real>& y) {
	list<interval> lst;

	for (int i = 0; i < y.size() - 1; i++) {
		real i1 = y.contents(y[i]);
		real i2 = y.contents(y[i+1]);


		real_point rp = real_point(i1, i2);
		int tp = s1->inside(x, rp, s2, eps);

		lst.append(interval(tp, tp, rp));
	}


	return lst;
}

template<class curve1, class curve2>
list<interval> free_space_cell2<curve1, curve2>::to_intervals(segment* s1, list<real_interval*>& x, segment* s2, real& y) {
	list<interval> lst;

	real_interval* i;
	forall (i, x) {
		if (typeid(*i) == typeid(real_sp_interval)) {
			lst.append(to_interval(s1, (static_cast<real_sp_interval&>(*i)), s2, y));

		}
		else if (typeid(*i) == typeid(real_tp_interval)) {
			lst.append(to_interval(s1, (static_cast<real_tp_interval&>(*i)), s2, y));
		}
	}


	return lst;
}

template<class curve1, class curve2>
list<interval> free_space_cell2<curve1, curve2>::to_intervals(segment* s1, real& x, segment* s2, list<real_interval*>& y) {
	list<interval> lst;

	real_interval* i;
	forall (i, y) {
		if (typeid(*i) == typeid(real_sp_interval)) {
			lst.append(to_interval(s1, x, s2, (static_cast<real_sp_interval&>(*i))));
		}
		else if (typeid(*i) == typeid(real_tp_interval)) {
			lst.append(to_interval(s1, x, s2, (static_cast<real_tp_interval&>(*i))));
		}
	}


	return lst;
}

template<class curve1, class curve2>
interval free_space_cell2<curve1, curve2>::to_interval(segment* s1, real_tp_interval x, segment* s2, real& y) {
	real value1 = x.v1;
	real value2 = x.v2;

	real_point rp = real_point(value1, value2);
	int tp = s1->inside(rp, y, s2, eps);
	return interval(tp, rp);
}

template<class curve1, class curve2>
interval free_space_cell2<curve1, curve2>::to_interval(segment* s1, real& x, segment* s2, real_tp_interval y) {
	real value1 = y.v1;
	real value2 = y.v2;

	real_point rp = real_point(value1, value2);
	int tp = s1->inside(x, rp, s2, eps);
	return interval(tp, rp);
}

template<class curve1, class curve2>
interval free_space_cell2<curve1, curve2>::to_interval(segment* s1, real_sp_interval x, segment* s2, real& y) {
	real value = x.v;
	return interval(3, real_point(value, value));
}

template<class curve1, class curve2>
interval free_space_cell2<curve1, curve2>::to_interval(segment* s1, real& x, segment* s2, real_sp_interval y) {
	real value = y.v;
	return interval(3, real_point(value, value));
}


int sort_cmp(const real& a , const real& b) {
	return (a-b).sign(20);
	//return   (a.to_double() == b.to_double()) ? 0 : (a.to_double() > b.to_double()) ? 1 : -1;
}

// x_intersection_int : segment_y, ty, segment_x
// y_intersection_int : segment_x, tx, segment_y
template<class curve1, class curve2>

list<interval> free_space_cell2<curve1, curve2>::intersection_int2(segment* s1, real t, segment* s2) {
	list<real> intersections = s2->circle_intersection(s1->coordinates(t), eps);
	intersections.push_front(s2->min_t());
	intersections.push_back(s2->max_t());
	//intersections.sort();
	//TODO
	//intersections.sort(&sort_cmp);

	return to_intervals(s2, intersections, s1, t);
}


// x_intersection_int : segment_y, ty, segment_x
// y_intersection_int : segment_x, tx, segment_y
template<class curve1, class curve2>

list<interval> free_space_cell2<curve1, curve2>::intersection_int(segment* s1, real t, segment* s2) {
	list<real_interval*> inters = s2->circle_intersection_int(s1->coordinates(t), eps);
	list<interval> intervals = to_intervals(s2, inters, s1, t);

	// clean up
	real_interval* p; forall (p, inters) delete p;
	return intervals;
}


template<class curve1, class curve2>

list<interval> free_space_cell2<curve1, curve2>::intersection_int(segment* s1, segment* s2) {
	return intersection_int(s1, s1->min_t(), s2);
}

//TODO watch positive or negative side!
template<class curve1, class curve2>

//list<two_tuple<real, list<real_interval*>> free_space_cell2<curve1, curve2>::control_points(segment* seg_x, segment* seg_y, real min_x, real max_x, bool add_middle, bool positive_side) {
list<real> free_space_cell2<curve1, curve2>::control_points(segment* seg_x, segment* seg_y, real min_x, real max_x, bool add_middle, bool positive_side) {
	// test
	control_points2(seg_x, seg_y, min_x, max_x, add_middle, positive_side);

	list<two_tuple<real, real_interval*>> ext_x = seg_x->offset_intersection3((c1*)seg_y, eps);
	list<two_tuple<real, real_interval*>> ext_y = seg_y->offset_intersection3((c2*)seg_x, eps);

	list<real> bottom = seg_x->circle_intersection(seg_y->coordinates(seg_y->min_t()), eps);
	list<real> top = seg_x->circle_intersection(seg_y->coordinates(seg_y->max_t()), eps);

	list<real> lst;

	two_tuple<real, real_interval*> p;
	forall (p, ext_x) {
		if (p.first() > min_x && p.first() < max_x) lst.append(p.first());
		delete p.second();	// clean up
	}

	forall (p, ext_y) {
		real_interval* rp = p.second();
		if (typeid(*rp) == typeid(real_sp_interval)) 
			if ((static_cast<real_sp_interval*>(rp))->v > min_x && (static_cast<real_sp_interval*>(rp))->v < max_x)
				lst.append( (static_cast<real_sp_interval*>(rp))->v );

		delete p.second();	// clean up
	}

	real r;
	forall(r, top) if (r > min_x && r < max_x) lst.append(r);
	forall(r, bottom) if (r > min_x && r < max_x) lst.append(r);

	lst.sort();

	lst.push_front(min_x);
	lst.push_back(max_x);

	if (!positive_side) lst.reverse();

	list<real> lstf = remove_duplicates(lst);

	cout << "ALL: ";
	forall (r, lstf) cout << r.to_double() << " ";
	cout << "\n";


	if (add_middle) {
		list<real> result; result.append(lstf.head());

		for (int i = 1; i < lstf.size(); i++) {
			real r1 = lstf.contents(lstf[i-1]);
			real r2 = lstf.contents(lstf[i]);

			if (r1 != r2) {
				result.append((r1+r2)/2);
			}

			result.append(r2);
		}

		return result;
	}

	return lstf;
}

int sort_cmp2(const two_tuple<real, list<interval>>& a , const two_tuple<real, list<interval>>& b) {
	return a.first() == b.first() ? 0 : a.first() > b.first() ? 1 : -1;
	//return   (a.to_double() == b.to_double()) ? 0 : (a.to_double() > b.to_double()) ? 1 : -1;
}

// can already provide intervals for points of extremes in x direction, otherwise provide empty list
template<class curve1, class curve2>
template<class c1, class c2>
list<two_tuple<real, list<interval>>> free_space_cell2<curve1, curve2>::control_points2(
																						c1* seg_x, 
																						c2* seg_y, 
																						real min_x, 
																						real max_x,
																						bool add_middle = true, 
																						bool positive_side = true,
																						bool for_intersection = true,
																						bool debug = false) {
	list<two_tuple<real, list<interval>>> ret;
	list<interval> tmp;

	segment* seg_xs = static_cast<segment*>(seg_x); segment* seg_ys = static_cast<segment*> (seg_y);
	offset_intersection<c2>* seg_xo = reinterpret_cast<offset_intersection<c2>*> (seg_x); offset_intersection<c1>* seg_yo = reinterpret_cast<offset_intersection<c1>*> (seg_y);


	//list<two_tuple<real, real_interval*>> ext_x = seg_x->offset_intersection3((c1*)seg_y, eps);
	//list<two_tuple<real, real_interval*>> ext_y = seg_y->offset_intersection3((c2*)seg_x, eps);
	list<two_tuple<real, real_interval*>> ext_x = seg_xo->offset_intersection3(static_cast<c1*>(seg_y), eps);
	list<two_tuple<real, real_interval*>> ext_y = seg_yo->offset_intersection3(static_cast<c2*>(seg_x), eps);

	if (debug) {
		cout << "ext_x: " << "\n";
		two_tuple<real, real_interval*> xy; forall(xy, ext_x) {
			cout << xy.first().to_double() << ": ";
			real_interval* s = xy.second();
			if (typeid(*s) == typeid(real_sp_interval))  cout << (static_cast<real_sp_interval*>(s))->v.to_double() << "\n";
			else  cout << (static_cast<real_tp_interval*>(s))->v1.to_double() << " " << (static_cast<real_tp_interval*>(s))->v2.to_double()<<   "\n";
		}
		cout << "\n";

		cout << "ext_y: " << "\n";
		forall(xy, ext_y) {
			cout << xy.first().to_double() << ": ";
			real_interval* s = xy.second();
			if (typeid(*s) == typeid(real_sp_interval))  cout << (static_cast<real_sp_interval*>(s))->v.to_double() << "\n";
			else  cout << (static_cast<real_tp_interval*>(s))->v1.to_double() << " " << (static_cast<real_tp_interval*>(s))->v2.to_double()<<   "\n";
		}
		cout << "\n";
	}

	list<real> bottom = seg_xs->circle_intersection(seg_ys->coordinates(seg_ys->min_t()), eps);
	list<real> top = seg_xs->circle_intersection(seg_ys->coordinates(seg_ys->max_t()), eps);

	list<real> lst;

	two_tuple<real, real_interval*> p;
	
	forall (p, ext_x) {
		real t = p.first();
		
		if (lst.search(t) == 0 && t > min_x && t < max_x) {
			lst.append(t);

			if (typeid(*(p.second())) == typeid(real_sp_interval)) {
				tmp.clear();
				real val = (static_cast<real_sp_interval*>(p.second()))->v;

				if (val == seg_ys->min_t()) tmp.append(to_interval(seg_xs, t, seg_ys, real_sp_interval(val)));
				else tmp.append(to_interval(seg_xs, t, seg_ys, real_tp_interval(seg_ys->min_t(), val)));

				tmp.append(interval(3, real_point(val, val)));

				if (val == seg_ys->max_t()) tmp.append(to_interval(seg_xs, t, seg_ys, real_sp_interval(val)));
				else tmp.append(to_interval(seg_xs, t, seg_ys, real_tp_interval(val, seg_ys->max_t())));

				ret.append(two_tuple<real, list<interval>>(t, tmp));
			}
			else if (typeid(*(p.second())) == typeid(real_tp_interval)) {
				//TODO can remove to_intervals, as all intervals are of type 3
				tmp.clear();
				list<real> intersections;

				two_tuple<real, real_interval*> pt;
				forall (pt, ext_y) {
					real_interval* ri = pt.second();
					// point has to be on ray that goes through point t on x-axis
					if (typeid(*ri) == typeid(real_sp_interval) && t == (static_cast<real_sp_interval*>(ri))->v) {
						intersections.append(pt.first());
					}
					else if (typeid(*ri) == typeid(real_tp_interval) && t >= (static_cast<real_tp_interval*>(ri))->v1 && t <= (static_cast<real_tp_interval*>(ri))->v2) {
						intersections.append(pt.first());
					}
				}

				intersections.sort();
				list<real> intersections_ = remove_duplicates(intersections);
				intersections_.push_front(seg_ys->min_t());
				intersections_.push_back(seg_ys->max_t());
				tmp = to_intervals(seg_ys, intersections_, seg_xs, t);

				ret.append(two_tuple<real, list<interval>>(t, tmp));
			}
		}
		delete p.second();	// clean up
	}

	tmp.clear();

	if (for_intersection) {
		forall (p, ext_y) {
			real_interval* rp = p.second();
			if (typeid(*rp) == typeid(real_sp_interval)) {
				if ((static_cast<real_sp_interval*>(rp))->v > min_x && (static_cast<real_sp_interval*>(rp))->v < max_x &&
					lst.search((static_cast<real_sp_interval*>(rp))->v) == 0) {
					lst.append( (static_cast<real_sp_interval*>(rp))->v );
					ret.append( two_tuple<real, list<interval>>((static_cast<real_sp_interval*>(rp))->v, tmp));
				}
			}
		}
	}
	else if (debug) cout << "profited from not adding " << ext_y.size() << " items!\n";

	forall (p, ext_y) delete p.second();	// clean up

	if (debug) {
		cout << "*************************************************************************\n";
		cout << "ALL0-2: ";
		two_tuple<real, list<interval>> r2;
		forall (r2, ret) cout << r2.first().to_double() << ": [" << r2.second() << "]\n";
		cout << "\n";
		real k; forall(k, lst) cout << k.to_double() << " "; cout <<  "\n"; 
	}
	real r; 
	forall(r, top) {
		if (lst.search(r) == 0 && r > min_x && r < max_x) {
			lst.append( r );
			ret.append(two_tuple<real, list<interval>>(r, tmp));
		}
	}
	forall(r, bottom) {
		if (lst.search(r) == 0 && r > min_x && r < max_x) {
			lst.append( r );
			ret.append(two_tuple<real, list<interval>>(r, tmp));
		}
	}

	if (debug) {
		cout << "*************************************************************************\n";
		cout << "ALL1-2: ";
		two_tuple<real, list<interval>> r2;
		forall (r2, ret) cout << r2.first().to_double() << ": [" << r2.second() << "]\n";
		cout << "\n";
		real k; forall(k, lst) cout << k.to_double() << " "; cout << "\n"; 
	}

	ret.sort(sort_cmp2); // all different, can sort quickly

	ret.push_front(two_tuple<real, list<interval>>(min_x, tmp));
	ret.push_back(two_tuple<real, list<interval>>(max_x, tmp));

	if (!positive_side) {
		ret.reverse();
	}


	if (add_middle) {
		list<two_tuple<real, list<interval>>> result2; result2.append(ret.head());

		for (int i = 1; i < ret.size(); i++) {
			real r1 = ret.contents(ret[i-1]).first();
			real r2 = ret.contents(ret[i]).first();

			if (r1 != r2) {
				result2.append(two_tuple<real, list<interval>>((r1+r2)/2, tmp));
			}

			result2.append(ret.contents(ret[i]));
		}
		
		ret = result2;
	}
	if (debug) {
		cout << "*************************************************************************\n";
		cout << "ALL2-2: ";
		two_tuple<real, list<interval>> r2;
		forall (r2, ret) cout << r2.first().to_double() << ": [" << r2.second() << "]\n";
		cout << "\n";
	}

	return ret;
}



template<class curve1, class curve2>
template<class T> 
list<T> free_space_cell2<curve1, curve2>::remove_duplicates(list<T> lst) {
	if (lst.size() == 0) return lst;

	list<real> lstf;
	lstf.append(lst.pop());
	while (lst.size() > 0) {
		while (lst.size() > 0 && lst.front() == lstf.back()) lst.pop();
		if (lst.size() > 0) lstf.append(lst.pop());
	}

	return lstf;
}


template<class curve1, class curve2>

int free_space_cell2<curve1, curve2>::check_intersection(
													segment* s1,
													segment* s2,
													list<interval>& lst, 
													real& start_point, 
													list<real>& other_points, 
													int follow_index, 
													two_tuple<real, side> intersection_details, 
													bool& extreme_point_exists,
													real& extreme_point,
													int i = 0,
													bool debug = false
													) {

	int intersection_result = -1;

	if (i >= other_points.size()) {
		extreme_point_exists = false;
		return intersection_result;
	}

	if (lst.contents(lst[follow_index]).type == 3) throw unfollowable_interval();
	
	if (debug) cout << "==========================================================\n";
	real k; forall(k, other_points) if (debug) cout << k.to_double() << " ";
	if (debug) cout << "\n";
	list<interval> intvt_p = lst;
	real r = other_points.contents(other_points[i]);
	list<interval> intvt = intersection_int(s1, r, s2);

	list<int> result_ind;
	list<interval> result_type;

	if (debug) cout << "checking " << intvt_p << " -> " << intvt << "\n";
	if (debug) cout << " r : " << start_point.to_double() << " -> " << r.to_double() << "\n";

	if (!transition_manager::contains(intvt_p, intvt, follow_index, result_ind, result_type)) throw critical_error();	//TODO throw exception!
	
	if (debug) cout << intvt_p << " -> " << intvt << "\n";
	if (debug) cout << "results: " << result_ind << ", " << result_type << "\n";
	
	extreme_point_exists = true;

	if (result_ind.size() > 0) {
		//	interval we're following gets divided into one (or several) new intervals	
		real candidate; bool candidate_f = false;
		int id;
		//list<real> candidates; int id;
		forall (id, result_ind) {

			if (intersection_details.second() == UP) {
				if ((intvt.contents(intvt[id]).limits.second() > intersection_details.first() && intersection_result < 1) || 
					(intvt.contents(intvt[id]).limits.second() == intersection_details.first() && id == intvt.size() -1)) {	// if interval is the last one then there is full intersection
					if (debug) cout << "intersection 1" << "\n";
					intersection_result = 1;
				}
				else if (intvt.contents(intvt[id]).limits.second() == intersection_details.first() && intersection_result < 0) {
					if (debug) cout << "intersection 0" << "\n";
					intersection_result = 0;
				}
			}
			else if (intersection_details.second() == DOWN) {
				if ((intvt.contents(intvt[id]).limits.first() < intersection_details.first() && intersection_result < 1) || 
					(intvt.contents(intvt[id]).limits.first() == intersection_details.first() && id == 0)) {				// if interval is the last one then there is full intersection
					if (debug) cout << "intersection 1" << "\n";
					intersection_result = 1;
				}
				else if (intvt.contents(intvt[id]).limits.first() == intersection_details.first() && intersection_result < 0) {
					if (debug) cout << "intersection 0" << "\n";
					intersection_result = 0;
				}
			}

			// interval came to an end
			if (result_type.contents(result_type[id]).type == 3) {
				if (!candidate_f) {
					candidate = r;
					candidate_f = true;
				}
				else if (r > candidate) candidate = r;

				continue;
			}
			else {
				// recursively try to find extreme point of interval at id
				bool rp_; real r_;
				int int_res = check_intersection(s1, s2, result_type, r, other_points, id, intersection_details, rp_, r_, i+1, false);
				if (int_res > intersection_result ) intersection_result = int_res;

				if (!rp_) extreme_point_exists = false;
				if (!candidate_f) {
					candidate = r_;
					candidate_f = true;
				}
				else if (r_ > candidate) candidate = r_;
			}
		}
		if (extreme_point_exists) {
			extreme_point = candidate;
		}
	}

	return intersection_result;
}

template<class curve1, class curve2>

int free_space_cell2<curve1, curve2>::check_intersection2(
													segment* s1,
													segment* s2,
													list<interval>& lst, 
													real& start_point, 
													list<two_tuple<real, list<interval>>>& other_points, 
													int follow_index, 
													two_tuple<real, side> intersection_details, 
													bool& extreme_point_exists,
													real& extreme_point,
													int i = 0,
													bool debug = false
													) {

	int intersection_result = -1;

	if (i >= other_points.size()) {
		extreme_point_exists = false;
		return intersection_result;
	}

	if (lst.contents(lst[follow_index]).type == 3) throw unfollowable_interval();
	
	if (debug) cout << "==========================================================\n";
	two_tuple<real, list<interval>> k; forall(k, other_points) if (debug) cout << k.first().to_double() << " ";
	if (debug) cout << "\n";
	list<interval> intvt_p = lst;
	two_tuple<real, list<interval>> r = other_points.contents(other_points[i]);
	list<interval> intvt = (r.second().size() != 0) ? r.second() : intersection_int(s1, r.first(), s2);

	list<int> result_ind;
	list<interval> result_type;

	if (debug) cout << "checking " << intvt_p << " -> " << intvt << "\n";
	if (debug) cout << " r : " << start_point.to_double() << " -> " << r.first().to_double() << "\n";

	if (!transition_manager::contains(intvt_p, intvt, follow_index, result_ind, result_type)) throw critical_error();	//TODO throw exception!
	
	if (debug) cout << intvt_p << " -> " << intvt << "\n";
	if (debug) cout << "results: " << result_ind << ", " << result_type << "\n";
	
	extreme_point_exists = true;

	if (result_ind.size() > 0) {
		//	interval we're following gets divided into one (or several) new intervals	
		real candidate; bool candidate_f = false;
		int id;
		//list<real> candidates; int id;
		forall (id, result_ind) {

			if (intersection_details.second() == UP) {
				if ((intvt.contents(intvt[id]).limits.second() > intersection_details.first() && intersection_result < 1) || 
					(intvt.contents(intvt[id]).limits.second() == intersection_details.first() && id == intvt.size() -1)) {	// if interval is the last one then there is full intersection
					if (debug) cout << "intersection 1" << "\n";
					intersection_result = 1;
				}
				else if (intvt.contents(intvt[id]).limits.second() == intersection_details.first() && intersection_result < 0) {
					if (debug) cout << "intersection 0" << "\n";
					intersection_result = 0;
				}
			}
			else if (intersection_details.second() == DOWN) {
				if ((intvt.contents(intvt[id]).limits.first() < intersection_details.first() && intersection_result < 1) || 
					(intvt.contents(intvt[id]).limits.first() == intersection_details.first() && id == 0)) {				// if interval is the last one then there is full intersection
					if (debug) cout << "intersection 1" << "\n";
					intersection_result = 1;
				}
				else if (intvt.contents(intvt[id]).limits.first() == intersection_details.first() && intersection_result < 0) {
					if (debug) cout << "intersection 0" << "\n";
					intersection_result = 0;
				}
			}

			// interval came to an end
			if (result_type.contents(result_type[id]).type == 3) {
				if (!candidate_f) {
					candidate = r.first();
					candidate_f = true;
				}
				else if (r.first() > candidate) candidate = r.first();

				continue;
			}
			else {
				// recursively try to find extreme point of interval at id
				bool rp_; real r_;
				int int_res = check_intersection2(s1, s2, result_type, r.first(), other_points, id, intersection_details, rp_, r_, i+1, debug);
				if (int_res > intersection_result ) intersection_result = int_res;

				if (!rp_) extreme_point_exists = false;
				if (!candidate_f) {
					candidate = r_;
					candidate_f = true;
				}
				else if (r_ > candidate) candidate = r_;
			}
		}
		if (extreme_point_exists) {
			extreme_point = candidate;
		}
	}

	return intersection_result;
}

template<class curve1, class curve2>
template<class T>
list<T> free_space_cell2<curve1, curve2>::lst_union(list<T>& lst1, list<T>& append) {
	list<T> lst = lst1;

	T i;
	forall (i, append) {
		if (lst1.search(i) == nil)
			lst.append(i);
	}

	return lst;
}


template<class curve1, class curve2>
template<class c1, class c2>
void free_space_cell2<curve1, curve2>::adjacent(c1* s1, c2* s2, real p1, bool& r1e, real& r1, bool& r2e, real& r2) {
	two_tuple<real,side> intd = two_tuple<real, side>(p1, DOWN);
	bool epe; real ep; list<real> cp;

part1:
	list<interval> sintervals = intersection_int(s1, s1->min_t(), s2);
	int id = 0;
	for (id = 0; id < sintervals.size(); id++) {
		interval in = sintervals.contents(sintervals[id]);
		if (in.type == 2 && in.limits.first() >= p1) goto f2;
	}
f1:
	// no interval of type 2 upwards from p1
	r1e = true;
	r1 = s1->min_t();
	goto part2;

f2:	// interval of type 2 upwards from p1
	cp = control_points(s1, s2, s1->min_t(), s1->max_t(), true);
	
	int result = check_intersection(s1, s2, sintervals, cp.pop(), cp, id, intd, epe, ep);
	if (result == 1) {
		r1e = r2e = false; return;
	}
	else if (epe) {
		r1e = true; r1 = ep;
	}
	else {
		r1e = r2e = false; return;
	}

part2:
	list<interval> tintervals = intersection_int(s2, s2->max_t(), s1);
	for (id = 0; id < tintervals.size(); id++) {
		interval in = tintervals.contents(tintervals[id]);
		if (in.type == 2) goto s2;
	}
s1:
	// no interval of type 2
	r2e = false; return; 
s2:
	real_point in_limits = tintervals.contents(tintervals[id]).limits;
	real middle = (in_limits.first() + in_limits.second()) / 2;
	list<real> cp2 = control_points(s1, s2, s1->min_t(), middle, true);
	cp2.reverse();	// go from right to left
	//list<real> cp2 = control_points(s1, s2, s1->min_t(), middle, true, false); //reverse
	list<interval> mintervals = intersection_int(s1, middle, s2);
	// check if first interval outside intd!
	if (mintervals.back().limits.first() < p1) {
		// intersection with p1 already occurs
		result = 1;
	}
	else result = check_intersection(s1, s2, mintervals, cp2.pop(), cp2, mintervals.size()-1, intd, epe, ep);	// following the top interval

	if (result == 1 || !epe) {
		// cannot get to the right side of the obstacle OR
		// same obstacle as in part1
		r2e = false;	
	}
	else {	// result != 1, epe = true
		// check intersection from middle to right
		cp2 = control_points(s1, s2, middle, s1->max_t(), true);
		result = check_intersection(s1, s2, mintervals, cp2.pop(), cp2, mintervals.size()-1, intd, epe, ep);	// following the top interval
		if (result == 1) {
			r2e = false; // cannot get to the right side of the obstacle
		}
		else if (epe) {
			r2e = true; r2 = ep;
		}
		else {
			r2e = false;	// obstacle has no right point (reaches all the way to s1->max_t())
		}
	}
}

template<class curve1, class curve2>
template<class c1, class c2>
void free_space_cell2<curve1, curve2>::adjacent2(c1* s1, c2* s2, real p1, bool& r1e, real& r1, bool& r2e, real& r2) {
	two_tuple<real,side> intd = two_tuple<real, side>(p1, DOWN);
	bool epe; real ep; list<two_tuple<real, list<interval>>> cp;

part1:
	list<interval> sintervals = intersection_int(s1, s1->min_t(), s2);
	int id = 0;
	for (id = 0; id < sintervals.size(); id++) {
		interval in = sintervals.contents(sintervals[id]);
		if (in.type == 2 && in.limits.first() >= p1) goto f2;
	}
f1:
	// no interval of type 2 upwards from p1
	r1e = true;
	r1 = s1->min_t();
	goto part2;

f2:	// interval of type 2 upwards from p1
	cp = control_points2(s1, s2, s1->min_t(), s1->max_t(), true);
	
	int result = check_intersection2(s1, s2, sintervals, cp.pop().first(), cp, id, intd, epe, ep);
	if (result == 1) {
		r1e = r2e = false; return;
	}
	else if (epe) {
		r1e = true; r1 = ep;
	}
	else {
		r1e = r2e = false; return;
	}

part2:
	list<interval> tintervals = intersection_int(s2, s2->max_t(), s1);
	for (id = 0; id < tintervals.size(); id++) {
		interval in = tintervals.contents(tintervals[id]);
		if (in.type == 2) goto s2;
	}
s1:
	// no interval of type 2
	r2e = false; return; 
s2:
	real_point in_limits = tintervals.contents(tintervals[id]).limits;
	real middle = (in_limits.first() + in_limits.second()) / 2;
	//list<real> cp2 = control_points(s1, s2, s1->min_t(), middle, true);
	//cp2.reverse();	// go from right to left
	cp = control_points2(s1, s2, s1->min_t(), middle, true, false); //reverse
	list<interval> mintervals = intersection_int(s1, middle, s2);
	// check if first interval outside intd!
	if (mintervals.back().limits.first() < p1) {
		// intersection with p1 already occurs
		result = 1;
	}
	else result = check_intersection2(s1, s2, mintervals, cp.pop().first(), cp, mintervals.size()-1, intd, epe, ep);	// following the top interval

	if (result == 1 || !epe) {
		// cannot get to the right side of the obstacle OR
		// same obstacle as in part1
		r2e = false;	
	}
	else {	// result != 1, epe = true
		// check intersection from middle to right
		cp = control_points2(s1, s2, middle, s1->max_t(), true);
		result = check_intersection2(s1, s2, mintervals, cp.pop().first(), cp, mintervals.size()-1, intd, epe, ep);	// following the top interval
		if (result == 1) {
			r2e = false; // cannot get to the right side of the obstacle
		}
		else if (epe) {
			r2e = true; r2 = ep;
		}
		else {
			r2e = false;	// obstacle has no right point (reaches all the way to s1->max_t())
		}
	}
}

template<class curve1, class curve2>
template<class c1, class c2>
void free_space_cell2<curve1, curve2>::opposite(c1* s1, c2* s2, real p1, bool& r1e, real& r1, bool& r2e, real& r2) {
	two_tuple<real,side> intd = two_tuple<real, side>(p1, DOWN);
	bool epe; real ep; list<real> cp;

part1:
	list<interval> sintervals = intersection_int(s1, s1->min_t(), s2);
	int id = 0;
	for (id = 0; id < sintervals.size(); id++) {
		interval in = sintervals.contents(sintervals[id]);
		if (in.type == 2 && in.limits.first() >= p1) goto f1;
	}
	// no interval of type 2
	goto f2_;

f1:	// interval of type 2
	cp = control_points(s1, s2, s1->min_t(), s1->max_t(), true);
	
	int result = check_intersection(s1, s2, sintervals, cp.pop(), cp, id, intd, epe, ep);
	if (result == 1) {
		r1e = r2e = false; return;
	}
//	else {
f2_:
	list<interval> p1intervals = intersection_int(s2, p1, s1);
	for (id = 0; id < p1intervals.size(); id++) {
		interval in = p1intervals.contents(p1intervals[id]);
		if (in.type == 2) goto f2;
	}
	// no interval of type 2
	r1e = true; r1 = p1; goto part2;

f2:	// interval of type 2
	// control poins upwards of p1
	cp = control_points(s2, s1, p1, s2->max_t(), true);
	check_intersection(s2, s1, p1intervals, cp.pop(), cp, id, intd, epe, ep);
	if (epe) {
		r1e = true; r1 = ep;
	}
	else {
		r1e = false;
	}
//	}

part2:
	list<interval> rintervals = intersection_int(s1, s1->max_t(), s2);
	for (id = 0; id < rintervals.size(); id++) {
		interval in = rintervals.contents(rintervals[id]);
		if (in.type == 2 && in.limits.first() >= p1) goto f3;
	}

	// no interval of type 2 upwards from p1
	r2e = false; return;

f3:	// interval of type 2 upwards from p1
	real_point in_limits = rintervals.contents(rintervals[id]).limits;
	real middle = (in_limits.first() + in_limits.second()) / 2;
	list<real> cp2 = control_points(s2, s1, middle, s2->max_t(), true);
	list<interval> mintervals = intersection_int(s2, middle, s1);
	check_intersection(s2, s1, mintervals, cp2.pop(), cp2, mintervals.size()-1, intd, epe, ep);	// following the right interval
	if (!epe) r2e = false;
	else {
		r2e = true; r2 = ep;
	}
}

template<class curve1, class curve2>
template<class c1, class c2>
void free_space_cell2<curve1, curve2>::opposite2(c1* s1, c2* s2, real p1, bool& r1e, real& r1, bool& r2e, real& r2) {
	two_tuple<real,side> intd = two_tuple<real, side>(p1, DOWN);
	bool epe; real ep; list<two_tuple<real, list<interval>>> cp;

part1:
	list<interval> sintervals = intersection_int(s1, s1->min_t(), s2);
	int id = 0;
	for (id = 0; id < sintervals.size(); id++) {
		interval in = sintervals.contents(sintervals[id]);
		if (in.type == 2 && in.limits.first() >= p1) goto f1;
	}
	// no interval of type 2
	goto f2_;

f1:	// interval of type 2
	cp = control_points2(s1, s2, s1->min_t(), s1->max_t(), true);
	
	int result = check_intersection2(s1, s2, sintervals, cp.pop().first(), cp, id, intd, epe, ep);
	if (result == 1) {
		r1e = r2e = false; return;
	}
//	else {
f2_:
	list<interval> p1intervals = intersection_int(s2, p1, s1);
	for (id = 0; id < p1intervals.size(); id++) {
		interval in = p1intervals.contents(p1intervals[id]);
		if (in.type == 2) goto f2;
	}
	// no interval of type 2
	r1e = true; r1 = p1; goto part2;

f2:	// interval of type 2
	// control poins upwards of p1
	cp = control_points2(s2, s1, p1, s2->max_t(), true, true, false);
	check_intersection2(s2, s1, p1intervals, cp.pop().first(), cp, id, intd, epe, ep);
	if (epe) {
		r1e = true; r1 = ep;
	}
	else {
		r1e = false;
	}
//	}

part2:
	list<interval> rintervals = intersection_int(s1, s1->max_t(), s2);
	for (id = 0; id < rintervals.size(); id++) {
		interval in = rintervals.contents(rintervals[id]);
		if (in.type == 2 && in.limits.first() >= p1) goto f3;
	}

	// no interval of type 2 upwards from p1
	r2e = false; return;

f3:	// interval of type 2 upwards from p1
	real_point in_limits = rintervals.contents(rintervals[id]).limits;
	real middle = (in_limits.first() + in_limits.second()) / 2;
	cp = control_points2(s2, s1, middle, s2->max_t(), true, true, false);
	list<interval> mintervals = intersection_int(s2, middle, s1);
	check_intersection2(s2, s1, mintervals, cp.pop().first(), cp, mintervals.size()-1, intd, epe, ep);	// following the right interval
	if (!epe) r2e = false;
	else {
		r2e = true; r2 = ep;
	}
}

template<class curve1, class curve2>
void free_space_cell2<curve1, curve2>::left2top(real p1, bool& r1e, real& r1, bool& r2e, real& r2) {
	adjacent2(segment_x, segment_y, p1, r1e, r1, r2e, r2);
}

template<class curve1, class curve2>
void free_space_cell2<curve1, curve2>::left2right(real p1, bool& r1e, real& r1, bool& r2e, real& r2) {
	opposite2(segment_x, segment_y, p1, r1e, r1, r2e, r2);
}

template<class curve1, class curve2>
void free_space_cell2<curve1, curve2>::bottom2right(real p1, bool& r1e, real& r1, bool& r2e, real& r2) {
	adjacent2(segment_y, segment_x, p1, r1e, r1, r2e, r2);
}

template<class curve1, class curve2>
void free_space_cell2<curve1, curve2>::bottom2top(real p1, bool& r1e, real& r1, bool& r2e, real& r2) {
	opposite2(segment_y, segment_x, p1, r1e, r1, r2e, r2);
}


template<class curve1, class curve2>
void free_space_cell2<curve1, curve2>::solve(
											bool lp1b, real lp1,
											bool lp2b, real lp2,
											bool bp1b, real bp1,
											bool bp2b, real bp2,
											bool& tp1b, real& tp1,
											bool& tp2b, real& tp2,
											bool& rp1b, real& rp1,
											bool& rp2b, real& rp2) {

	tp1b = tp2b = rp1b = rp2b = false;

	real p1, p2; bool p1b, p2b;

	// LEFT TO TOP //
	if (lp1b) {
		left2top(lp1, p1b, p1, p2b, p2);
		if (!tp1b && p1b) {tp1 = p1; tp1b = true;}
		else if (tp1b && p1b && p1 < tp1) tp1 = p1;
		if (!tp2b && p2b) {tp2 = p2; tp2b = true;}
		else if (tp2b && p2b && p2 < tp2) tp2 = p2;
	}

	if (lp2b) {
		left2top(lp2, p1b, p1, p2b, p2);
		if (!tp1b && p1b) {tp1 = p1; tp1b = true;}
		else if (tp1b && p1b && p1 < tp1) tp1 = p1;
		if (!tp2b && p2b) {tp2 = p2; tp2b = true;}
		else if (tp2b && p2b && p2 < tp2) tp2 = p2;
	}

	// LEFT TO RIGHT //
	if (lp1b) {
		left2right(lp1, p1b, p1, p2b, p2);
		if (!rp1b && p1b) {rp1 = p1; rp1b = true;}
		else if (rp1b && p1b && p1 < rp1) rp1 = p1;
		if (!rp2b && p2b) {rp2 = p2; rp2b = true;}
		else if (rp2b && p2b && p2 < rp2) rp2 = p2;
	}

	if (lp2b) {
		left2right(lp2, p1b, p1, p2b, p2);
		if (!rp1b && p1b) {rp1 = p1; rp1b = true;}
		else if (rp1b && p1b && p1 < rp1) rp1 = p1;
		if (!rp2b && p2b) {rp2 = p2; rp2b = true;}
		else if (rp2b && p2b && p2 < rp2) rp2 = p2;
	}


	// BOTTOM TO RIGHT //
	if (bp1b) {
		bottom2right(bp1, p1b, p1, p2b, p2);
		if (!rp1b && p1b) {rp1 = p1; rp1b = true;}
		else if (rp1b && p1b && p1 < rp1) rp1 = p1;
		if (!rp2b && p2b) {rp2 = p2; rp2b = true;}
		else if (rp2b && p2b && p2 < rp2) rp2 = p2;
	}

	if (bp2b) {
		bottom2right(bp2, p1b, p1, p2b, p2);
		if (!rp1b && p1b) {rp1 = p1; rp1b = true;}
		else if (rp1b && p1b && p1 < rp1) rp1 = p1;
		if (!rp2b && p2b) {rp2 = p2; rp2b = true;}
		else if (rp2b && p2b && p2 < rp2) rp2 = p2;
	}


	// BOTTOM TO TOP //
	if (bp1b) {
		bottom2top(bp1, p1b, p1, p2b, p2);
		if (!tp1b && p1b) {tp1 = p1; tp1b = true;}
		else if (tp1b && p1b && p1 < tp1) tp1 = p1;
		if (!tp2b && p2b) {tp2 = p2; tp2b = true;}
		else if (tp2b && p2b && p2 < tp2) tp2 = p2;
	}

	if (bp2b) {
		bottom2top(bp2, p1b, p1, p2b, p2);
		if (!tp1b && p1b) {tp1 = p1; tp1b = true;}
		else if (tp1b && p1b && p1 < tp1) tp1 = p1;
		if (!tp2b && p2b) {tp2 = p2; tp2b = true;}
		else if (tp2b && p2b && p2 < tp2) tp2 = p2;
	}

}



template<class curve1, class curve2>
string free_space_cell2<curve1, curve2>::matlab_plot_cell(float step) {
	string var_x = "(" + segment_x->x_from_t("x") + ")";
	string var_y = "(" + segment_y->x_from_t("y") + ")";

	leda::string_ostream s;
	s << "[x,y]=meshgrid(" 
		<< segment_x->min_t().to_double() << ":" << step << ":" << segment_x->max_t().to_double()
		<< ", "
		<< segment_y->min_t().to_double() << ":" << step << ":" << segment_y->max_t().to_double()
		<< ");\n";

	s << "g = " << segment_y->matlab_function(var_y) << ";\n";
	s << "f = " << segment_x->matlab_function(var_x) << ";\n";
	s << "r = sqrt((g - f).^2 + (" << var_x << " - " << var_y << ").^2);\n";
	s << "contourf(x, y, r, [" << eps.to_double() << " " << eps.to_double() << "]);\n";
	s << "set(gca,'YTick',[])\n";
	s << "set(gca,'XTick',[])\n";
	return s.str();
}


template<class curve1, class curve2>
string free_space_cell2<curve1, curve2>::matlab_plot_curve_x(float step, int i) {
	leda::string_ostream s_;
	s_ << "x" << i;
	string var_x = "(" + segment_x->x_from_t(s_.str()) + ")";

	real x1 = segment_x->t_to_x(segment_x->min_t());
	real x2 = segment_x->t_to_x(segment_x->max_t());

	double x1d = min(x1.to_double(), x2.to_double());
	double x2d = max(x1.to_double(), x2.to_double());

	leda::string_ostream s;
	s << "x" << i << "=" << x1d << ":" << step << ":" << x2d << ";\n";
	s << "y" << i << "=" << segment_x->matlab_function(var_x) << ";\n";
	return s.str();
}

template<class curve1, class curve2>
string free_space_cell2<curve1, curve2>::matlab_plot_curve_y(float step, int i) {
	leda::string_ostream s_;
	s_ << "x" << i;
	string var_x = "(" + segment_y->x_from_t(s_.str()) + ")";

	real x1 = segment_y->t_to_x(segment_y->min_t());
	real x2 = segment_y->t_to_x(segment_y->max_t());

	double x1d = min(x1.to_double(), x2.to_double());
	double x2d = max(x1.to_double(), x2.to_double());

	leda::string_ostream s;
	s << "x" << i << "=" << x1d << ":" << step << ":" << x2d << ";\n";
	s << "y" << i << "=" << segment_y->matlab_function(var_x) << ";\n";
	return s.str();
}

#endif