#include "stdafx.h"

#include <LEDA/internal/std/math.h>
#include <LEDA/internal/std/assert.h>

#include <LEDA/numbers/real.h>
#include <LEDA/numbers/integer.h>
#include <LEDA/system/basic.h>

#include "curve_segment.h"
#include "free_space_cell.h"
#include "straight_line.h"

#include "transition_manager.h"

using namespace leda;
#if defined(LEDA_STD_IO_HEADERS)
using std::ifstream;
using std::ofstream;
using std::cin;
using std::cout;
using std::endl;
using std::flush;
#endif

#undef LEDA_VECTOR
#define LEDA_VECTOR growing_array

inline void list_print(const string& name, const list<int>& L) {
  cout << name << ": " << L << "\n";
}

inline void inc(int& i) {
	i+=10;
}

int _tmain(int argc, _TCHAR* argv[])
{
  real x;
  x=(sqrt(real(17))-sqrt(real(12)));
  x*=(sqrt(real(17))+sqrt(real(12)));
  x-=5;
  std::cout << x.to_double();
  if (x==0) std::cout << "x==0\n";
  else std::cout << "x!=0\n";

  double xd;
  xd=(sqrt((double)17)-sqrt((double)12));
  xd*=(sqrt((double)17)+sqrt((double)12));
  xd-=5;
  if (xd==0) std::cout << "xd==0\n";
  else std::cout << "xd!=0\n";

  curve_segment<straight_line>* s = new straight_line(real_point(-20,0), real_point(100,0));
  //list<real>* points = s->circle_intersection(real_point(0,0), 1.564);

  straight_line* s2 = new straight_line(real_point(-11,10), real_point(9,-10));
  list<two_tuple<real,real>> points = s->offset_intersection(s2, 1);
 // list<two_tuple<real_point,Interval>>* points_e = s->offset_intersection2(s2, 1);


  cout << "offset_intersection: " << points.size() << "\n";

  two_tuple<real,real> p;

  forall(p, points) {
	  cout << p << "\n";
	}

  cout << s2->inside(0.1, two_tuple<real,real>(0.2,0.25), s, 0.1);


  /*--------------------------*/

  straight_line* seg1 = new straight_line(real_point(-11.8,0), real_point(10,0));
  //straight_line* seg2 = new straight_line(real_point(-1,10), real_point(1,-10));
  straight_line* seg2 = new straight_line(real_point(-10,1), real_point(10,1));

  list<real> points2 = seg1->circle_intersection(seg2->coordinates(0), 1);
  cout << "intersections:\n";
  real p2;
  forall(p2, points2) {
	  cout << p2 << "\n";
	}


  list<real>* points3 = new list<real>();
  points3->append(real(56));
  points3->append(real(3.12));
  points3->append(real(5));
  points3->append(real(4));
  points3->append(real(3.11));
  points3->append(real(6));
  points3->append(real(9));
  points3->append(real(1));
  points3->append(real(-20));
  points3->append(real(345));
  points3->append(real(23));

  points3->sort();
  cout << "sorted" << "\n";
  forall(p2, *points3) {
	  cout << p2 << "\n";
	}


  free_space_cell<straight_line>* cell = new free_space_cell<straight_line>(seg1, seg2, 2);

  cout << "cell intersections x" << "\n";

  list<real> points4 = cell->x_intersections();

  forall(p2, points4) {
	  cout << p2 << "\n";
	}

  cout << "cell intersections y" << "\n";

  list<real> points5 = cell->y_intersections();

  forall(p2, points5) {
	  cout << p2 << "\n";
	}



  straight_line* seg3 = new straight_line(real_point(1,0), real_point(10,0));
  straight_line* seg4 = new straight_line(real_point(-10,-10), real_point(10,10));

  //straight_line* seg3 = new straight_line(real_point(-12,0), real_point(20,0));
  //straight_line* seg4 = new straight_line(real_point(-10,1), real_point(10,1));

  cout << "seg3, seg4: " << ((new free_space_cell<straight_line>(seg3, seg4, sqrt(real(2))/2))->y_intersections()) << "\n";
  //list<interval>* lstx = ((new free_space_cell<straight_line>(seg3, seg4, sqrt(real(2))/2))->y_intersections_int());
  free_space_cell<straight_line>* fsc = new free_space_cell<straight_line>(seg3, seg4, sqrt(real(2))/2);
  list<interval> lstx = fsc->intersection_int(fsc->segment_x, 1, fsc->segment_y);

  cout << fsc->segment_x->min_t() << "\n";

  interval i1 = lstx.pop();
  cout << i1.limits << " - " << i1.type << "\n";
  interval i2 = lstx.pop();
  cout << i2.limits << " - " << i2.type << "\n";
  interval i3 = lstx.pop();
  cout << i3.limits << " - " << i3.type << "\n";

  //straight_line sl1 = straight_line(real_point(-2,0), real_point(20,0));
  //straight_line sl2 = straight_line(real_point(-2,0.5), real_point(10.5,0.5));

  //straight_line sl2 = straight_line(real_point(sqrt(real(2))/2,sqrt(real(2))/2), real_point(3,3));
  
  straight_line sl2 = straight_line(real_point(-3,-3), real_point(sqrt(real(2))/2,sqrt(real(2))/2));
  straight_line sl1 = straight_line(real_point(-1,0), real_point(4,0));

  //straight_line sl1 = straight_line(real_point(-0,0), real_point(20,0));
  //straight_line sl2 = straight_line(real_point(-2,1), real_point(10.5,1));

  /*
  TODO

  straight_line sl1 = straight_line(real_point(-2,0), real_point(20,0));
  straight_line sl2 = straight_line(real_point(-2,1), real_point(10.5,1));

  at 0 -> no right point
  at 1 -> -2
  at 2 -> 10.5


  */



  //free_space_cell<straight_line> fsc2 = free_space_cell<straight_line>(&sl1, &sl2, 3);
  free_space_cell<straight_line> fsc2 = free_space_cell<straight_line>(&sl1, &sl2, 1);

  /*
  free_space_cell<straight_line> fsc2 = free_space_cell<straight_line>(
												new straight_line(real_point(-2,0), real_point(10,0)),
												//new straight_line(real_point(-2,0), real_point(10,0)),
												new straight_line(real_point(-2,0.5), real_point(10.5,0.5)),
												//1);
												3);
												*/
	/*								
    free_space_cell<straight_line> fsc2 = free_space_cell<straight_line>(
												new straight_line(real_point(-1,-1), real_point(10,10)),
												//new straight_line(real_point(-2,0), real_point(10,0)),
												new straight_line(real_point(-10,-9), real_point(20,21)),
												//1);
												1/sqrt(real(2)));
	*/

  cout << "\n\n\n";

  real r;
  int i = 0;
  list<real> lst_;
  list<real> lst_2 = fsc2.test_intersections_x();

  cout << "initial list " << lst_2 << "\n";

  for (int i = 0; i < lst_2.size() -1; i++) {
	  real r1 = lst_2.contents(lst_2[i]);
	  real r2 = lst_2.contents(lst_2[i+1]);
	  lst_.append(r1);
	  if (r1 != r2) {
		  lst_.append((r1+r2)/2);
		}
	}
  lst_.append(lst_2.contents(lst_2[lst_2.size()-1]));

//  transition_manager man_;

  //forall(r, lst_) {
  i = 0;
  list<interval> intv_p = fsc2.y_intersections_int(lst_.contents(lst_[i]));
  /*
  list<interval> intvt_p; interval in_;

  int count = 0;
  forall (in_, intv_p) {
	intvt_p.append(interval(in_.type, in_.behaves_as, count));
	count++;
  }
  */
  two_tuple<real, side> int_tt = two_tuple<real, side>(-2, DOWN);
  real rp; 
  /*
  if (fsc2.get_right_point(intv_p, lst_.pop_front(), 0,lst_, rp, 0, &int_tt))
	cout << "right point is " << rp << "\n";
  else
	  cout << "there is no right point!\n";

  cout << "\n" << "------------------------------------------" << "\n"  << "------------------------------------------" << "\n" ;
  */
  bool rp_; real extreme_point;  
  int result = fsc2.check_intersection_x(intv_p, lst_.pop_front(), lst_, 0, int_tt, rp_, extreme_point);

  cout << "result: " << result << " ";
  if (!rp_)
	  cout << "there is no extreme point." << "\n";
  else
	  cout << "extreme point = " << extreme_point.to_double() << "\n";


  list<interval> three_test; 
  three_test.append(interval(3,2,0));
  three_test.append(interval(3,1,1));
  //three_test.append(interval(3,1,2));
  list<list<interval>> three_test_res;
  transition_manager::three2three(three_test, three_test_res);
  //cout << "three res: " << three_test_res << "\n";
  list<interval> temp_three;
  forall(temp_three, three_test_res) {
	   cout << "three res: " << temp_three << "\n";
	}

	  /*
  for (int j = 1; j < lst_.size(); j++) {
		
	  real r = lst_.contents(lst_[i]);
	  list<interval> intv = fsc2.y_intersections_int(r);
	  
	  cout << i << ": " << r << "\n";
	  
	  interval in;
	  forall(in, intv) {
		  cout << "[" << in.limits.first().to_double() << "," << in.limits.second().to_double() << "]";
		  cout << " - " << in.type << ": " << in.behaves_as << "\n";
		}

	  list<interval> intvt; interval in_;
	  int count = 0;
	  forall (in_, intv) {
		  intvt.append(interval(in_.type, in_.behaves_as, count));
		  count++;
		}
	  
	  i++;
	    list<int> result_ind_;
		list<interval> result_type_;
	  cout << intvt_p << " -> " << intvt << "\n";
	  cout << "is outcome in: " << man_.contains(intvt_p, intvt, 2, result_ind_, result_type_) << "\n";
	  cout << "results: " << result_ind_ << ", " << result_type_ << "\n";
	  cout << "\n";



	  intvt_p = intvt;
	}
	*/
  
  /*

  list<interval> intervals_1 = fsc2.y_intersections_int(-1);
  list<interval> intervals_2 = fsc2.y_intersections_int(1);
  list<two_tuple<int, int>> input;
  if (fsc2.transform(intervals_1, 0, input, intervals_2)) cout << "YES" << "\n";


  //fsc2.test_intersections_x();

  list<interval> ilst;
  interval in1; in1.type = IN | OUT; in1.behaves_as = IN | OUT;
  interval in2; in2.type = IN | OUT; in2.behaves_as = IN | OUT;
  interval in3; in3.type = IN; in3.behaves_as = IN | OUT;
  ilst.append(in1); ilst.append(in2); ilst.append(in3);
  
  list<interval> ilst2;
  while (fsc2.fix_interval(ilst, ilst2)) {
	  ilst = ilst2;
	  ilst2.clear();
	}
  cout << "\n";
  cout << "\n";
  forall(in1, ilst) {
	  cout << in1.type << " - " << in1.behaves_as << "\n";
	}



  straight_line sl5 = straight_line(real_point(-1,0), real_point(10,0));
  straight_line sl6 = straight_line(real_point(-1,1), real_point(-20,1));


  //free_space_cell<straight_line> fsc2 = free_space_cell<straight_line>(&sl1, &sl2, 3);
  free_space_cell<straight_line> fsc3 = free_space_cell<straight_line>(&sl5, &sl6, 1);
  list<interval> intv2 = fsc3.y_intersections_int(-1);
    cout << "\n";
  cout << "\n";
  forall(in1, intv2) {
	  cout << in1.type << " - " << in1.behaves_as << "\n";
	}

  fsc2.test2();


  list<int> lst_test;
  list<int> test1;
  test1.append(1); test1.append(2);
  list<int> test2 = test1;
  lst_test.conc(test1);

  test2.apply(inc);
  lst_test.merge(test2);


  list_print("B",lst_test);


  transition_manager man;
  list<list<interval>> lst_tp;
  man.transition(interval(1, 1, 0), lst_tp);

  cout << "first\n";

  cout << "\n";
  list<interval> itinput; itinput.append(interval(3,2,0)); itinput.append(interval(1,1,1)); itinput.append(interval(3,2,2));
  list<interval> outcome; outcome.append(interval(3,1,1)); outcome.append(interval(3,2,2));

  list<int> result_ind;
  list<interval> result_type;

  cout << "is outcome in: " << man.contains(itinput, outcome, 1, result_ind, result_type) << "\n";
  cout << "results: " << result_ind << ", " << result_type << "\n";

  list<list<interval>> itoutput;
  man.translation(itinput, 0, itoutput);
  list<interval> tmp_;
  forall(tmp_, itoutput) {
	  cout << tmp_ << "\n";
	}

  cout << "second\n";

  list<list<interval>> itoutput2, itoutput3;
  man.posibilities(itinput, itoutput2);
  man.filter(itoutput2, itoutput3);
  cout << "size2: " << itoutput3.size() << "\n";
  forall(tmp_, itoutput3) {
	  cout << tmp_ << "\n";
	}
  

  list<interval> join_test, join_output;
  join_test.append(interval(1,1,0));
  join_test.append(interval(1,1,1));
  join_test.append(interval(3,1,2));
  join_test.append(interval(3,2,3));

  man.join(join_test, 0, join_output);

  cout << "\n" << join_output << "\n";

  list<interval> itinput2; 
  list<list<interval>> itoutput4, itoutput5;
  man.posibilities(itinput2, itoutput4);
  man.filter(itoutput4, itoutput5);
  cout << "size3: " << itoutput5.size() << "\n";
  forall(tmp_, itoutput5) {
	  cout << tmp_ << "\n";
	}

  cout << "\n\n\n\n";
  
  list<interval> itinput3;  itinput3.append(interval(2,2,0)); itinput3.append(interval(3,1,1)); itinput3.append(interval(2,2,2));
  itoutput3.clear(); man.filtered_posibilities(itinput3, itoutput3);
  cout << "size: " << itoutput3.size() << "\n";
  forall(tmp_, itoutput3) {
	  cout << tmp_ << "\n";
	}



  list<interval> chk1;  chk1.append(interval(1,1,0)); chk1.append(interval(2,2,1)); chk1.append(interval(1,1,2));
  list<interval> chk2;  chk2.append(interval(3,1,0)); chk2.append(interval(2,2,1)); chk2.append(interval(1,1,2));
  list<interval> chk3;  chk3.append(interval(1,1,0)); chk3.append(interval(3,2,1)); chk3.append(interval(1,1,2));
  list<interval> chk4;  chk4.append(interval(1,1,0));

  result_ind.clear();
  result_type.clear();
  cout << chk1 << " -> " << chk1 << "\n";
  cout << "is outcome in: " << man.contains(chk1, chk1, 1, result_ind, result_type) << "\n";
  cout << "results: " << result_ind << ", " << result_type << "\n";
  cout << "\n";

  result_ind.clear();
  result_type.clear();
  cout << chk1 << " -> " << chk2 << "\n";
  cout << "is outcome in: " << man.contains(chk1, chk2, 1, result_ind, result_type) << "\n";
  cout << "results: " << result_ind << ", " << result_type << "\n";
  cout << "\n";

  result_ind.clear();
  result_type.clear();
  cout << chk2 << " -> " << chk2 << "\n";
  cout << "is outcome in: " << man.contains(chk2, chk2, 0, result_ind, result_type) << "\n";
  cout << "results: " << result_ind << ", " << result_type << "\n";
  cout << "\n";

  result_ind.clear();
  result_type.clear();
  cout << chk2 << " -> " << chk1 << "\n";
  cout << "is outcome in: " << man.contains(chk2, chk1, 1, result_ind, result_type) << "\n";
  cout << "results: " << result_ind << ", " << result_type << "\n";
  cout << "\n";

  result_ind.clear();
  result_type.clear();
  cout << chk1 << " -> " << chk3 << "\n";
  cout << "is outcome in: " << man.contains(chk1, chk3, 1, result_ind, result_type) << "\n";
  cout << "results: " << result_ind << ", " << result_type << "\n";
  cout << "\n";

  result_ind.clear();
  result_type.clear();
  cout << chk3 << " -> " << chk4 << "\n";
  cout << "is outcome in: " << man.contains(chk3, chk4, 1, result_ind, result_type) << "\n";
  cout << "results: " << result_ind << ", " << result_type << "\n";
  cout << "\n";
  */
  system("pause");

  return 0;
}

