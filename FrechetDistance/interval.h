#ifndef INTERVAL_H_INCLUDED
#define INTERVAL_H_INCLUDED

//=================================
// included dependencies
#include "stdafx.h"
#include <LEDA/core/tuple.h>
#include <LEDA/numbers/real.h>

using leda::list;
using leda::real;
using leda::two_tuple;

typedef two_tuple<real, real> real_point;
namespace frechet {
class interval {
public:
	int type; 
//	int behaves_as;
	real_point limits;
	list<int> ids;

	interval();
	//interval(int type, int behaves_as, real_point limits);
	//interval(int type, int behaves_as);
	//interval(int type, int behaves_as, int id);
	//interval(int type, int behaves_as, int id1, int id2);
	//interval(int type, int behaves_as, list<int> ids);
	//interval(int type, int behaves_as, list<int> ids1, list<int> ids2);
	//interval(int type, int behaves_as, list<int> ids, real_point limits);

	interval(int type, real_point limits);
	interval(int type);
	interval(int type, int id);
	interval(int type, int id1, int id2);
	interval(int type, list<int> ids);
	interval(int type, list<int> ids1, list<int> ids2);
	interval(int type, list<int> ids, real_point limits);

	friend std::ostream& operator<<(std::ostream& os, const interval& dt);
	bool operator==(const interval& f) const;
	bool operator!=(const interval& f) const;
};
}
#endif