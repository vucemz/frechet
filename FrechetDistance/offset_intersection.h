#ifndef CURVE_SEGMENT_H_INCLUDED
#define CURVE_SEGMENT_H_INCLUDED

//=================================
// included dependencies
#include <LEDA/core/tuple.h>
#include <LEDA/numbers/real.h>

#if defined(LEDA_STD_IO_HEADERS)
using std::ifstream;
using std::ofstream;
using std::cin;
using std::cout;
using std::endl;
using std::flush;
#endif

using namespace leda;

typedef two_tuple<real, real> real_point;
typedef two_tuple<real, real_point> real_num_point;

template<class curve>
class offset_intersection {
public:
	/* 
		returns list of pairs of parameters (intersection parameter on segment curve is an interval of points) 
		of both curves at which intersection occurs 

		every intersection is of type <t, <s1, s2>>, where t is the point of intersection on curve *this*, 
		and (s1, s2) is the interval of points of intersection on curve *segment*

		e.g. if segment is circular arc of radii eps and center point at this->coordinates(x), then (on of the) 
		intersection points will be <x, <segment->min_t(), segment->max_t()>>

		local caching of this function can increase performance drastically

		caller takes responsibility for allocated objects
	*/
	virtual list<two_tuple<real, real_interval*>> offset_intersection3(curve* segment, const real& eps) =0;
};

#endif