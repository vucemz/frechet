#ifndef FREE_SPACE_H_INCLUDED
#define FREE_SPACE_H_INCLUDED

//=================================
// included dependencies
#include "stdafx.h"
#include "free_space_cell.h"
#include "../ParametricSearch/decision_problem.h"
#include "../ParametricSearch/parametric_search.h"

using leda::real;
using leda::list;
using leda::string;
using parametric_search::decision_problem;
using parametric_search::quicksort;

namespace frechet {
template<class curve1, class curve2>
class free_space : public parametric_search::decision_problem<real> {
private:
	list<curve2*> curve_x; list<curve_segment<curve1>*> curve_xc;
	list<curve1*> curve_y; list<curve_segment<curve2>*> curve_yc;

	void preprocess_tangents(real& eps, list<curve2*>& curve_x, list<curve1*>& curve_y, list<curve2*>& ppx, list<curve1*>& ppy);
	free_space_cell<curve1, curve2>* preprocess(real& eps, list<curve2*>& ppx, list<curve1*>& ppy);
private:	
	free_space_cell<curve1, curve2>* add_curve_x(free_space_cell<curve1, curve2>* c, curve2* seg);
	free_space_cell<curve1, curve2>* add_curve_y(free_space_cell<curve1, curve2>* c, curve1* seg);

	void append_curve_x(free_space_cell<curve1, curve2>* c, curve2* seg);
	void append_curve_y(free_space_cell<curve1, curve2>* c, curve1* seg);

	free_space_cell<curve1, curve2>* build(list<curve2*> curve_x, list<curve1*> curve_y);

	string plot_cell(curve_segment<curve1>* cx, curve_segment<curve2>* cy, int step, real& eps);

	void delete_cell(free_space_cell<curve1, curve2>* cell);

	bool binary_search(real roots[], real_point& res, int l, int r);
	free_space_cell<curve1, curve2>* get_top_right();
public:
	free_space(list<curve2*>& curve_x, list<curve1*>& curve_y);
	free_space(real& k, list<curve2*>& curve_x, list<curve1*>& curve_y);
	~free_space();

	int plot_cells(real& eps, std::ostream& out = std::cout, int step = 100, int i = 0);
	void plot_curves(std::ostream& out = std::cout, int step = 100);

	int solve(real& eps);
	real optimization();
};

template<class curve1, class curve2>
free_space<curve1, curve2>::~free_space() {
	curve2* cx; forall (cx, curve_x) delete cx;
	curve1* cy; forall (cy, curve_y) delete cy;
}

template<class curve1, class curve2>
free_space<curve1, curve2>::free_space(list<curve2*>& curve_x, list<curve1*>& curve_y) {
	curve2* cx; forall (cx, curve_x) { curve2* copy = new curve2(*cx); this->curve_x.append(copy); this->curve_xc.append(copy); }
	curve1* cy; forall (cy, curve_y) { curve1* copy = new curve1(*cy);  this->curve_y.append(copy); this->curve_yc.append(copy); }
}

template<class curve1, class curve2>
free_space<curve1, curve2>::free_space(real& k, list<curve2*>& curve_x, list<curve1*>& curve_y) {
	list<curve2*> pp_curve_x; list<curve1*> pp_curve_y;

	this->preprocess_tangents(k, curve_x, curve_y, pp_curve_x, pp_curve_y);
	curve2* cx; forall (cx, pp_curve_x) { this->curve_x.append(cx); this->curve_xc.append(cx); }
	curve1* cy; forall (cy, pp_curve_y) { this->curve_y.append(cy); this->curve_yc.append(cy); }
}

template<class curve1, class curve2>
free_space_cell<curve1, curve2>*  free_space<curve1, curve2>::add_curve_x(free_space_cell<curve1, curve2>* c, curve2* seg) {
	curve1* seg_y = c->segment_yc;
	return new free_space_cell<curve1, curve2>(seg, seg_y);
}

template<class curve1, class curve2>
free_space_cell<curve1, curve2>*  free_space<curve1, curve2>::add_curve_y(free_space_cell<curve1, curve2>* c, curve1* seg) {
	curve2* seg_x = c->segment_xc;
	return new free_space_cell<curve1, curve2>(seg_x, seg);
}

template<class curve1, class curve2>
void free_space<curve1, curve2>::append_curve_x(free_space_cell<curve1, curve2>* cell, curve2* seg) {
	list<free_space_cell<curve1, curve2>*> cells = cell->right_hand_most();
	cells.reverse();

	free_space_cell<curve1, curve2>* c;
	free_space_cell<curve1, curve2>* last;

	last = 0;
	forall (c, cells) {
		free_space_cell<curve1, curve2>* fsc = add_curve_x(c, seg);

		c->right = fsc;
		if (last) last->up = fsc;

		last = fsc;
	}
}

template<class curve1, class curve2>
void free_space<curve1, curve2>::append_curve_y(free_space_cell<curve1, curve2>* cell, curve1* seg) {
	list<free_space_cell<curve1, curve2>*> cells = cell->top_hand_most();
	cells.reverse();

	free_space_cell<curve1, curve2>* c;
	free_space_cell<curve1, curve2>* last;

	last = 0;
	forall (c, cells) {
		free_space_cell<curve1, curve2>* fsc = add_curve_y(c, seg);

		c->up = fsc;
		if (last) last->right = fsc;

		last = fsc;
	}
}

template<class curve1, class curve2>
free_space_cell<curve1, curve2>* free_space<curve1, curve2>::build(list<curve2*> curve_x, list<curve1*> curve_y) {
	curve2* first_x = curve_x.pop_front();
	curve1* first_y = curve_y.pop_front();

	free_space_cell<curve1, curve2>* cell = new free_space_cell<curve1, curve2>(first_x, first_y);

	while (!curve_x.empty()) {
		curve2* seg = curve_x.pop_front();
		append_curve_x(cell, seg);
	}

	while (!curve_y.empty()) {
		curve1* seg = curve_y.pop_front();
		append_curve_y(cell, seg);
	}

	cell->bp1b = true; cell->bp1 = cell->segment_x->min_t();
	cell->lp1b = true; cell->lp1 = cell->segment_y->min_t();

	return cell;
}

template<class curve1, class curve2>
void free_space<curve1, curve2>::delete_cell(free_space_cell<curve1, curve2>* cell) {
	if (cell->up) delete cell->up;
	delete cell;
}

// solve row by row, bottom up
template<class curve1, class curve2>
int free_space<curve1, curve2>::solve(real& eps) {

	// construct a new cell
	list<curve2*> free_x; list<curve1*> free_y;
	free_space_cell<curve1, curve2>* cell = preprocess(eps, free_x, free_y);

	bool t1b; real t1; 
	bool t2b; real t2; 
	bool r1b; real r1;
	bool r2b; real r2;

	t1b = t2b = r1b = r2b = false;

	free_space_cell<curve1, curve2>* c = cell;

	real_point c_x = c->segment_x->coordinates(c->bp1) ;
	real_point c_y = c->segment_y->coordinates(c->lp1) ;

	bool succeeds = false;

	cout << "\nCHECKING FOR " << eps.to_double() << ":\n";

	// if starting point out of free space return unsuccessful
	if ( (c_x.first() - c_y.first()) * (c_x.first() - c_y.first()) +
		(c_x.second() - c_y.second()) * (c_x.second() - c_y.second()) > eps*eps) return -1;

	while (true) {
		cout << "--------- NEW LEVEL --------------------\n";
		free_space_cell<curve1, curve2>* c2 = c;
		while (true) {
			succeeds = succeeds | c2->solve_propagate(
				t1b, t1,
				t2b, t2,
				r1b, r1,
				r2b, r2, eps);

			cout << "results for bottom axis: ";
			if (c2->bp1b) cout << c2->bp1.to_double() << " | ";
			else cout << "no tp1 | ";
			if (c2->bp2b) cout << c2->bp2.to_double() << "\n";
			else cout << "no tp2\n";

			cout << "results for left axis: ";
			if (c2->lp1b) cout << c2->lp1.to_double() << " | ";
			else cout << "no tp1 | ";
			if (c2->lp2b) cout << c2->lp2.to_double() << "\n";
			else cout << "no tp2\n";

			if (c2->right == 0) break;
			c2 = c2->right;
		}
		if (c->up == 0) break;
		c = c->up;
	}

	cout << "--------- FINAL RESULTS --------------------\n";

	cout << "results for top axis: ";
	if (t1b) cout << t1.to_double() << " | ";
	else cout << "no tp1 | ";
	if (t2b) cout << t2.to_double() << "\n";
	else cout << "no tp2\n";

	cout << "results for right axis: ";
	if (r1b) cout << r1.to_double() << " | ";
	else cout << "no tp1 | ";
	if (r2b) cout << r2.to_double() << "\n";
	else cout << "no tp2\n";

	if (succeeds) cout << "Top right corner CAN be reached!\n";
	else cout << "Top right corner CAN'T be reached!\n";

	// remove cell and all curves connected with it
	delete_cell(cell);
	curve2* cx; forall (cx, free_x) delete cx;
	curve1* cy; forall (cy, free_y) delete cy;

	return (succeeds ? 1 : -1);
}

template<class curve1, class curve2>
int free_space<curve1, curve2>::plot_cells(real& eps, std::ostream& out, int step, int i = 0) {
	if (i == curve_y.size()) {
		out << "ha = tight_subplot(" << curve_y.size() << "," << curve_x.size() << ", [0 0], [0 0], [0 0]);\n";
		return 1;
	}

	int ni = plot_cells (eps, out, step, i+1);

	curve_segment<curve2>* cy = curve_y.contents(curve_y[i]);
	curve_segment<curve1>* cx;
	forall (cx, curve_x) {
		out << "axes(ha(" << ni << "));\n";
		out << plot_cell(cx, cy, step, eps);
		ni++;
	}

	return ni;
}


template<class curve1, class curve2>
void free_space<curve1, curve2>::plot_curves(std::ostream& out, int step) {
	int i = 1;
	
	curve_segment<curve1>* cx;
	forall (cx, this->curve_xc) {
		out << cx->matlab_plot(step, i) << "\n"; i++;
	}

	curve_segment<curve2>* cy;
	forall (cy, this->curve_yc) {
		out << cy->matlab_plot(step, i) << "\n"; i++;
	}

	out << "plot(x1,y1";
	for (int j = 1; j < i; j++) {
		out << ",x" << j << ",y" << j;
	}
	out << ")\n";
}

template<class R> list<R> remove_duplicates(list<R> lst) {
	list<R> res;

	while (!lst.empty()) {
		R h = lst.pop(); res.append(h);
		while (!lst.empty() && lst.front() == h) lst.pop();
	}

	return res;
}

template<class curve1, class curve2>
bool free_space<curve1, curve2>::binary_search(real roots[], real_point& res, int l, int r) {
	if (r - l == 1) {
		real lr = roots[l];
		real rr = roots[r];
		res = real_point(lr, rr); return true;
	}
	
	int m = l + (r-l)/2;
	real mr = roots[m];

	int ms = solve(mr);

	if (ms != -1) return binary_search(roots, res, l, m);
	else return binary_search(roots, res, m, r);
}

template<class R>
static R* to_array(list<R> lst) {
	R* arr = new R[lst.size()];

	for (int i = 0; i < lst.size(); i++) {
		arr[i] = lst.contents(lst[i]);
	}

	return arr;
}

template<class curve1, class curve2>
real free_space<curve1, curve2>::optimization() {
	real r;

	list<real> prelim_roots;
	prelim_roots.append(0);
	real_point co1; real_point co2;
	
	co1 =  curve_xc.head()->coordinates(curve_xc.head()->min_t());
	co2 =  curve_yc.head()->coordinates(curve_yc.head()->min_t());
	prelim_roots.append(sqrt((co1.first() - co2.first()) * (co1.first() - co2.first()) + (co1.second() - co2.second()) * (co1.second() - co2.second())));
	
	co1 = curve_xc.back()->coordinates(curve_xc.back()->max_t());
	co2 = curve_yc.back()->coordinates(curve_yc.back()->max_t());
	prelim_roots.append(sqrt((co1.first() - co2.first()) * (co1.first() - co2.first()) + (co1.second() - co2.second()) * (co1.second() - co2.second())));

	r;	forall(r, prelim_roots) cout << r.to_double() << "\n";
	cout << "\n";cout << "\n";

	curve_segment<curve1>* cx; curve1* ey;
	forall (cx, curve_xc) {
		forall (ey, curve_y) {
			prelim_roots.conc(cx->preliminary_roots(ey));
		}
	}

	curve_segment<curve2>* cy; curve2* ex;
	forall (cy, curve_yc) {
		forall (ex, curve_x) {
			prelim_roots.conc(cy->preliminary_roots(ex));
		}
	}

	prelim_roots.sort();
	prelim_roots = remove_duplicates(prelim_roots);
	
	prelim_roots.push_back(10000);

	r;	forall(r, prelim_roots) cout << r.to_double() << "\n";
	cout << prelim_roots.size() << "\n";

	real* arr = to_array(prelim_roots);
	real_point res;
	cout << binary_search(arr, res, 0, prelim_roots.size() - 1);
	cout << " " << res.first().to_double() << " " << res.second().to_double() << "\n";
	delete[] arr;

	list<function<real>*> polynomials;
	
	forall (cy, curve_yc) {
		forall (ex, curve_x) {
			polynomials.conc(cy->polynomials(ex, this->curve_x));
		}
	}


	cout << "collected " << polynomials.size() << " x polynomials!\n";
	parametric_search::quicksort<real> qs = parametric_search::quicksort<real>(res.first(), res.second());
	two_tuple<real, real> ps_result = qs.parametric_search(polynomials, *this);
	cout << ps_result.first().to_double() << " - " << ps_result.second().to_double() << "\n";

	// delete polynomials;
	while (!polynomials.empty()) {
		function<real>* p = polynomials.pop();
		delete p;
	}

	forall (cx, curve_xc) {
		forall (ey, curve_y) {
			polynomials.conc(cx->polynomials(ey, this->curve_y));
		}
	}

	cout << "collected " << polynomials.size() << " y polynomials!\n";
	qs = parametric_search::quicksort<real>(ps_result.first(), ps_result.second());
	ps_result = qs.parametric_search(polynomials, *this);
	cout << ps_result.first().to_double() << " - " << ps_result.second().to_double() << "\n";

	// delete polynomials;
	while (!polynomials.empty()) {
		function<real>* p = polynomials.pop();
		delete p;
	}

	return ps_result.second();
}

// when result is deleted so should be all the curves in such grid (they are copies)
template<class curve1, class curve2>
free_space_cell<curve1, curve2>* free_space<curve1, curve2>::preprocess(real& eps, list<curve2*>& ppx, list<curve1*>& ppy) {
	curve_segment<curve1>* x;
	forall (x, curve_xc) {
		list<real> points = x->points_of_curvature(1/eps);
		real b = x->min_t(); 
		real r; forall (r, points) {
			 ppx.append(static_cast<curve2*>(x->copy(b, r)));
			 b = r;
		}
		real e = x->max_t();
		ppx.append(static_cast<curve2*>(x->copy(b, e)));
	}

	curve_segment<curve2>* y;
	forall (y, curve_yc) {
		list<real> points = y->points_of_curvature(1/eps);
		real b = y->min_t(); 
		real r; forall (r, points) {
			 ppy.append(static_cast<curve1*>(y->copy(b, r)));
			 b = r;
		}
		real e = y->max_t();
		ppy.append(static_cast<curve1*>(y->copy(b, e)));
	}

	return this->build(ppx, ppy);
}

template<class curve1, class curve2>
void free_space<curve1, curve2>::preprocess_tangents(real& k,  list<curve2*>& curve_xc, list<curve1*>& curve_yc, list<curve2*>& ppx, list<curve1*>& ppy) {
	curve_segment<curve1>* x;
	forall (x, curve_xc) {
		list<real> points = x->points_of_tangents(k);
		real b = x->min_t(); 
		real r; forall (r, points) {
			 ppx.append(static_cast<curve2*>(x->copy(b, r)));
			 b = r;
		}
		real e = x->max_t();
		ppx.append(static_cast<curve2*>(x->copy(b, e)));
	}

	curve_segment<curve2>* y;
	forall (y, curve_yc) {
		list<real> points = y->points_of_tangents(k);
		real b = y->min_t(); 
		real r; forall (r, points) {
			 ppy.append(static_cast<curve1*>(y->copy(b, r)));
			 b = r;
		}
		real e = y->max_t();
		ppy.append(static_cast<curve1*>(y->copy(b, e)));
	}
}

template<class curve1, class curve2>
string free_space<curve1, curve2>::plot_cell(curve_segment<curve1>* cx, curve_segment<curve2>* cy, int step, real& eps) {
	string var_x = "(" + cx->x_from_t("x") + ")";
	string var_y = "(" + cy->x_from_t("y") + ")";

	leda::string_ostream s;
	s << "[x,y]=meshgrid(" 
		<< cx->min_t().to_double() << ":" << ( (cx->max_t().to_double()-cx->min_t().to_double())/step) << ":" << cx->max_t().to_double()
		<< ", "
		<< cy->min_t().to_double() << ":" <<  ( (cy->max_t().to_double()-cy->min_t().to_double())/step)  << ":" << cy->max_t().to_double()
		<< ");\n";

	s << "g = " << cy->matlab_function(var_y) << ";\n";
	s << "f = " << cx->matlab_function(var_x) << ";\n";
	s << "r = sqrt((g - f).^2 + (" << var_x << " - " << var_y << ").^2);\n";
	s << "contourf(x, y, r, [" << eps.to_double() << " " << eps.to_double() << "]);\n";
	s << "set(gca,'YTick',[])\n";
	s << "set(gca,'XTick',[])\n";
	return s.str();
}



}
#endif