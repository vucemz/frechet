/*
#include "stdafx.h"
#include "free_space_cell.h"


template<class curve>
bool free_space_cell<curve>::fix_interval(list<interval>& lst, list<interval>& out) {
	interval prev; bool prev_flag;
	interval next; bool next_flag;
	bool change = false;
	for (int i = 0; i < lst.size(); i++) {
		if (i > 0) {
			prev = lst.contents(lst[i-1]);
			prev_flag = true;
		}
		else prev_flag = false;

		if (i < lst.size() - 1) {
			next = lst.contents(lst[i+1]);
			next_flag = true;
		}
		else next_flag = false;

		interval current = lst.contents(lst[i]);

		if (current.behaves_as == (POINT) && current.type == IN) {
			current.behaves_as = IN; change = true;
		}
		else if (current.behaves_as == (POINT) && current.type == OUT) {
			current.behaves_as = OUT; change = true;
		} 
		else {
			if (current.behaves_as == (POINT) && prev_flag && (prev.type == IN || (prev.type == (POINT) && prev.behaves_as == IN))) {
				current.behaves_as = OUT; change = true;
			}
			if (current.behaves_as == (POINT) && next_flag && (next.type == IN || (next.type == (POINT) && next.behaves_as == IN))) {
				current.behaves_as = OUT; change = true;
			}
			if (current.behaves_as == (POINT) && prev_flag && (prev.type == OUT || (prev.type == (POINT) && prev.behaves_as == OUT))) {
				current.behaves_as = IN; change = true;
			}
			if (current.behaves_as == (POINT) && next_flag && (next.type == OUT || (next.type == (POINT) && next.behaves_as == OUT))) {
				current.behaves_as = IN; change = true;
			}
		}
		out.append(current);
	}

	return change;
}
	
template<class curve>
bool free_space_cell<curve>::transform (list<interval>& lst, int i, list<two_tuple<int, int>>& input, list<interval>& next) {
	if (lst.size() == i) {

		// join
		// test against next

		int z = 0;
		for (int j = 0; j < input.size(); j++) {
			int type = input.contents(input[j]).first();
			if (type == -1) continue;

			if (type != next.contents(next[z]).type) return false;
			z++;
		}

		return true;
	}

	interval intr = lst.contents(lst[i]);
		
	list<int> types;
	if (intr.type == IN) {
		types.append(IN);
		types.append(POINT);
	}
	else if (intr.type == OUT) {
		types.append(OUT);
		types.append(POINT);
	}
	else {
		if (intr.behaves_as == IN) {
			types.append(IN);
			types.append(-1);
		}
		else if (intr.behaves_as == OUT) {
			types.append(OUT);
			types.append(-1);
		}
		else {
			types.append(IN);
			types.append(OUT);
			types.append(-1);
		}
	}

	int t;
	forall(t, types) {
		input.push_back(two_tuple<int, int>(t, i));

		if (transform(lst, i+1, input, next)) return true;

		input.pop_back();
	}

	return false;
}


template<class curve>
free_space_cell<curve>::free_space_cell(curve_segment<curve>* x, curve_segment<curve>* y, const real& eps) {
	this->segment_x = x;
	this->segment_y = y;
	this->eps = eps;
}

template<class curve>
free_space_cell<curve>::~free_space_cell() {

}

template<class curve>
list<interval> free_space_cell<curve>::intersection_int(curve_segment<curve>* s1, real t, curve_segment<curve>* s2) {
	list<real> intersections = s2->circle_intersection(s1->coordinates(t), eps);
	intersections.append(s2->min_t());
	intersections.append(s2->max_t());
	intersections.sort();

	list<interval> lst;

	for (int i = 0; i < intersections.size() - 1; i++) {
		real i1 = intersections.contents(intersections[i]);
		real i2 = intersections.contents(intersections[i+1]);

		interval intr;
		intr.limits = real_point(i1, i2);
		intr.type = s2->inside(intr.limits, t, s1, eps);
		intr.behaves_as = POINT;
		intr.ids.append(i);


		lst.append(intr);
	}

	return lst;
}

template<class curve>
list<interval> free_space_cell<curve>::y_intersections_int() {
	return y_intersections_int(segment_x->min_t());
}

template<class curve>
list<interval> free_space_cell<curve>::x_intersections_int() {
	return x_intersections_int(segment_y->min_t());
}

template<class curve>
list<interval> free_space_cell<curve>::y_intersections_int(real tx) {
	list<interval> lst = intersection_int(segment_x, tx, segment_y);
	list<interval> lst_;
	while (fix_interval(lst, lst_)) {
		lst = lst_;
		lst_.clear();
	}

	return lst;
}

template<class curve>
list<interval> free_space_cell<curve>::x_intersections_int(real ty) {
	list<interval> lst = intersection_int(segment_y, ty, segment_x);
	list<interval> lst_;
	while (fix_interval(lst, lst_)) {
		lst = lst_;
		lst_.clear();
	}

	return lst;
}

template<class curve>
list<real> free_space_cell<curve>::y_intersections(real t) {
	return segment_y->circle_intersection(
		segment_x->coordinates(t), 
		eps);
}

template<class curve>
list<real> free_space_cell<curve>::x_intersections(real t) {
	return segment_x->circle_intersection(
		segment_y->coordinates(t), 
		eps);
}

template<class curve>
list<real> free_space_cell<curve>::y_intersections() {
	return y_intersections(segment_x->min_t());
}

template<class curve>
list<real> free_space_cell<curve>::x_intersections() {
	return x_intersections(segment_y->min_t());
}

template<class curve>
list<real> free_space_cell<curve>::test_intersections_x() {
	list<real> lst;
	real_point p;


	list<real_point> cint = segment_x->offset_intersection((curve*)segment_y, eps);
	forall(p, cint) {
		lst.append(p.first());
	}

	list<real_point> cint2 = segment_y->offset_intersection((curve*)segment_x, eps);
	forall(p, cint2) {
		lst.append(p.second());	// add double
		lst.append(p.second());
	}

	lst = lst_union(lst, segment_x->circle_intersection(segment_y->coordinates(segment_y->min_t()), eps));
	lst = lst_union(lst, segment_x->circle_intersection(segment_y->coordinates(segment_y->max_t()), eps));

	lst.append(segment_x->min_t());
	lst.append(segment_x->max_t());

	lst.sort();

	return lst;
}

template<class curve>
bool free_space_cell<curve>::get_right_point(list<interval>& lst, real& lst_point, int follow, list<real>& points, real& result, int i = 0, two_tuple<real, side>* intersection = 0) {
	if (i >= points.size()) {
		//result = points.contents(points[points.size() - 1]);
		return false;
	}

	list<interval> intvt_p = lst;
	real r = points.contents(points[i]);
	list<interval> intvt = y_intersections_int(r);

	list<int> result_ind;
	list<interval> result_type;

	cout << "checking " << intvt_p << " -> " << intvt << "\n";

	if (!transition_manager::contains(intvt_p, intvt, follow, result_ind, result_type)) return false;	//TODO throw exception!

	cout << "at r = " << r.to_double() << "\n";
	cout << intvt_p << " -> " << intvt << "\n";
	//cout << "is outcome in: " << transition_manager::contains(intvt_p, intvt, follow, result_ind, result_type) << "\n";
	cout << "results: " << result_ind << ", " << result_type << "\n";
	cout << "\n";

	if (result_ind.size() > 0) {
		list<real> candidates; int id;
		forall (id, result_ind) {
			real r_; 
			if (get_right_point(intvt, r, id, points, r_, i+1))
				candidates.append(r_);
			else {
				return false;
			}
		}
		if (candidates.size() > 0) {
			candidates.sort();
			result = candidates.contents(candidates[candidates.size()-1]);
			return true;
		}
		else return false;
	}
	else {
		if (i == 0) result = lst_point;
		else result = points.contents(points[i-1]);
		return true;
	}
}

template<class curve>
template<class T>
list<T> free_space_cell<curve>::lst_union(list<T>& lst1, list<T>& append) {
	list<T> lst = lst1;

	T i;
	forall (i, append) {
		if (lst1.search(i) == nil)
			lst.append(i);
	}

	return lst;
}
*/