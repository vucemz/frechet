#include "stdafx.h"
#include "interval.h"
namespace frechet {

interval::interval() {
}

//interval::interval(int type, int behaves_as, real_point limits) {
//	this->type = type;
//	this->behaves_as = behaves_as;
//	this->limits = limits;
//}
//
//interval::interval(int type, int behaves_as) {
//	this->type = type;
//	this->behaves_as = behaves_as;
//}
//
//interval::interval(int type, int behaves_as, int id) {
////	this->is_new = false;
//	this->type = type;
//	this->behaves_as = behaves_as;
//
//	if (id != -1) ids.append(id);
//	//ids.append(id);
//}
//
//interval::interval(int type, int behaves_as, int id1, int id2) {
////	this->is_new = false;
//	this->type = type;
//	this->behaves_as = behaves_as;
//	if (id1 != -1) ids.append(id1);
//	if (id2 != -1) ids.append(id2);
//
//	//ids.append(id1);
//	//ids.append(id2);
//
//}
//
//interval::interval(int type, int behaves_as, list<int> ids) {
////	this->is_new = false;
//	this->type = type;
//	this->behaves_as = behaves_as;
//	this->ids = ids;
//}
//
//
//interval::interval(int type, int behaves_as, list<int> ids1, list<int> ids2) {
////	this->is_new = false;
//	this->type = type;
//	this->behaves_as = behaves_as;
//	int id;
//	forall(id, ids1) this->ids.append(id);
//	forall(id, ids2) this->ids.append(id);
//}
//
//interval::interval(int type, int behaves_as, list<int> ids, real_point limits) {
////	this->is_new = false;
//	this->type = type;
//	this->behaves_as = behaves_as;
//	this->ids = ids;
//	this->limits = limits;
//}



interval::interval(int type, real_point limits) {
	this->type = type;
	this->limits = limits;
}

interval::interval(int type) {
	this->type = type;
}

interval::interval(int type, int id) {
	this->type = type;
	if (id != -1) ids.append(id);
}

interval::interval(int type, int id1, int id2) {
	this->type = type;
	if (id1 != -1) ids.append(id1);
	if (id2 != -1) ids.append(id2);

}

interval::interval(int type, list<int> ids) {
	this->type = type;
	this->ids = ids;
}


interval::interval(int type, list<int> ids1, list<int> ids2) {
	this->type = type;
	int id;
	forall(id, ids1) this->ids.append(id);
	forall(id, ids2) this->ids.append(id);
}

interval::interval(int type, list<int> ids, real_point limits) {
	this->type = type;
	this->ids = ids;
	this->limits = limits;
}


std::ostream& operator<<(std::ostream& os, const interval& dt)
{
	if (dt.ids.size() > 0)
//		os << "<" << dt.type  << "," << dt.behaves_as << " ids = [" << dt.ids << "]" << ", l: (" << dt.limits.first().to_double() << "," << dt.limits.second().to_double()  << ")>";
		os << "<" << dt.type  <<  " ids = [" << dt.ids << "]" << ", l: (" << dt.limits.first().to_double() << "," << dt.limits.second().to_double()  << ")>";
	else
		//os << "<" << dt.type  << "," << dt.behaves_as << " l: (" << dt.limits.first().to_double() << "," << dt.limits.second().to_double()  << ")>";
		os << "<" << dt.type  <<  " l: (" << dt.limits.first().to_double() << "," << dt.limits.second().to_double()  << ")>";
	return os;
}
	
bool interval::operator==(const interval& f) const {
	//return (f.type == type) && (f.behaves_as == behaves_as);
	return (f.type == type);
}

bool interval::operator!=(const interval& f) const {
	return !(f == *this);
}
}