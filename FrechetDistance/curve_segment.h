#ifndef CURVE_SEGMENT_H_INCLUDED
#define CURVE_SEGMENT_H_INCLUDED

//=================================
// included dependencies
#include <LEDA/core/tuple.h>
#include <LEDA/numbers/real.h>

#include "../ParametricSearch/function.h"

using leda::list;
using leda::real;
using leda::two_tuple;
using leda::string;

using parametric_search::function;

typedef two_tuple<real, real> real_point;
typedef two_tuple<real, real_point> real_num_point;

namespace frechet {
struct real_interval {
	virtual void Member(){}
};

struct real_tp_interval : public real_interval {
	real v1, v2;

	real_tp_interval() {
	}

	real_tp_interval(real v1, real v2) {
		this->v1 = v1; this->v2 = v2;
	}

	int type() {return 2;}
};

struct real_sp_interval : public real_interval {
	real v;

	real_sp_interval() {
	}

	real_sp_interval(real v) {
		this->v = v;
	}

	int type() {return 1;}
};

template<class curve>
class curve_segment {
public:
	/* 
		returns list of pairs of parameters (intersection parameter on segment curve is an interval of points) 
		of both curves at which intersection occurs 

		every intersection is of type <t, <s1, s2>>, where t is the point of intersection on curve *this*, 
		and (s1, s2) is the interval of points of intersection on curve *segment*

		e.g. if segment is circular arc of radii eps and center point at this->coordinates(x), then (on of the) 
		intersection points will be <x, <segment->min_t(), segment->max_t()>>

		local caching of this function can increase performance drastically

		caller takes responsibility for allocated objects
	*/
	virtual list<two_tuple<real, real_interval*>> offset_intersection3(curve* segment, const real& eps) =0;

	/* 
		returns SORTED list of points at which intersection with given circle occurs 

		local caching of this function can increase performance drastically
	*/
	virtual list<real> circle_intersection(const real_point& base, const real& radius) =0;

	/* 
		returns a list of intervals on the curve intersecting with given circle 

		caller takes responsibility for allocated objects
	*/
	virtual list<real_interval*> circle_intersection_int(const real_point& base, const real& radius);

	/*
		returns (x,y) point of curve at parameter t
	*/
	virtual real_point coordinates(real t) =0;

	/* returns minimal parameter t of the curve	*/
	virtual real min_t() = 0;

	/* returns maximal parameter t of the curve	*/
	virtual real max_t() = 0;



	virtual curve_segment<curve>* copy(real min, real max) {
		return this;
	};


	virtual list<real> preliminary_roots(curve* segment) { 
		list<real> l; return l;
	}

	/*
		return polynomials that represent t on the segment of critical points and local extremes
	*/
	virtual list<function<real>*> polynomials(curve* segment, list<curve*> segments) {
		list<function<real>*> l; return l;
	}

	/*
		points on the curve where curvature matches the parameter. If there is an interval starting and ending points should be added
	*/
	virtual list<real> points_of_curvature(real& curvature) {
		list<real> lst; return lst;
	}

	/*
		points where D[this, t] == k
	*/
	virtual list<real> points_of_tangents(real& k) {
		list<real> lst; return lst;
	}


	virtual string x_from_t(string var) { return var; }
	virtual string matlab_function(string var) { return "x"; }

	virtual bool in_range(real t);
	virtual bool in_range(real t, int bits);

	virtual real x_to_t(real x) { return x; }
	virtual real t_to_x(real t) { return t; }

	template<class c1> int inside(real p1, real p2, curve_segment<c1>* curve, real eps);
	template<class c1> int inside(real p1, real_point t2, curve_segment<c1>* curve, real eps);
	template<class c1> int inside(real_point t1, real p2, curve_segment<c1>* curve, real eps);

	string matlab_plot(int step, int i);
};


template<class curve>
bool curve_segment<curve>::in_range(real t) {
	return (t >= min_t() && t <= max_t());
}

template<class curve>
bool curve_segment<curve>::in_range(real t, int bits) {
	return ((t - min_t()).sign(bits) >= 0 && (t - max_t()).sign(bits) <= 0);
}

template<class curve>
template<class c1>
int curve_segment<curve>::inside(real t1, real t2, curve_segment<c1>* curve, real eps) {
	real_point p1 = coordinates(t1);
	real_point p2 = curve->coordinates(t2);

	real xdiff = p1.first() - p2.first();
	real ydiff = p1.second() - p2.second();

	real test = xdiff*xdiff + ydiff*ydiff;
	real eps2 = eps*eps;

	if (test < eps2) return 1;
	else if (test > eps2) return 2;
	else return 3;
}

template<class curve>
template<class c1>
int curve_segment<curve>::inside(real p1, real_point t2, curve_segment<c1>* curve, real eps) {
	return inside(p1, (t2.first() + t2.second()) / real(2), curve, eps);
}

template<class curve>
template<class c1>
int curve_segment<curve>::inside(real_point t1, real p2, curve_segment<c1>* curve, real eps) {
	return inside((t1.first() + t1.second()) / real(2), p2, curve, eps);
}

template<class curve>
list<real_interval*> curve_segment<curve>::circle_intersection_int(const real_point& base, const real& radius) {
	list<real_interval*> res;

	list<real> intr = circle_intersection(base, radius);
	intr.push_front(this->min_t());
	intr.push_back(this->max_t());

	for (int i = 0; i < intr.size() - 1; i++) {
		real r1 = intr.contents(intr[i]);
		real r2 = intr.contents(intr[i+1]);

		if (r1 == r2) 
			res.append(new real_sp_interval(r1));
		else 
			res.append(new real_tp_interval(r1, r2));
	}

	return res;
}


template<class curve>
string curve_segment<curve>::matlab_plot(int step, int i) {
	leda::string_ostream s_;
	s_ << "x" << i;
	string var_x = "(" + s_.str() + ")";

	real x1 = this->t_to_x(min_t());
	real x2 = this->t_to_x(max_t());

	double x1d = leda::min(x1.to_double(), x2.to_double());
	double x2d = leda::max(x1.to_double(), x2.to_double());

	leda::string_ostream s;
	s << "x" << i << "=" << x1d << ":" << ((x2d-x1d)/(double)step) << ":" << x2d << ";\n";
	s << "y" << i << "=" << matlab_function(var_x) << ";\n";
	return s.str();
}

}
#endif