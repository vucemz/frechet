#ifndef CIRCULAR_LINE_H_INCLUDED
#define CIRCULAR_LINE_H_INCLUDED

class straight_line;

//=================================
// included dependencies
#include "stdafx.h"
#include <LEDA/core/tuple.h>
#include <LEDA/numbers/real.h>
#include <LEDA/system/stream.h >
#include <LEDA/core/string.h>
#include <LEDA/core/dictionary.h>
#include "../FrechetDistance/curve_segment.h"

using namespace frechet;

using leda::real;
using leda::list;
using leda::two_tuple;
using leda::dictionary;

typedef two_tuple<real, real> real_point;
typedef two_tuple<real, real_point> real_num_point;
typedef two_tuple<real_point, real_point> real_point_point;


// y = -oy +- sqrt(r^2-x^2-ox^2-2*ox*x)
// x = sgn*t + ox
class circular_line :  public frechet::curve_segment<straight_line>, public frechet::curve_segment<circular_line> {
public:
using curve_segment<circular_line>::in_range;
using curve_segment<circular_line>::circle_intersection_int;

private:
	//dictionary<two_tuple<real_point, real>, list<real>> cache;
	//dictionary<circular_line, list<real_point>> cache_cl;
	
public:
	real radius;
	real ox, oy;
	real_point limits;
	int sgn; // +1 if going forward, -1 if backward
	int orientation;	// +1 if up, -1 if down
public:
	// limits should be in interval [-r, r] or [r, -r]
	circular_line();
	circular_line(real r, real_point center, real_point limits, int orientation);
	circular_line(circular_line* cl, const real& eps, bool positive);

	circular_line* copy(real min, real max);

	real min_t();
	real max_t();
	real_point coordinates(real t);

	list<real_point> intersections(circular_line& cl);
	list<real_point> intersections(straight_line& sl);

	//list<real_num_point> offset_intersection2(circular_line* segment, const real& eps);
	//list<real_num_point> offset_intersection2(straight_line* segment, const real& eps);
	list<two_tuple<real, real_interval*>> offset_intersection3(circular_line* segment, const real& eps);
	list<two_tuple<real, real_interval*>> offset_intersection3(straight_line* segment, const real& eps);

	list<real> circle_intersection(const real_point& base, const real& clradius);
	//list<real_interval*> circle_intersection_int(const real_point& base, const real& r);

	string matlab_function(string var);
	string x_from_t(string var);


	real x_to_t(real x);
	real x_to_t(real x, real ox);
	real t_to_x(real t);
};

#endif