#ifndef CIRCULAR_LINE2_H_INCLUDED
#define CIRCULAR_LINE2_H_INCLUDED


//=================================
// forward declared dependencies

//=================================
// included dependencies
#include "stdafx.h"
#include <LEDA/core/tuple.h>
#include <LEDA/numbers/real.h>
#include <LEDA/system/stream.h >
#include <LEDA/core/string.h>
#include "../FrechetDistance/curve_segment.h"
#include "../FrechetDistance/interval.h"

#include "straight_line2.h"

#if defined(LEDA_STD_IO_HEADERS)
using std::ifstream;
using std::ofstream;
using std::cin;
using std::cout;
using std::endl;
using std::flush;
#endif

using namespace leda;
using namespace frechet;


class circular_line2 : public my_curve {
public:
	real radius;
	real ox, oy;
	real_point limits;
	int sgn; // +1 if going forward, -1 if backward
	int orientation;	// +1 if up, -1 if down

public:

	circular_line2();
	circular_line2(real r, real_point center, real_point limits, int orientation);
	circular_line2(circular_line2* cl, const real& eps, bool positive);

	list<two_tuple<real, real_interval*>> offset_intersection3(my_curve* segment, const real& eps);
	list<real> circle_intersection(const real_point& base, const real& r);
	
	real_point coordinates(real t);
	real min_t();
	real max_t();

	list<two_tuple<real, real_interval*>> offset_intersection3(straight_line2* segment, const real& eps);
	list<two_tuple<real, real_interval*>> offset_intersection3_(straight_line2* segment, const real& eps);
	list<two_tuple<real, real_interval*>> offset_intersection3(circular_line2* segment, const real& eps);
	list<two_tuple<real, real_interval*>> offset_intersection3_(circular_line2* segment, const real& eps);


	list<real_point> intersections(circular_line2& cl);
	list<real_point> intersections(straight_line2& sl);

	string matlab_function(string var);
	string x_from_t(string var);


	real x_to_t(real x);
	real x_to_t(real x, real ox);
	real t_to_x(real t);

	list<real> points_of_tangents(real& k) {
		list<real> r;
		real tmp = 1+k*k;
		if (tmp > 0) {
			real t = orientation*k*radius/sqrt(tmp);
			if (in_range(t)) r.append(t);
		}

		return r;

	}

	circular_line2* copy(real min, real max) {
		circular_line2* cl2 = new circular_line2(*this);
		cl2->limits=real_point(min, max);
		return cl2;
	}
};


#endif