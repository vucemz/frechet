#include "stdafx.h"
#include "circular_line2.h"
#include "straight_line2.h"

namespace leda {
	int compare(const straight_line2& a, const straight_line2& b) {
		if (a.coef.first() > b.coef.first()) return 1;
		if (a.coef.first() < b.coef.first()) return -1;

		if (a.coef.second() > b.coef.second()) return 1;
		if (a.coef.second() < b.coef.second()) return -1;

		if (a.limits.first() > b.limits.first()) return 1;
		if (a.limits.first() < b.limits.first()) return -1;

		if (a.limits.second() > b.limits.second()) return 1;
		if (a.limits.second() < b.limits.second()) return -1;

		return 0;
	}
}


list<two_tuple<real, real_interval*>> straight_line2::offset_intersection3(my_curve* segment, const real& eps) {
	return segment->offset_intersection3_(this, eps);
}

list<two_tuple<real, real_interval*>> straight_line2::offset_intersection3_(straight_line2* segment, const real& eps) {
	return segment->offset_intersection3(this, eps);
}

list<two_tuple<real, real_interval*>> straight_line2::offset_intersection3_(circular_line2* segment, const real& eps) {
	return segment->offset_intersection3(this, eps);
}





straight_line2::straight_line2(const real_point& p1, const real_point& p2) {
	coef = real_point(
		p1.second() + p1.first() * (p2.second() - p1.second()) / (p1.first() - p2.first()),
		(p2.second() - p1.second()) / (p2.first() - p1.first()));

	limits = real_point(p1.first(), p2.first());

	if (limits.first() > limits.second()) {
		this->sgn = -1;
		this->limits = real_point(sgn*limits.first(), sgn*limits.second());
	}
	else {
		this->sgn = 1;
		this->limits = limits; 
	}
}

straight_line2::straight_line2(straight_line2* segment, const real& eps, bool positive) {

	real x_offset;
	real y_offset;

	if (segment->coef.second() != 0) {
		real tmp = 1/segment->coef.second();
		x_offset = eps/sqrt(1+tmp*tmp);
		real y_offset1 = x_offset * segment->coef.second();

		//real y_offset2 = x_offset / segment->coef.second();
		real y_offset2 = x_offset * tmp;

		y_offset = y_offset2 + y_offset1;
	}
	else {
		x_offset = 0;
		y_offset = eps;
	}


	if (positive) {
		coef = real_point(segment->coef.first() + y_offset, segment->coef.second());

		real sx1 = segment->t_to_x(segment->limits.first()) - x_offset;
		real sx2 = segment->t_to_x(segment->limits.second()) - x_offset;

		limits = real_point(sx1, sx2);

		if (limits.first() > limits.second()) {
			this->sgn = -1;
			this->limits = real_point(sgn*limits.first(), sgn*limits.second());
		}
		else {
			this->sgn = 1;
			this->limits = limits; 
		}

		//limits = real_point(segment->limits.first() - x_offset, segment->limits.second() - x_offset);
	}
	else {
		coef = real_point(segment->coef.first() - y_offset, segment->coef.second());

		real sx1 = segment->t_to_x(segment->limits.first()) + x_offset;
		real sx2 = segment->t_to_x(segment->limits.second()) + x_offset;

		limits = real_point(sx1, sx2);

		if (limits.first() > limits.second()) {
			this->sgn = -1;
			this->limits = real_point(sgn*limits.first(), sgn*limits.second());
		}
		else {
			this->sgn = 1;
			this->limits = limits; 
		}

		//limits = real_point(segment->limits.first() + x_offset, segment->limits.second() + x_offset);
	}
}


bool straight_line2::intersection(straight_line2& segment, real& ins) {

	real a = coef.first();
	real b = coef.second();
	real c = segment.coef.first();
	real d = segment.coef.second();

	if (b-d == 0) return false;

	real x = (-a+c)/(b-d);
	real t = x_to_t(x);

	//if (in_range(x) && segment.in_range(x)) {
	if (in_range(t) && segment.in_range( segment.x_to_t(x))) {
		ins = t;
		return true;
	}

	return false;
}

list<real_point> straight_line2::intersections(circular_line2& sl) {
	list<real_point> res;

	real la = coef.first();
	real lb = coef.second();
	real r = sl.radius;
	real ox = sl.ox;
	real oy = sl.oy;
	real sgn = sl.sgn;

	real b = -la*la + 2* la *(lb* ox - oy) + 2 *lb *ox *oy - oy*oy + r*r + lb*lb * (-ox*ox + r*r);
	if (b >= 0) {
		real a = la * lb + ox + lb * oy;
		real c = 1 + lb*lb;

		real rt = sqrt(b);

		real x1 = -(a - rt)/c; real t1 = sgn*(x1 + ox);
		real x2 = -(a + rt)/c; real t2 = sgn*(x2 + ox);

		//if ( t1 >= sl.min_t() && t1 <= sl.max_t() && this->in_range(x_to_t(x1))) {
		if ( sl.in_range(t1) && this->in_range(x_to_t(x1))) {
			//if (this->coordinates(t1) == sl.coordinates(x1))
			if (this->coordinates(x_to_t(x1)) == sl.coordinates(t1))
				res.append(real_point(x_to_t(x1), t1));
		}
		//if (t2 >= sl.min_t() && t2 <= sl.max_t() && this->in_range(x_to_t(x2))) {
		if (sl.in_range(t2) && this->in_range(x_to_t(x2))) {
			//if (this->coordinates(t2) == sl.coordinates(x2))
			if (this->coordinates(x_to_t(x2)) == sl.coordinates(t2))
				res.append(real_point(x_to_t(x2), t2));
		}
	}

	return res;
}


list<two_tuple<real, real_interval*>> straight_line2::offset_intersection3(straight_line2* segment, const real& eps) {
	list<two_tuple<real, real_interval*>> lst;

	straight_line2 s1 = straight_line2(segment, eps, true);
	straight_line2 s2 = straight_line2(segment, eps, false);

	real ins1;
	if (intersection(s1, ins1)) {
		real x_offset = segment->t_to_x(segment->limits.first()) - s1.t_to_x(s1.limits.first());
		lst.append(two_tuple<real, real_interval*>(ins1,  new real_sp_interval(segment->x_to_t(t_to_x(ins1) + x_offset))));
		//lst.append(
		//	two_tuple<real, real_interval*>(ins1, new real_sp_interval(ins1 + x_offset)));
	}

	real ins2;
	if (intersection(s2, ins2)) {
		real x_offset = segment->t_to_x(segment->limits.first()) - s2.t_to_x(s2.limits.first());
		lst.append(two_tuple<real, real_interval*>(ins2,  new real_sp_interval(segment->x_to_t(t_to_x(ins2) + x_offset))));
		//lst.append(
		//	two_tuple<real, real_interval*>(ins2, new real_sp_interval(ins2 + x_offset)));	
	}

	return lst;
}

list<two_tuple<real, real_interval*>> straight_line2::offset_intersection3(circular_line2* segment, const real& eps) {
	list<two_tuple<real, real_interval*>> res;

	circular_line2 s1 = circular_line2(segment, eps, true);
	circular_line2 s2 = circular_line2(segment, eps, false);

	list<real_point> l1 = intersections(s1);
	
	real_point p;
	forall (p, l1) {
		real t = p.second();
		real ts = p.first();
		if (s1.radius != 0) 
			res.append(two_tuple<real, real_interval*>(ts, new real_sp_interval(t  * segment->radius/s1.radius )));	//TODO check
		else
			res.append(two_tuple<real, real_interval*>(ts, new real_tp_interval(segment->min_t(), segment->max_t())));
			//res.append(two_tuple<real, real_interval*>(x, new real_tp_interval(s1.min_t(), s1.max_t())));
	}

	list<real_point> l2 = intersections(s2);

	forall (p, l2) {
		real t = p.second();
		real ts = p.first();
		if (s2.radius != 0) 
			res.append(two_tuple<real, real_interval*>(ts, new real_sp_interval(t   * segment->radius/s2.radius )));	//TODO check
		else
			res.append(two_tuple<real, real_interval*>(ts, new real_tp_interval(segment->min_t(), segment->max_t())));
			//res.append(two_tuple<real, real_interval*>(x, new real_tp_interval(s2.min_t(), s2.max_t())));
	}

	return res;
}


list<real> straight_line2::circle_intersection(const real_point& base, const real& r) {
	list<real> lst;

	real a = coef.first();
	real b = coef.second();
	real c = base.second();
	real d = base.first();

	real sqr = -a*a-c*c+2*b*c*d-b*b*d*d+2*a*(c-b*d)+r*r+b*b*r*r;
		
	if (sqr.compare(0) < 0) return lst;

	real tmp1 = -a*b+b*c+d;
	real tmp2 = 1+b*b;

	real x1 = (tmp1 - sqrt(sqr))/tmp2;
	real x2 = (tmp1 + sqrt(sqr))/tmp2;

	real t1 = x_to_t(x1);
	real t2 = x_to_t(x2);

	//if (in_range(x1)) lst.append(x1);
	//if (in_range(x2)) lst.append(x2);
	if (in_range(t1)) lst.append(t1);
	if (in_range(t2)) lst.append(t2);
		
	lst.sort();

	return lst;
}

real_point straight_line2::coordinates(real t) {
	real x = t_to_x(t);

	//return real_point(t, coef.first() + coef.second() * t);
	return real_point(x, coef.first() + coef.second() * x);
}


real straight_line2::min_t() {
	return limits.first();
}

real straight_line2::max_t() {
	return limits.second();
}

string straight_line2::x_from_t(string var) {
	leda::string_ostream s_;
	s_ << "(" << sgn << "*" << var << ")";
	return s_.str();
}

string straight_line2::matlab_function(string var) {
	leda::string_ostream s;
	s << coef.first().to_double() << "+" << var << "*" << coef.second().to_double();
	return s.str();
}

real straight_line2::t_to_x(real t) {
	return sgn*t;
}

real straight_line2::x_to_t(real x) {
	return sgn*x;
}