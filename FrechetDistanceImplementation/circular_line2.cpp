#include "stdafx.h"
#include "circular_line2.h"

namespace leda {
	int compare(const circular_line2& a, const circular_line2& b) {
		if (a.radius > b.radius) return 1;
		if (a.radius < b.radius) return -1;

		if (a.ox > b.ox) return 1;
		if (a.ox < b.ox) return -1;

		if (a.oy > b.oy) return 1;
		if (a.oy < b.oy) return -1;

		if (a.orientation > b.orientation) return 1;
		if (a.orientation < b.orientation) return -1;

		if (a.sgn > b.sgn) return 1;
		if (a.sgn < b.sgn) return -1;

		if (a.limits.first() > b.limits.first()) return 1;
		if (a.limits.first() < b.limits.first()) return -1;

		if (a.limits.second() > b.limits.second()) return 1;
		if (a.limits.second() < b.limits.second()) return -1;

		return 0;
	}
}

list<two_tuple<real, real_interval*>> circular_line2::offset_intersection3(my_curve* segment, const real& eps) {
	return segment->offset_intersection3_(this, eps);
}


circular_line2::circular_line2() {
}

circular_line2::circular_line2(real r, real_point center, real_point limits, int orientation) {
	this->radius = r;
	this->ox = -center.first(); this->oy = -center.second();
	this->orientation = orientation;
	if (limits.first() > limits.second()) {
		this->sgn = -1;
		this->limits = real_point(sgn*limits.first(), sgn*limits.second());
	}
	else {
		this->sgn = 1;
		this->limits = limits; 
	}
}

circular_line2::circular_line2(circular_line2* cl, const real& eps, bool positive) {
	real new_r;	int sgn, orient;
	sgn = cl->sgn;
	orient = cl->orientation;
	if (positive) 
		new_r = cl->radius + eps;
	else 
		new_r = cl->radius - eps;

	real_point center = real_point(cl->ox, cl->oy);
	real_point limits = real_point(cl->limits.first() * (new_r / cl->radius), cl->limits.second() * (new_r / cl->radius));

	this->radius = abs(new_r);
	this->ox = center.first(); this->oy = center.second();
	this->orientation = orient * sign(new_r);
	if (limits.first() > limits.second()) {
		this->sgn = -1*cl->sgn;
		//this->limits = real_point(limits.second(), limits.first());
		this->limits = real_point(this->sgn*limits.first(), this->sgn*limits.second());
	}
	else {
		this->sgn = 1*cl->sgn;
		this->limits = limits; 
	}
}


real circular_line2::min_t() {
	return limits.first();
}

real circular_line2::max_t() {
	return limits.second();
}


real_point circular_line2::coordinates(real t) {
	//real x = sgn*t-ox;
	real x = t_to_x(t);
	real b = radius*radius-x*x-ox*ox-2*ox*x;
	real y = -oy + orientation*sqrt(b);
	return real_point(x,y);
}


list<real> circular_line2::circle_intersection(const real_point& base, const real& clradius) {

	real clox = -base.first();
	real cloy = -base.second();

	list<real> res;

	if ((ox - clox)*(ox - clox) + (oy - cloy)*(oy - cloy) > (radius + clradius)*(radius + clradius)) return res;

	// check if they have the same center
	if (this->ox == clox && this->oy == cloy) {
		return res;
	}

	// move base to (0,0)
	real nox = ox - clox;
	real noy = oy - cloy;

	real r1 = radius;
	real r2 = clradius;
	
	real b = -noy*noy * (nox*nox*nox*nox + noy*noy*noy*noy + 2* nox*nox *(noy*noy - r1*r1 - r2*r2) + (r1*r1 - r2*r2)*(r1*r1 - r2*r2) - 2 *noy*noy * (r1*r1 + r2*r2));
	if (b >= 0) {
		double bd = b.to_double();
		real a = nox*nox*nox + nox* noy*noy - nox* r1*r1 + nox*r2*r2;
		real c = 2 *(nox*nox + noy*noy);

		real x1 = -(a + sqrt(b))/c;
		real x2 = -(a - sqrt(b))/c;

		//real t1 =  x_to_t(x1, nox); //real t1 = sgn*(x1+nox);
		//real t2 =  x_to_t(x2, nox); //real t2 = sgn*(x2+nox);

		real t1 = sgn*(x1+nox);
		real t2 = sgn*(x2+nox);

		int param = 100;
		
		//cout << "b == 0: " << (b == 0) << "\n";
		//cout << "on the same height: " << ((oy - cloy) == 0) << "\n";
		//cout << t1 << "," << t2 << "\n";
		//cout << this->max_t() << "\n";
		//cout << x1 << "," << x2 << "\n";
		//cout << "compare to max " << (t1 == this->max_t()) << " " << (t2 == this->max_t()) << " " << (t1 == t2) << "\n";

		// TODO check if sgn = -1
		//if (this->in_range(t1)) {
		//	real_point t1_coord = this->coordinates(t1);
		//	real tx1 = t1_coord.first(); real ty1 = t1_coord.second();
		//	if (((tx1+clox)*(tx1+clox) + (ty1+cloy)*(ty1+cloy) - clradius*clradius).sign() == 0) res.append(t1);
		//}
		if (this->in_range(t1)) {
			real_point t1_coord = this->coordinates(t1);
			real tx1 = t1_coord.first(); real ty1 = t1_coord.second();
			if (((tx1+clox)*(tx1+clox) + (ty1+cloy)*(ty1+cloy) - clradius*clradius).sign(param) == 0) res.append(t1);
		}
		
		//if (this->in_range(t1)) {
		//	real_point t2_coord = this->coordinates(t2);
		//	real tx2 = t2_coord.first(); real ty2 = t2_coord.second();
		//	// if they have centers on the same height then there will be only one real intersection unless t == this->max() or t == this->min()

		//	if ((oy - cloy).sign() != 0 && ((tx2+clox)*(tx2+clox) + (ty2+cloy)*(ty2+cloy) - clradius*clradius).sign() == 0) res.append(t2);
		//	else if ((oy - cloy).sign() == 0 && t2 == this->max_t() && b == 0) res.append(t2);
		//	else if ((oy - cloy).sign() == 0 && t2 == this->min_t() && b == 0) res.append(t2);
		//	//else if ((oy - cloy).sign(param) == 0 && t2 == this->max_t() && ((tx2+clox)*(tx2+clox) + (ty2+cloy)*(ty2+cloy) - clradius*clradius).sign(param) == 0) res.append(t2);
		//	//else if ((oy - cloy).sign(param) == 0 && t2 == this->min_t() && ((tx2+clox)*(tx2+clox) + (ty2+cloy)*(ty2+cloy) - clradius*clradius).sign(param) == 0) res.append(t2);
		//}

		if (this->in_range(t2)) {
			real_point t2_coord = this->coordinates(t2);
			real tx2 = t2_coord.first(); real ty2 = t2_coord.second();
			// if they have centers on the same height then there will be only one real intersection unless t == this->max() or t == this->min()

			if ((oy - cloy).sign(param) != 0 && ((tx2+clox)*(tx2+clox) + (ty2+cloy)*(ty2+cloy) - clradius*clradius).sign(param) == 0) res.append(t2);
			else if ((oy - cloy).sign(param) == 0 && t2 == this->max_t() && b == 0) res.append(t2);
			else if ((oy - cloy).sign(param) == 0 && t2 == this->min_t() && b == 0) res.append(t2);
		}
	}
	res.sort();
	return res;
}



list<real_point> circular_line2::intersections(circular_line2& cl) {

	//dic_item it = cache_cl.lookup(cl);
	//if (it) {
	//	return cache_cl.access(cl);
	//}

	list<real_point> res;

	double rad = cl.radius.to_double();

	if ((ox - cl.ox)*(ox - cl.ox) + (oy - cl.oy)*(oy - cl.oy) > (radius + cl.radius)*(radius + cl.radius)) return res;

	//TODO: obravnavaj se sign1 = -sign2, ce sta centra in r ista
	// check if they have the same center
	if (this->ox == cl.ox && this->oy == cl.oy) {
		//cache_cl.insert(cl, res);
		return res;
	}

	// move base to (0,0)
	real nox = ox - cl.ox;
	real noy = oy - cl.oy;

	real r1 = radius;
	real r2 = cl.radius;
	
	real b = -noy*noy * (nox*nox*nox*nox + noy*noy*noy*noy + 2* nox*nox *(noy*noy - r1*r1 - r2*r2) + (r1*r1 - r2*r2)*(r1*r1 - r2*r2) - 2 *noy*noy * (r1*r1 + r2*r2));
	if (b >= 0) {
		real a = nox*nox*nox + nox* noy*noy - nox* r1*r1 + nox*r2*r2;
		real c = 2 *(nox*nox + noy*noy);

		real x1 = -(a + sqrt(b))/c;
		real x2 = -(a - sqrt(b))/c;

		real t1 =  x_to_t(x1, nox); //real t1 = sgn*(x1+nox);
		real t2 =  x_to_t(x2, nox); //real t2 = sgn*(x2+nox);

		real t1c = cl.x_to_t(x1, 0); //real t1c = cl.sgn*(x1);
		real t2c = cl.x_to_t(x2, 0); //real t2c = cl.sgn*(x2);

		if (this->in_range(t1) && cl.in_range(t1c))
			if (cl.coordinates(t1c).second() == this->coordinates(t1).second()) res.append(real_point(t1, t1c));

		if (this->in_range(t2) && cl.in_range(t2c))
			if (cl.coordinates(t2c).second() == this->coordinates(t2).second()) res.append(real_point(t2, t2c));

	}

	//cache_cl.insert(cl, res);

	return res;
}


// return list of <t, x>, where t is parameter of "this" and x is parameter of "sl"
list<real_point> circular_line2::intersections(straight_line2& sl) {
	list<real_point> res;

	real la = sl.coef.first();
	real lb = sl.coef.second();
	real r = radius;

	//if (orientation > 0) {
	real b = -la*la + 2* la *(lb* ox - oy) + 2 *lb *ox *oy - oy*oy + r*r + lb*lb * (-ox*ox + r*r);
	if (b >= 0) {
		real a = la * lb + ox + lb * oy;
		real c = 1 + lb*lb;

		real x1 = -(a - sqrt(b))/c; real t1 = sgn*(x1 + ox);
		real x2 = -(a + sqrt(b))/c; real t2 = sgn*(x2 + ox);

		if ( x1 >= sl.min_t() && x1 <= sl.max_t() && this->in_range(t1)) {
			if (this->coordinates(t1) == sl.coordinates(sl.x_to_t(x1)))
				res.append(real_point(t1, sl.x_to_t(x1)));
				//res.append(real_point(t1, x1));
		}
		if (x2 >= sl.min_t() && x2 <= sl.max_t() && this->in_range(t2)) {
			if (this->coordinates(t2) == sl.coordinates(sl.x_to_t(x2)))
				res.append(real_point(t2, sl.x_to_t(x2)));
		}
	}
	return res;
}




list<two_tuple<real, real_interval*>> circular_line2::offset_intersection3(straight_line2* segment, const real& eps) {
	list<two_tuple<real, real_interval*>> res;

	straight_line2 s1 = straight_line2(segment, eps, true);
	straight_line2 s2 = straight_line2(segment, eps, false);

	list<real_point> l1 = intersections(s1);
	list<real_point> l2 = intersections(s2);

	real_point p;
	forall (p, l1) {
		//real x = p.second(); double xd = x.to_double();
		real ts = p.second(); double tsd = ts.to_double();
		real xs = s1.t_to_x(ts);
		real t = p.first(); double td = t.to_double();
		//real x_offset = segment->limits.first() - s1.limits.first();
		real x_offset = segment->t_to_x(segment->limits.first()) - s1.t_to_x(s1.limits.first());
		double x_o = x_offset.to_double();
		//res.append(real_num_point(t, real_point(x+x_offset, x+x_offset)));
		//res.append(two_tuple<real, real_interval*>(t, new real_sp_interval(x+x_offset)));
		res.append(two_tuple<real, real_interval*>(t, new real_sp_interval( segment->x_to_t(  xs +  x_offset))));
	}

	forall (p, l2) {
		//real x = p.second(); double xd = x.to_double();
		real ts = p.second(); double tsd = ts.to_double();
		real xs = s2.t_to_x(ts);
		real t = p.first(); double td = t.to_double();
		//real x_offset = segment->limits.first() - s2.limits.first();
		real x_offset = segment->t_to_x(segment->limits.first()) - s2.t_to_x(s2.limits.first());
		double x_o = x_offset.to_double();
		//res.append(real_num_point(t, real_point(x+x_offset, x+x_offset)));
		res.append(two_tuple<real, real_interval*>(t, new real_sp_interval(segment->x_to_t(   xs+x_offset ))));
	}

	return res;
}

list<two_tuple<real, real_interval*>> circular_line2::offset_intersection3_(straight_line2* segment, const real& eps) {
	return segment->offset_intersection3(this, eps);
}

list<two_tuple<real, real_interval*>> circular_line2::offset_intersection3(circular_line2* segment, const real& eps) {
	list<two_tuple<real, real_interval*>> res;
	
	circular_line2 offset1 = circular_line2(segment, eps, true); double ofs1r = offset1.radius.to_double();
	circular_line2 offset2 = circular_line2(segment, eps, false); double ofs2r = offset2.radius.to_double();

	if (offset1.radius == 0 && this->coordinates(-offset1.ox + this->ox) == real_point(-offset1.ox, -offset1.oy)) {
		//res.append(real_num_point(offset1.ox - this->ox, segment->limits));
		res.append(two_tuple<real, real_interval*>(-offset1.ox + this->ox, new real_tp_interval(segment->limits.first(), segment->limits.second())));
	}
	else if (offset1.radius != 0) {
		list<real_point> pts1 = intersections(offset1);
		real_point pt;
		forall(pt, pts1)
			//res.append(real_num_point(pt.first(), real_point(pt.second() * segment->radius / offset1.radius, pt.second() * segment->radius / offset1.radius   ))   );
			res.append(two_tuple<real, real_interval*>(pt.first(), new real_sp_interval(pt.second() * segment->radius / offset1.radius ))   );
	}

	if (offset2.radius == 0 && this->coordinates(-offset2.ox + this->ox) == real_point(-offset2.ox, -offset2.oy)) {
		//res.append(real_num_point(offset2.ox - this->ox, segment->limits));
		res.append(two_tuple<real, real_interval*>(-offset2.ox + this->ox, new real_tp_interval(segment->limits.first(), segment->limits.second())));
	}
	else if (offset2.radius != 0) {
		list<real_point> pts2 = intersections(offset2);
		real_point pt;
		forall(pt, pts2)
			//res.append(real_num_point(pt.first(), real_point(pt.second() * segment->radius / offset2.radius, pt.second() * segment->radius / offset2.radius   ))   );
			res.append(two_tuple<real, real_interval*>(pt.first(), new real_sp_interval(pt.second() * segment->radius / offset2.radius ))   );
	}
	
	return res;
}

list<two_tuple<real, real_interval*>> circular_line2::offset_intersection3_(circular_line2* segment, const real& eps) {
	return segment->offset_intersection3(this, eps);
}

string circular_line2::x_from_t(string var) {
	leda::string_ostream s_;
	s_ << "(" << sgn << "*" << var << "-(" << ox.to_double() << "))";
	return s_.str();
}

string circular_line2::matlab_function(string var) {
	leda::string_ostream s;
	s << 
		"-(" << oy.to_double() 
		<< ")+" << orientation 
		<< "*sqrt(-(" 
		<< ox.to_double() 
		<< ").^2 + (" 
		<< radius.to_double()
		<< ").^2"
		<< "-2*" << ox.to_double() << "*" << var
		<< "-(" << var << ").^2)";

	return s.str();
}

real circular_line2::t_to_x(real t) {
	return sgn*t-ox;
}

real circular_line2::x_to_t(real x) {
	return sgn*(x+ox);
}

real circular_line2::x_to_t(real x, real ox) {
	return sgn*(x+ox);
}