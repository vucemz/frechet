#ifndef STRAIGHT_LINE_H_INCLUDED
#define STRAIGHT_LINE_H_INCLUDED

//=================================
// forward declared dependencies
class circular_line;

//=================================
// included dependencies
#include "stdafx.h"
#include <LEDA/core/tuple.h>
#include <LEDA/numbers/real.h>
#include <LEDA/system/stream.h >
#include <LEDA/core/string.h>
#include "../FrechetDistance/curve_segment.h"
#include "../FrechetDistance/interval.h"
#if defined(LEDA_STD_IO_HEADERS)
using std::ifstream;
using std::ofstream;
using std::cin;
using std::cout;
using std::endl;
using std::flush;
#endif

//using namespace leda;
using leda::real;
using leda::list;
using leda::two_tuple;

using namespace frechet;

typedef two_tuple<real, real> real_point;

class straight_line : public frechet::curve_segment<straight_line>, public frechet::curve_segment<circular_line>  {
public:
	int sgn;
	real_point coef;	/* coef(a, b):  y = a + b x*/
	real_point limits;  /* [min_x, max_x] */
public:
	using curve_segment<straight_line>::in_range;
	straight_line(const real_point& p1, const real_point& p2);
	straight_line(straight_line* segment, const real& eps, bool positive);
	
	straight_line* copy(real min, real max);

	list<two_tuple<real, real_interval*>> offset_intersection3(straight_line* segment, const real& eps);
	list<two_tuple<real, real_interval*>> offset_intersection3(circular_line* segment, const real& eps);

	bool intersection(straight_line& segment, real& ins);
	list<real_point> intersections(circular_line& segment);

	list<real> circle_intersection(const real_point& base, const real& r);
	list<real_interval*> circle_intersection_int(const real_point& base, const real& radius);
	
	real_point coordinates(real t);
	real min_t();
	real max_t();

	real x_to_t(real x);
	real t_to_x(real t);

	string matlab_function(string var);
	string x_from_t(string var);

	list<real> preliminary_roots(straight_line* sl);
	list<function<real>*> polynomials(straight_line* segment, list<straight_line*> segments);
};

#endif