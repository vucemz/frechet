#ifndef STRAIGHT_LINE_FUNCTION_H_INCLUDED
#define STRAIGHT_LINE_FUNCTION_H_INCLUDED

#include <LEDA/numbers/real.h>

#include "straight_line.h"
#include "../FrechetDistance/free_space.h"
#include "../ParametricSearch/function.h"

using frechet::free_space;
using parametric_search::function;
using leda::real;

class straight_line_function : public function<real> {
public:
	straight_line* sl;
	list<straight_line*> segments;

	real a;	real k;
	real ox; real oy;
	int sgn;

public:
	straight_line_function(straight_line* sl, int sgn, real ox, real oy) {
		this->sl = sl;
		this->sgn = sgn;
		this->a = sl->coef.first(); this->k = sl->coef.second();
		this->ox = ox; this->oy = oy;
	}


	straight_line_function(list<straight_line*> segments, straight_line* sl, int sgn, real ox, real oy) {
		this->segments = segments;
		this->sl = sl;
		this->sgn = sgn;
		this->a = sl->coef.first(); this->k = sl->coef.second();
		this->ox = ox; this->oy = oy;
	}

	list<real> roots(function<real>* p_) {
		straight_line_function* p = static_cast<straight_line_function*>(p_);

		list<real> lst;

		// if not the same curve segment no roots
		if (this->sl != p->sl) return lst;

		real ox1 = ox; real oy1 = oy;
		real ox2 = p->ox; real oy2 = p->oy;

		// same curve use shortcut
		if (ox1 == ox2 && oy1 == oy2) {
			real root = sqrt((a+k*ox-oy)*(a+k*ox-oy)/(1+k*k));
			lst.append(root);

			return lst;
		}


		real tmp1 = (ox1 - ox2 + k*oy1 - k*oy2)*(ox1 - ox2 + k*oy1 - k*oy2);

		if (tmp1 > 0) {
			real tmp2 = (ox1*ox1 - 2*ox1*ox2 + ox2*ox2 + (oy1 - oy2)*(oy1 - oy2))*(4*a*a + (1 + k*k)*ox1*ox1 + ox2*ox2 + k*k*ox2*ox2 - 4*k*ox2*oy1 + oy1*oy1 + k*k*oy1*oy1 + 
						4*a*(k*(ox1 + ox2) - oy1 - oy2) + 2*oy1*oy2 - 2*k*k*oy1*oy2 + oy2*oy2 + k*k*oy2*oy2 + 2*ox1*((-1 + k*k)*ox2 - 2*k*oy2));

			if (tmp2 >= 0) {

				real root = sqrt(tmp2)/(2 * sqrt(tmp1));
				lst.append(root);
			}
		}

		return lst;
	}

	bool eval(real r, real& val) {
		real tmp = -(a + k*ox - oy)*(a + k*ox - oy) + (1 + k*k)*r*r;
		if (tmp >= 0) {
			val = ((-a)*k + ox + k*oy + sgn*sqrt(tmp))/(1 +  k*k);
			return true;
		}

		return false;
	}

	int compare(function<real>* p_, real low, real high) {
		straight_line_function* p = static_cast<straight_line_function*>(p_);
		// return based on cumulative t of starting points of curves
		if (this->sl != p->sl) {
			real t1; real t2;
			cumulative_t_x(this->sl, this->sl->min_t(), t1);
			cumulative_t_x(p->sl, p->sl->min_t(), t2);
			
			//return (t1 > t2) ? 1 : (t1 < t2) ? -1 : 0;
			return (t1 - t2).sign(100);
		}

		real r = low + (high-low)/2;
		real v1; real v2;

		bool b1 = this->eval(r, v1);
		bool b2 = p->eval(r, v2);

		//if (b1 && b2)  return (v1 > v2) ? 1 : (v1 < v2) ? -1 : 0;
		if (b1 && b2)  return (v1-v2).sign(100);
		if (!b1 && b2)  return -1;
		if (b1 && !b2)  return 1;
		if (!b1 && !b2)  return 0; //????

		return 0;
	}


	//-------------------------------------------------------------------------------//

	bool cumulative_t_x(straight_line* cx, real t, real& cumulative_x) {
		cumulative_x = 0;

		straight_line* c;
		forall (c, segments) {
			if (c == cx) goto f;
			cumulative_x += c->max_t() - c->min_t();
		}
		return false;

	f:	
		cumulative_x += t - c->min_t();
		return true;
	}
};

#endif