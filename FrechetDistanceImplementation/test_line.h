#ifndef TEST_LINE_H_INCLUDED
#define TEST_LINE_H_INCLUDED

//=================================
// included dependencies
#include "stdafx.h"
#include <LEDA/core/tuple.h>
#include <LEDA/numbers/real.h>
#include "../FrechetDistance/curve_segment.h"
#include "../FrechetDistance/interval.h"
#include "straight_line.h"
#if defined(LEDA_STD_IO_HEADERS)
using std::ifstream;
using std::ofstream;
using std::cin;
using std::cout;
using std::endl;
using std::flush;
#endif

using namespace leda;
using namespace frechet;

typedef two_tuple<real, real> real_point;

class test_line : public frechet::curve_segment<test_line> , public frechet::curve_segment<straight_line> {
//class test_line : public curve_segment<test_line>  {
public:
	list<two_tuple<real,real>> offset_intersection(test_line* segment, const real& eps) {
		list<two_tuple<real,real>> t; return t;
	}
	
	list<two_tuple<real,real>> offset_intersection(straight_line* segment, const real& eps) {
		list<two_tuple<real,real>> t; return t;
	}

	list<real_num_point> offset_intersection2(test_line* segment, const real& eps) {
		list<real_num_point> t; return t;
	}
	
	list<real_num_point> offset_intersection2(straight_line* segment, const real& eps) {
		list<real_num_point> t; return t;
	}

	/* returns list of parameters at which intersection with given circle occurs */
	list<real> circle_intersection(const real_point& base, const real& radius){
		list<real> lst;
		return lst;
	}

	real_point coordinates(real t){ real_point p; return p;}

	real min_t() { return 0;}
	real max_t() { return 0;}

	test_line() {

	}

	test_line(real& r) {

	}
};

#endif