#ifndef STRAIGHT_LINE2_H_INCLUDED
#define STRAIGHT_LINE2_H_INCLUDED


//=================================
// forward declared dependencies

//=================================
// included dependencies
#include "stdafx.h"
#include <LEDA/core/tuple.h>
#include <LEDA/numbers/real.h>
#include <LEDA/system/stream.h >
#include <LEDA/core/string.h>
#include "../FrechetDistance/curve_segment.h"
#include "../FrechetDistance/interval.h"



#if defined(LEDA_STD_IO_HEADERS)
using std::ifstream;
using std::ofstream;
using std::cin;
using std::cout;
using std::endl;
using std::flush;
#endif

using namespace leda;
using namespace frechet;


class straight_line2;
class circular_line2;


typedef two_tuple<real, real> real_point;

class my_curve : public frechet::curve_segment<my_curve>  {
public:
	virtual list<two_tuple<real, real_interval*>> offset_intersection3(my_curve* segment, const real& eps) =0;

	virtual list<two_tuple<real, real_interval*>> offset_intersection3_(straight_line2* segment, const real& eps) =0;
	virtual list<two_tuple<real, real_interval*>> offset_intersection3_(circular_line2* segment, const real& eps) =0;

	virtual list<real> circle_intersection(const real_point& base, const real& r) =0;
	
	virtual real_point coordinates(real t) =0;
	virtual real min_t() =0;
	virtual real max_t() =0;

	virtual list<real> points_of_tangents(real& k) =0;
	virtual my_curve* copy(real min, real max) =0;
};


class straight_line2 : public my_curve {
public:
	int sgn;
	real_point coef;	/* coef(a, b):  y = a + b x*/
	real_point limits;  /* [min_x, max_x] */


	straight_line2(const real_point& p1, const real_point& p2);
	straight_line2(straight_line2* segment, const real& eps, bool positive);

	list<two_tuple<real, real_interval*>> offset_intersection3(my_curve* segment, const real& eps);
	list<real> circle_intersection(const real_point& base, const real& r);
	
	real_point coordinates(real t);
	real min_t();
	real max_t();

	list<two_tuple<real, real_interval*>> offset_intersection3(straight_line2* segment, const real& eps);
	list<two_tuple<real, real_interval*>> offset_intersection3_(straight_line2* segment, const real& eps);
	list<two_tuple<real, real_interval*>> offset_intersection3(circular_line2* segment, const real& eps);
	list<two_tuple<real, real_interval*>> offset_intersection3_(circular_line2* segment, const real& eps);

	bool intersection(straight_line2& segment, real& ins);
	list<real_point> intersections(circular_line2& segment);

	real x_to_t(real x);
	real t_to_x(real t);

	string matlab_function(string var);
	string x_from_t(string var);

	list<real> points_of_tangents(real& k) {
		list<real> r; return r;
	}

	straight_line2* copy(real min, real max) {
		straight_line2* sl2 = new straight_line2(*this);
		sl2->limits=real_point(min, max);
		return sl2;
	}
};


#endif