#include "stdafx.h"
#include "straight_line.h"
#include "circular_line.h"

straight_line::straight_line(const real_point& p1, const real_point& p2) {
	coef = real_point(
		p1.second() + p1.first() * (p2.second() - p1.second()) / (p1.first() - p2.first()),
		(p2.second() - p1.second()) / (p2.first() - p1.first()));
	//if (p1.first() <= p2.first())
	//	limits = real_point(p1.first(), p2.first());
	//else
	//	limits = real_point(p2.first(), p1.first());

	limits = real_point(p1.first(), p2.first());

	if (limits.first() > limits.second()) {
		this->sgn = -1;
		this->limits = real_point(sgn*limits.first(), sgn*limits.second());
	}
	else {
		this->sgn = 1;
		this->limits = limits; 
	}
}

straight_line::straight_line(straight_line* segment, const real& eps, bool positive) {

	real x_offset;
	real y_offset;

	if (segment->coef.second() != 0) {
		real tmp = 1/segment->coef.second();
		x_offset = eps/sqrt(1+tmp*tmp);
		real y_offset1 = x_offset * segment->coef.second();

		//real y_offset2 = x_offset / segment->coef.second();
		real y_offset2 = x_offset * tmp;

		y_offset = y_offset2 + y_offset1;
	}
	else {
		x_offset = 0;
		y_offset = eps;
	}


	if (positive) {
		coef = real_point(segment->coef.first() + y_offset, segment->coef.second());

		real sx1 = segment->t_to_x(segment->limits.first()) - x_offset;
		real sx2 = segment->t_to_x(segment->limits.second()) - x_offset;

		limits = real_point(sx1, sx2);

		if (limits.first() > limits.second()) {
			this->sgn = -1;
			this->limits = real_point(sgn*limits.first(), sgn*limits.second());
		}
		else {
			this->sgn = 1;
			this->limits = limits; 
		}

		//limits = real_point(segment->limits.first() - x_offset, segment->limits.second() - x_offset);
	}
	else {
		coef = real_point(segment->coef.first() - y_offset, segment->coef.second());

		real sx1 = segment->t_to_x(segment->limits.first()) + x_offset;
		real sx2 = segment->t_to_x(segment->limits.second()) + x_offset;

		limits = real_point(sx1, sx2);

		if (limits.first() > limits.second()) {
			this->sgn = -1;
			this->limits = real_point(sgn*limits.first(), sgn*limits.second());
		}
		else {
			this->sgn = 1;
			this->limits = limits; 
		}

		//limits = real_point(segment->limits.first() + x_offset, segment->limits.second() + x_offset);
	}
}

straight_line* straight_line::copy(real min, real max) {
	straight_line* cp = new straight_line(*this);
	cp->limits = real_point(min, max);
	return cp;
}


list<two_tuple<real, real_interval*>> straight_line::offset_intersection3(straight_line* segment, const real& eps) {
	list<two_tuple<real, real_interval*>> lst;

	straight_line s1 = straight_line(segment, eps, true);
	straight_line s2 = straight_line(segment, eps, false);

	real ins1;
	if (intersection(s1, ins1)) {
		real x_offset = segment->t_to_x(segment->limits.first()) - s1.t_to_x(s1.limits.first());
		lst.append(two_tuple<real, real_interval*>(ins1,  new real_sp_interval(segment->x_to_t(t_to_x(ins1) + x_offset))));
		//lst.append(
		//	two_tuple<real, real_interval*>(ins1, new real_sp_interval(ins1 + x_offset)));
	}

	real ins2;
	if (intersection(s2, ins2)) {
		real x_offset = segment->t_to_x(segment->limits.first()) - s2.t_to_x(s2.limits.first());
		lst.append(two_tuple<real, real_interval*>(ins2,  new real_sp_interval(segment->x_to_t(t_to_x(ins2) + x_offset))));
		//lst.append(
		//	two_tuple<real, real_interval*>(ins2, new real_sp_interval(ins2 + x_offset)));	
	}

	return lst;
}

bool straight_line::intersection(straight_line& segment, real& ins) {

	real a = coef.first();
	real b = coef.second();
	real c = segment.coef.first();
	real d = segment.coef.second();

	if (b-d == 0) return false;

	real x = (-a+c)/(b-d);
	real t = x_to_t(x);

	//if (in_range(x) && segment.in_range(x)) {
	if (in_range(t, 100) && segment.in_range( segment.x_to_t(x), 100)) {
		ins = t;
		return true;
	}

	return false;
}

list<real_point> straight_line::intersections(circular_line& sl) {
	list<real_point> res;

	real la = coef.first();
	real lb = coef.second();
	real r = sl.radius;
	real ox = sl.ox;
	real oy = sl.oy;
	real sgn = sl.sgn;

	real b = -la*la + 2* la *(lb* ox - oy) + 2 *lb *ox *oy - oy*oy + r*r + lb*lb * (-ox*ox + r*r);
	if (b >= 0) {
		real a = la * lb + ox + lb * oy;
		real c = 1 + lb*lb;

		real rt = sqrt(b);

		real x1 = -(a - rt)/c; real t1 = sgn*(x1 + ox);
		real x2 = -(a + rt)/c; real t2 = sgn*(x2 + ox);

		//if ( t1 >= sl.min_t() && t1 <= sl.max_t() && this->in_range(x_to_t(x1))) {
		if ( sl.in_range(t1) && this->in_range(x_to_t(x1))) {
			//if (this->coordinates(t1) == sl.coordinates(x1))
			if (this->coordinates(x_to_t(x1)) == sl.coordinates(t1))
				res.append(real_point(x_to_t(x1), t1));
		}
		//if (t2 >= sl.min_t() && t2 <= sl.max_t() && this->in_range(x_to_t(x2))) {
		if (sl.in_range(t2) && this->in_range(x_to_t(x2))) {
			//if (this->coordinates(t2) == sl.coordinates(x2))
			if (this->coordinates(x_to_t(x2)) == sl.coordinates(t2))
				res.append(real_point(x_to_t(x2), t2));
		}
	}

	return res;
}


list<two_tuple<real, real_interval*>> straight_line::offset_intersection3(circular_line* segment, const real& eps) {
	list<two_tuple<real, real_interval*>> res;

	circular_line s1 = circular_line(segment, eps, true);
	circular_line s2 = circular_line(segment, eps, false);

	list<real_point> l1 = intersections(s1);
	
	real_point p;
	forall (p, l1) {
		real t = p.second();
		real ts = p.first();
		if (s1.radius != 0) 
			res.append(two_tuple<real, real_interval*>(ts, new real_sp_interval(t  * segment->radius/s1.radius )));	//TODO check
		else
			res.append(two_tuple<real, real_interval*>(ts, new real_tp_interval(segment->min_t(), segment->max_t())));
			//res.append(two_tuple<real, real_interval*>(x, new real_tp_interval(s1.min_t(), s1.max_t())));
	}

	list<real_point> l2 = intersections(s2);

	forall (p, l2) {
		real t = p.second();
		real ts = p.first();
		if (s2.radius != 0) 
			res.append(two_tuple<real, real_interval*>(ts, new real_sp_interval(t   * segment->radius/s2.radius )));	//TODO check
		else
			res.append(two_tuple<real, real_interval*>(ts, new real_tp_interval(segment->min_t(), segment->max_t())));
			//res.append(two_tuple<real, real_interval*>(x, new real_tp_interval(s2.min_t(), s2.max_t())));
	}

	return res;
}


list<real> straight_line::circle_intersection(const real_point& base, const real& r) {
	list<real> lst;

	real a = coef.first();
	real b = coef.second();
	real c = base.second();
	real d = base.first();

	real sqr = -a*a-c*c+2*b*c*d-b*b*d*d+2*a*(c-b*d)+r*r+b*b*r*r;
		
	if (sqr.compare(0) < 0) return lst;

	real tmp1 = -a*b+b*c+d;
	real tmp2 = 1+b*b;

	real x1 = (tmp1 - sqrt(sqr))/tmp2;
	real x2 = (tmp1 + sqrt(sqr))/tmp2;

	real t1 = x_to_t(x1);
	real t2 = x_to_t(x2);

	if (in_range(t1,100)) lst.append(t1);
	if (in_range(t2,100)) lst.append(t2);
		
	lst.sort();

	return lst;
}

list<real_interval*> straight_line::circle_intersection_int(const real_point& base, const real& radius) {
	list<real_interval*> res;

	list<real> intr = circle_intersection(base, radius);
	intr.push_front(this->min_t());
	intr.push_back(this->max_t());

	for (int i = 0; i < intr.size() - 1; i++) {
		real r1 = intr.contents(intr[i]);
		real r2 = intr.contents(intr[i+1]);

		if ((r1 - r2).sign(100) == 0) 
			res.append(new real_sp_interval(r1));
		else 
			res.append(new real_tp_interval(r1, r2));
	}

	return res;
}


real_point straight_line::coordinates(real t) {
	real x = t_to_x(t);

	//return real_point(t, coef.first() + coef.second() * t);
	return real_point(x, coef.first() + coef.second() * x);
}


real straight_line::min_t() {
	return limits.first();
}

real straight_line::max_t() {
	return limits.second();
}

string straight_line::x_from_t(string var) {
	leda::string_ostream s_;
	s_ << "(" << sgn << "*" << var << ")";
	return s_.str();
}

string straight_line::matlab_function(string var) {
	leda::string_ostream s;
	s << coef.first().to_double() << "+" << var << "*" << coef.second().to_double();
	return s.str();
}

real straight_line::t_to_x(real t) {
	return sgn*t;
}

real straight_line::x_to_t(real x) {
	return sgn*x;
}


list<real> straight_line::preliminary_roots(straight_line* sl) {
	list<real> roots;

	real a = sl->coef.first();
	real k = sl->coef.second();
	real_point coord1 = sl->coordinates(sl->min_t());
	real ox1 = coord1.first();
	real oy1 = coord1.second();
	real x = this->coordinates(this->min_t()).first();
	
	real r2 = a*a + ox1*ox1 - 2*a*oy1 + oy1*oy1 + 2*a*k*x - 2*ox1*x - 2*k*oy1*x + x*x + k*k*x*x;
	if (r2 >= 0) roots.append(sqrt(r2));

	x = this->coordinates(this->max_t()).first();
	
	r2 = a*a + ox1*ox1 - 2*a*oy1 + oy1*oy1 + 2*a*k*x - 2*ox1*x - 2*k*oy1*x + x*x + k*k*x*x;
	if (r2 >= 0) roots.append(sqrt(r2));

	real_point coord2 = sl->coordinates(sl->max_t());
	ox1 = coord1.first();
	oy1 = coord1.second();
	x = this->coordinates(this->min_t()).first();

	r2 = a*a + ox1*ox1 - 2*a*oy1 + oy1*oy1 + 2*a*k*x - 2*ox1*x - 2*k*oy1*x + x*x + k*k*x*x;
	if (r2 >= 0) roots.append(sqrt(r2));

	x = this->coordinates(this->max_t()).first();
	
	r2 = a*a + ox1*ox1 - 2*a*oy1 + oy1*oy1 + 2*a*k*x - 2*ox1*x - 2*k*oy1*x + x*x + k*k*x*x;
	if (r2 >= 0) roots.append(sqrt(r2));

	return roots;
}

#include "straight_line_function.h"

list<function<real>*> straight_line::polynomials(straight_line* segment, list<straight_line*> segments) {
	list<function<real>*> lst;

	straight_line_function* sl1 = new straight_line_function(segments, segment, 1, this->coordinates(this->min_t()).first(), this->coordinates(this->min_t()).second());
	straight_line_function* sl2 = new straight_line_function(segments, segment, -1, this->coordinates(this->min_t()).first(), this->coordinates(this->min_t()).second());

	straight_line_function* sl3 = new straight_line_function(segments, segment, 1, this->coordinates(this->max_t()).first(), this->coordinates(this->max_t()).second());
	straight_line_function* sl4 = new straight_line_function(segments, segment, -1, this->coordinates(this->max_t()).first(), this->coordinates(this->max_t()).second());

	lst.append(sl1);
	lst.append(sl2);
	lst.append(sl3);
	lst.append(sl4);
	return lst;
}