#include "stdafx.h"

#include <fstream>
#include <time.h>

#include <LEDA/internal/std/math.h>
#include <LEDA/internal/std/assert.h>

#include <LEDA/numbers/real.h>
#include <LEDA/numbers/integer.h>
#include <LEDA/system/basic.h>

//#include "../FrechetDistance/curve_segment.h"
//#include "../FrechetDistance/free_space_cell.h"
//#include "../FrechetDistance/transition_manager.h"
//#include "../FrechetDistance/interval.h"

#include "../FrechetDistanceImplementation/straight_line.cpp"
#include "../FrechetDistanceImplementation/circular_line.cpp"
//#include "../FrechetDistanceImplementation/test_line.h"
#include "../FrechetDistance/free_space.h"

//#include "../FrechetDistanceImplementation/straight_line2.cpp"
//#include "../FrechetDistanceImplementation/circular_line2.cpp"


using leda::real;
using leda::two_tuple;
using leda::list;

using namespace frechet;


int _tmain(int argc, _TCHAR* argv[])
{
	int t1, t2;

	int rep = 2;

	bool lp1b = false; real lp1;
	bool lp2b = false; real lp2;
	bool bp1b = false; real bp1;
	bool bp2b = false; real bp2;

	bool tp1b; real tp1;
	bool tp2b; real tp2;
	bool rp1b; real rp1;
	bool rp2b; real rp2;


	real zp1 = real(1)/real(10);
	real z = zp1;
	real u = zp1/2;





	//system("pause");

	//// #0
	//real lambda =  1 - 0.002;

	//my_curve* cl_m1_ =  new straight_line2(real_point(-2,0), real_point(-1,0));
	//my_curve* cl_m2_ =  new circular_line2(1, real_point(0,0), real_point(-1,1), 1);
	//my_curve* cl_m3_ =  new straight_line2(real_point(1,0), real_point(2,0));

	//list<my_curve*> cl_lst_;
	//cl_lst_.append(cl_m1_);
	//cl_lst_.append(cl_m2_);
	//cl_lst_.append(cl_m3_);

	//double e_ = 0.0000000;

	//my_curve* sl_r1_ =  new straight_line2(real_point(-2,0), real_point(-1,1));
	//my_curve* sl_r2_ =  new circular_line2(1, real_point(0,1), real_point(-1,1), -1);
	//my_curve* sl_r3_ =  new straight_line2(real_point(1,1), real_point(2,0));

	//list<my_curve*> sl_lst_;
	//sl_lst_.append(sl_r1_);
	//sl_lst_.append(sl_r2_);
	//sl_lst_.append(sl_r3_);

	//free_space<my_curve, my_curve> fs2_ = free_space<my_curve, my_curve>(real(1), sl_lst_, cl_lst_);
	//fs2_.plot_curves();
	//system("pause");

	//fs2_.print(lambda);

	//system("pause");

	//cout << "curve x plot ======================================================================================\n";
	//cout << "curve x plot ======================================================================================\n";
	//
	//t1 = clock();
	//fs2_.solve(lambda);
	//t2 = clock();
	//	
	//cout << "2: it took " << (t2-t1)/1000.0 << " seconds\n";







	//// random
	//straight_line sl_o1 = straight_line(real_point(0,0),real_point(0.1,zp1));
	//straight_line sl_p1 = straight_line(real_point(0,0),real_point(zp1+zp1/2,zp1*4));

	//real eps = zp1*4;
	//int i = 5;
	//int j = 5;


	//free_space<straight_line, straight_line> fscc7 = free_space<straight_line,straight_line>(&sl_o1, &sl_p1, eps);
	//free_space_cell<straight_line, straight_line> fscc8 = free_space_cell<straight_line,straight_line>(&sl_o1, &sl_p1, eps);


	//straight_line* sl_o;
	//real_point prev = real_point(zp1,zp1);
	//for (int a = 0; a < i; a++) {
	//	real_point nw = real_point( (a+2)*zp1, real(rand()%100)/100 );
	//	cout << prev << " -> " << nw << "\n";
	//	sl_o = new straight_line(prev,nw);

	//	fscc7.append_curve_x(sl_o);
	//	prev = nw;
	//}

	//cout << "-----------------\n";

	//straight_line* sl_p;
	//prev = real_point(zp1+zp1/2,zp1*4);
	//for (int a = 0; a < j; a++) {
	//	real_point nw = real_point( (a+2)*zp1, real(rand()%100)/100 );
	//	cout << prev << " -> " << nw << "\n";
	//	sl_p = new straight_line(prev,nw);

	//	fscc7.append_curve_y(sl_p);
	//	prev = nw;
	//}


	// random
	//straight_line sl_o1 = straight_line(real_point(0,0),real_point(zp1,zp1));
	//circular_line cl_p1 = circular_line(zp1, real_point(zp1, 0), real_point(-zp1,zp1), 1);
	//circular_line cl_p5 = circular_line(zp1, real_point(real(1)/real(2), 0), real_point(-zp1,zp1), 1);

	////real eps = real(6)/real(10);
	//real eps = real(6)/real(10);
	////eps = 0.6;
	////real eps = real(6)/real(10) - real(1)/(real(10000000));
	//int i = 5;
	//int j = 4;


	//free_space<circular_line, straight_line> fscc7 = free_space<circular_line,straight_line>(&sl_o1, &cl_p1);
	//free_space_cell<circular_line, straight_line> fscc8 = free_space_cell<circular_line,straight_line>(&sl_o1, &cl_p5);

	//straight_line* sl_o;
	//real_point prev = real_point(zp1,zp1);
	//for (int a = 0; a < i; a++) {
	//	real_point nw = real_point( (a+2)*zp1, real(rand()%100)/100 );
	//	sl_o = new straight_line(prev,nw);

	//	fscc7.append_curve_x(sl_o);
	//	prev = nw;
	//}

	//cout << "-----------------\n";

	//circular_line* cl_p;
	//for (int a = 1; a < j; a++) {
	//	real_point nw = real_point( a*(zp1+zp1) + zp1,0 );
	//	cl_p = new circular_line(zp1, nw, real_point(-zp1,zp1), 1);

	//	fscc7.append_curve_y(cl_p);
	//	prev = nw;
	//}

	////fscc7.print();
	//system("pause");

	//t1 = clock();
	////fscc7.solve();
	//t2 = clock();
	//	
	//cout << "it took " << (t2-t1)/1000.0 << " seconds\n";


	//lp1b = false;
	//lp2b = false;
	//bp1b = true; bp1 = 0;
	//bp2b = false;

	//	fscc8.solve(
	//		lp1b, lp1,
	//		lp2b, lp2,
	//		bp1b, bp1,
	//		bp2b, bp2,

	//		tp1b, tp1,
	//		tp2b, tp2,
	//		rp1b, rp1,
	//		rp2b, rp2);
	//

	//cout << "results for top axis: ";
	//if (tp1b) cout << tp1.to_double() << " | ";
	//else cout << "no tp1 | ";
	//if (tp2b) cout << tp2.to_double() << "\n";
	//else cout << "no tp2\n";

	//cout << "results for right axis: ";
	//if (rp1b) cout << rp1.to_double() << " | ";
	//else cout << "no tp1 | ";
	//if (rp2b) cout << rp2.to_double() << "\n";
	//else cout << "no tp2\n";

	system("pause");







	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


	//circular_line cln10 = circular_line(1, real_point(0,0), real_point(-1,1), 1);
	//straight_line sl_x = straight_line(real_point(-1,2),real_point(1,-2));
	//free_space_cell<circular_line, straight_line>* fscc4 = new free_space_cell<circular_line, straight_line>(&sl_x, &cln10, 2.3);

	//lp1b = true; lp1 = -0.4;
	//lp2b = false;
	//bp1b = false; bp1 = -0.4;
	//bp2b = false;


	//t1 = clock();
	//for (int i = 0; i < rep; i++) {
	//	circular_line cln10 = circular_line(1, real_point(0,0), real_point(-1,1), 1);
	//	straight_line sl_x = straight_line(real_point(-1,2),real_point(1,-2));
	//	free_space_cell<circular_line, straight_line>* fscc4 = new free_space_cell<circular_line, straight_line>(&sl_x, &cln10, 2.3);

	//	fscc4->solve(
	//		lp1b, lp1,
	//		lp2b, lp2,
	//		bp1b, bp1,
	//		bp2b, bp2,

	//		tp1b, tp1,
	//		tp2b, tp2,
	//		rp1b, rp1,
	//		rp2b, rp2);
	//}

	//t2 = clock();
	//cout << "it took " << (t2-t1)/1000.0/rep << " seconds\n";

	//cout << "results for top axis: ";
	//if (tp1b) cout << tp1.to_double() << " | ";
	//else cout << "no tp1 | ";
	//if (tp2b) cout << tp2.to_double() << "\n";
	//else cout << "no tp2\n";

	//cout << "results for right axis: ";
	//if (rp1b) cout << rp1.to_double() << " | ";
	//else cout << "no tp1 | ";
	//if (rp2b) cout << rp2.to_double() << "\n";
	//else cout << "no tp2\n";

	//cout << "cell plot =========================================================================================\n";
	//cout << fscc4->matlab_plot_cell(0.1) << "\n";

	//cout << "curve x plot ======================================================================================\n";
	//cout << fscc4->matlab_plot_curve_x(0.01, 1) << "\n";
	//cout << fscc4->matlab_plot_curve_y(0.01, 2) << "\n";







	//cout << "curve x plot ======================================================================================\n";
	//cout << "curve x plot ======================================================================================\n";

	//straight_line sl_m1 = straight_line(real_point(0,0),real_point(5,0));
	//straight_line sl_m2 = straight_line(real_point(5,0),real_point(7,1));
	//straight_line sl_m3 = straight_line(real_point(7,1),real_point(8,6));

	//straight_line sl_n1 = straight_line(real_point(0,0),real_point(1,1));
	//straight_line sl_n2 = straight_line(real_point(1,1),real_point(2,-1));
	//straight_line sl_n3 = straight_line(real_point(2,-1),real_point(3,2));
	//straight_line sl_n4 = straight_line(real_point(3,2),real_point(5,0));
	//straight_line sl_n5 = straight_line(real_point(5,0),real_point(6,1));


	//circular_line cl_u1 =  circular_line(1, real_point(0,0), real_point(-1,1), 1);
	//circular_line cl_u2 =  circular_line(1, real_point(2,0), real_point(-1,1), 1);
	//circular_line cl_u3 =  circular_line(0.5, real_point(3+0.5,0), real_point(-0.5,0.5), 1);

	//straight_line sl_z1 = straight_line(real_point(-1,0),real_point(-0.5,0.8));
	//straight_line sl_z2 = straight_line(real_point(-0.5,0.8),real_point(0,1.5));
	//straight_line sl_z3 = straight_line(real_point(0,1.5),real_point(0.4,0.3));
	//straight_line sl_z4 = straight_line(real_point(0.4,0.3),real_point(1,0));
	//straight_line sl_z5 = straight_line(real_point(1,0),real_point(2,0.6));
	//straight_line sl_z6 = straight_line(real_point(2,0.6),real_point(3,0));
	//straight_line sl_z7 = straight_line(real_point(3,0),real_point(3.5,0.1));
	
	/*free_space<straight_line, straight_line> fs = free_space<straight_line, straight_line>(&sl_x, &sl_x2, 1);*/
	//free_space<straight_line, straight_line> fs = free_space<straight_line, straight_line>(&sl_n1, &sl_m1, 8);
	//fs.append_curve_x(&sl_n2);
	//fs.append_curve_x(&sl_n3);
	//fs.append_curve_x(&sl_n4);
	//fs.append_curve_x(&sl_n5);

	//fs.append_curve_y(&sl_m2);
	//fs.append_curve_y(&sl_m3);

	//free_space<circular_line, straight_line> fs = free_space<circular_line, straight_line>(&sl_z1, &cl_u1, 0.53);
	//free_space<circular_line, straight_line> fs = free_space<circular_line, straight_line>(&sl_z1, &cl_u1);
	//fs.append_curve_y(&cl_u2);
	//fs.append_curve_y(&cl_u3);

	//fs.append_curve_x(&sl_z2);
	//fs.append_curve_x(&sl_z3);
	//fs.append_curve_x(&sl_z4);
	//fs.append_curve_x(&sl_z5);
	//fs.append_curve_x(&sl_z6);
	//fs.append_curve_x(&sl_z7);

	cout << "curve x plot ======================================================================================\n";
	cout << "curve x plot ======================================================================================\n";
	//fs.print();

	t1 = clock();
	rep = 1;
	//for (int i = 0; i < rep; i++)
	//	fs.solve();
	t2 = clock();
	cout << "it took " << (t2-t1)/1000.0/rep << " seconds\n";


	//free_space_cell<circular_line, straight_line> fsc_ = free_space_cell<circular_line, straight_line>(&sl_z1, &cl_u1, 0.6);
	//cout << fsc_.matlab_plot_cell(0.01)<<"\n";

	//lp1b = true; lp1 = -1;
	//lp2b = false;
	//bp1b = false;
	//bp2b = false;

	//	fsc_.solve(
	//		lp1b, lp1,
	//		lp2b, lp2,
	//		bp1b, bp1,
	//		bp2b, bp2,

	//		tp1b, tp1,
	//		tp2b, tp2,
	//		rp1b, rp1,
	//		rp2b, rp2);

	//cout << "results for top axis: ";
	//if (tp1b) cout << tp1.to_double() << " | ";
	//else cout << "no tp1 | ";
	//if (tp2b) cout << tp2.to_double() << "\n";
	//else cout << "no tp2\n";

	//cout << "results for right axis: ";
	//if (rp1b) cout << rp1.to_double() << " | ";
	//else cout << "no tp1 | ";
	//if (rp2b) cout << rp2.to_double() << "\n";
	//else cout << "no tp2\n";

	system("pause");






	// #1

	circular_line cl_m1 =  circular_line(1, real_point(0,0), real_point(-1,1), 1);
	circular_line cl_m2 =  circular_line(zp1*2, real_point(1+zp1*2,0), real_point(-zp1*2,zp1*2), -1);
	circular_line cl_m3 =  circular_line(zp1, real_point(1+zp1*5,0), real_point(-zp1,zp1), 1);
	circular_line cl_m4 =  circular_line(zp1*2, real_point(1+zp1*8,0), real_point(-zp1*2,0), -1);
	circular_line cl_m5 =  circular_line(zp1*3, real_point(1+zp1*8,zp1), real_point(0,zp1*3), -1);

	list<circular_line*> cl_lst;
	cl_lst.append(&cl_m1);
	cl_lst.append(&cl_m2);
	cl_lst.append(&cl_m3);
	cl_lst.append(&cl_m4);
	cl_lst.append(&cl_m5);

	straight_line sl_r1 = straight_line(real_point(-1,zp1*5),real_point(-zp1*4,1));
	straight_line sl_r2 = straight_line(real_point(-zp1*4,1),real_point(0,zp1*3));
	straight_line sl_r3 = straight_line(real_point(0,zp1*3),real_point(zp1*5,zp1*2));
	straight_line sl_r4 = straight_line(real_point(zp1*5,zp1*2),real_point(1,zp1*5));
	straight_line sl_r5 = straight_line(real_point(1,zp1*5),real_point(1+zp1*5,0));

	list<straight_line*> sl_lst;
	sl_lst.append(&sl_r1);
	sl_lst.append(&sl_r2);
	sl_lst.append(&sl_r3);
	sl_lst.append(&sl_r4);
	sl_lst.append(&sl_r5);

	real l = 0.706;
	free_space<circular_line, straight_line> fs2 = free_space<circular_line, straight_line>(sl_lst, cl_lst);


	// #2

	//circular_line cl_m1 =  circular_line(1, real_point(0,0), real_point(-1,1), 1);
	//circular_line cl_m2 =  circular_line(zp1*5, real_point(1+zp1*5,0), real_point(-zp1*5,zp1*3), -1);

	//list<circular_line*> cl_lst;
	//cl_lst.append(&cl_m1);
	//cl_lst.append(&cl_m2);

	//straight_line sl_r1 = straight_line(real_point(-1,zp1*2),real_point(-zp1*2,-zp1));
	//straight_line sl_r2 = straight_line(real_point(-zp1*2,-zp1),real_point(zp1*2,zp1));
	//straight_line sl_r3 = straight_line(real_point(zp1*2,zp1),real_point(zp1*5,zp1*5));
	//straight_line sl_r4 = straight_line(real_point(zp1*5,zp1*5),real_point(1,zp1));
	//straight_line sl_r5 = straight_line(real_point(1,zp1),real_point(1+zp1*2,-8*zp1));
	//straight_line sl_r6 = straight_line(real_point(1+zp1*2,-8*zp1),real_point(1+zp1*4,zp1));
	//straight_line sl_r7 = straight_line(real_point(1+zp1*4,zp1),real_point(1+zp1*6,-zp1));
	//straight_line sl_r8 = straight_line(real_point(1+zp1*6,-zp1),real_point(1+zp1*8,zp1*5));

	//list<straight_line*> sl_lst;
	//sl_lst.append(&sl_r1);
	//sl_lst.append(&sl_r2);
	//sl_lst.append(&sl_r3);
	//sl_lst.append(&sl_r4);
	//sl_lst.append(&sl_r5);
	//sl_lst.append(&sl_r6);
	//sl_lst.append(&sl_r7);
	//sl_lst.append(&sl_r8);

	//real l = 0.999;
	//free_space<circular_line, straight_line> fs2 = free_space<circular_line, straight_line>(sl_lst, cl_lst);
	//fs2.print(real(1));
	//system("pause");


	// #3

	//straight_line sl_r1 = straight_line(real_point(0,0),real_point(z*5,z*7));
	//straight_line sl_r2 = straight_line(real_point(z*5,z*7),real_point(z*6, z*5));
	//straight_line sl_r3 = straight_line(real_point(z*6, z*5),real_point(z*7, z*9));
	//straight_line sl_r4 = straight_line(real_point(z*7, z*9),real_point(z*8, z*4+u));
	//straight_line sl_r5 = straight_line(real_point(z*8, z*4+u),real_point(z*8+u, z*7));
	//straight_line sl_r6 = straight_line(real_point(z*8+u, z*7),real_point(z*9, z*4));
	//straight_line sl_r7 = straight_line(real_point(z*9, z*4),real_point(1, z*6));
	//straight_line sl_r8 = straight_line(real_point(1, z*6),real_point(1+z*2, z));
	//straight_line sl_r9 = straight_line(real_point(1+z*2, z),real_point(1+z*6, z*3));

	//straight_line sl_k1 = straight_line(real_point(0, z*2),real_point(z*4, z*7+u));
	//straight_line sl_k2 = straight_line(real_point(z*4, z*7+u),real_point(z*7, z*5));
	//straight_line sl_k3 = straight_line(real_point(z*7, z*5),real_point(1+z*2, z*4));
	//straight_line sl_k4 = straight_line(real_point(1+z*2, z*4),real_point(1+z*6, z*2));

	//list<straight_line*> sl_lst1;
	//sl_lst1.append(&sl_r1);
	//sl_lst1.append(&sl_r2);
	//sl_lst1.append(&sl_r3);
	//sl_lst1.append(&sl_r4);
	//sl_lst1.append(&sl_r5);
	//sl_lst1.append(&sl_r6);
	//sl_lst1.append(&sl_r7);
	//sl_lst1.append(&sl_r8);
	//sl_lst1.append(&sl_r9);

	//list<straight_line*> sl_lst2;
	//sl_lst2.append(&sl_k1);
	//sl_lst2.append(&sl_k2);
	//sl_lst2.append(&sl_k3);
	//sl_lst2.append(&sl_k4);

	//real l = z*3+u/6;
	//free_space<straight_line, straight_line> fs2 = free_space<straight_line, straight_line>(sl_lst1, sl_lst2);
	//fs2.print(real(0.305941));
	//system("pause");
	//cout << "\n";
	//fs2.print(real(0.307289));
	//system("pause");

	// #4

	//circular_line cl_m1 =  circular_line(1, real_point(0,0), real_point(-1,1), 1);

	//list<circular_line*> cl_lst;
	//cl_lst.append(&cl_m1);

	//double e = 0.0000002;

	//straight_line sl_r1 = straight_line(real_point(-1,zp1*2),real_point(-zp1*2,-zp1));
	//straight_line sl_r2 = straight_line(real_point(-zp1*2,-zp1),real_point(2*e, e));


	//list<straight_line*> sl_lst;
	//sl_lst.append(&sl_r1);
	//sl_lst.append(&sl_r2);
	//real l = 1;
	//free_space<circular_line, straight_line> fs2 = free_space<circular_line, straight_line>(sl_lst, cl_lst);


	// #5

	//straight_line cl_m1 =  straight_line(real_point(0,0),real_point(2,1));
	//straight_line cl_m2 =  straight_line(real_point(2,1),real_point(1,z*6));

	//list<straight_line*> cl_lst;
	//cl_lst.append(&cl_m1);
	//cl_lst.append(&cl_m2);

	//straight_line sl_r1 = straight_line(real_point(0,0),real_point(1,1));
	//straight_line sl_r2 = straight_line(real_point(1,1),real_point(2,0));


	//list<straight_line*> sl_lst;
	//sl_lst.append(&sl_r1);
	//sl_lst.append(&sl_r2);
	//real l = 1;
	//free_space<straight_line, straight_line> fs2 = free_space<straight_line, straight_line>(sl_lst, cl_lst);

	// #6

	//straight_line cl_m1 =  straight_line(real_point(0,0),real_point(7,0));
	//straight_line cl_m2 =  straight_line(real_point(7,0),real_point(8+7*z, 2));
	//straight_line cl_m3 =  straight_line(real_point(8+7*z, 2),real_point(1+7*z, 1+7*z));
	//straight_line cl_m4 =  straight_line(real_point(1+7*z, 1+7*z),real_point(0, 3+z*5));
	////straight_line cl_m5 =  straight_line(real_point(0, 3+z*5),real_point(5, 3+z*7));
	//straight_line cl_m5 =  straight_line(real_point(0, 3+z*5),real_point(5, 3+z*2));

	//list<straight_line*> cl_lst;
	//cl_lst.append(&cl_m1);
	//cl_lst.append(&cl_m2);
	//cl_lst.append(&cl_m3);
	//cl_lst.append(&cl_m4);
	//cl_lst.append(&cl_m5);

	//straight_line sl_r1 = straight_line(real_point(-1,5+z*2),real_point(7,5+z*2));
	//straight_line sl_r2 =  straight_line(real_point(7,5+z*2),real_point(8+z*8, 7));
	//straight_line sl_r3 =  straight_line(real_point(8+z*8, 7),real_point(4, 7+z*3));
	//straight_line sl_r4 =  straight_line(real_point(4, 7+z*3),real_point(0, 7));
	//straight_line sl_r5 =  straight_line(real_point(0, 7),real_point(z, 8+8*z));
	//straight_line sl_r6 =  straight_line(real_point(z, 8+8*z),real_point(7+z*2, 8+z*5));

	//list<straight_line*> sl_lst;
	//sl_lst.append(&sl_r1);
	//sl_lst.append(&sl_r2);
	//sl_lst.append(&sl_r3);
	//sl_lst.append(&sl_r4);
	//sl_lst.append(&sl_r5);
	//sl_lst.append(&sl_r6);

	////5.73256 - 5.73847
	//real l = 5.73256;
	//free_space<straight_line, straight_line> fs2 = free_space<straight_line, straight_line>(sl_lst, cl_lst);

	// #7

	//circular_line cl_m1 =  circular_line(z, real_point(0,0), real_point(-z,0), 1);
	//circular_line cl_m2 =  circular_line(z*2, real_point(0,-z), real_point(0,z*2), 1);
	//circular_line cl_m3 =  circular_line(z*3, real_point(-z,-z), real_point(z*3, 0), -1);
	//circular_line cl_m4 =  circular_line(z*4, real_point(-z,0), real_point(0, -z*4), -1);
	//circular_line cl_m5 =  circular_line(z*5, real_point(0,0), real_point(-z*5,-z*4-z/2-z/3-z/10), 1);

	//list<circular_line*> cl_lst;
	//cl_lst.append(&cl_m1);
	//cl_lst.append(&cl_m2);
	//cl_lst.append(&cl_m3);
	//cl_lst.append(&cl_m4);
	////cl_lst.append(&cl_m5);

	//straight_line sl_r1 = straight_line(real_point(-z/2,0),real_point(0,z));
	//straight_line sl_r2 = straight_line(real_point(0,z),real_point(2*z,0));
	//straight_line sl_r3 = straight_line(real_point(2*z,0),real_point(2*z-z/10,-2*z));
	//straight_line sl_r4 = straight_line(real_point(2*z-z/10,-2*z),real_point(-z,-z*4));
	//straight_line sl_r5 = straight_line(real_point(-z,-z*4),real_point(-4*z,-3*z));
	//straight_line sl_r6 = straight_line(real_point(-4*z,-3*z), real_point(-5*z,0));
	////straight_line sl_r5 = straight_line(real_point(-4*z,-3*z), real_point(-z,-z*4));

	//list<straight_line*> sl_lst;
	//sl_lst.append(&sl_r1);
	//sl_lst.append(&sl_r2);
	//sl_lst.append(&sl_r3);
	//sl_lst.append(&sl_r4);
	//sl_lst.append(&sl_r5);
	//sl_lst.append(&sl_r6);

	//
	//real eps_x = sqrt(
	//	(-real(0.04) + sqrt(real(0.008))) * (-real(0.04) + sqrt(real(0.008))) +
	//	(real(0.02) - sqrt(real(0.002))) * (real(0.02) - sqrt(real(0.002))));

	//real l = eps_x;
	////free_space<circular_line, straight_line> fs2 = free_space<circular_line, straight_line>(sl_lst, cl_lst, z/2+z/18);
	//free_space<circular_line, straight_line> fs2 = free_space<circular_line, straight_line>(sl_lst, cl_lst);


	// #8

	//circular_line cl_m1 =  circular_line(1, real_point(0,0), real_point(-1,1), 1);

	//list<circular_line*> cl_lst;
	//cl_lst.append(&cl_m1);

	//straight_line sl_r1 = straight_line(real_point(-1-5*z, 3*z),real_point(1+6*z,3*z));

	//list<straight_line*> sl_lst;
	//sl_lst.append(&sl_r1);
	//real l = 7*z - 0.00002;
	//free_space<circular_line, straight_line> fs2 = free_space<circular_line, straight_line>(sl_lst, cl_lst);

	//list<two_tuple<real, real_interval*>> lst = sl_r1.offset_intersection3(&cl_m1, 7*z-0.0002);
	//two_tuple<real, real_interval*> x;
	//forall(x, lst) cout << x.first().to_double() << "\n";
	//system("pause");

	// #9

	//straight_line cl_m1 =  straight_line(real_point(-1,0),real_point(0,1));
	//straight_line cl_m2 =  straight_line(real_point(0,1),real_point(1, 1));
	//straight_line cl_m3 =  straight_line(real_point(1,1),real_point(2,0));

	//list<straight_line*> cl_lst;
	//cl_lst.append(&cl_m1);
	//cl_lst.append(&cl_m2);
	//cl_lst.append(&cl_m3);

	//straight_line sl_r1 = straight_line(real_point(-1,z*5),real_point(0,0));
	//straight_line sl_r2 =  straight_line(real_point(0,0),real_point(1,0));
	//straight_line sl_r3 =  straight_line(real_point(1,0),real_point(2,z*7));

	//list<straight_line*> sl_lst;
	//sl_lst.append(&sl_r1);
	//sl_lst.append(&sl_r2);
	//sl_lst.append(&sl_r3);

	//real l = 1.2;

	//free_space<straight_line, straight_line> fs2 = free_space<straight_line, straight_line>(sl_lst, cl_lst);

	// #10

	//straight_line cl_m1 =  straight_line(real_point(0,0),real_point(7,0));
	//straight_line cl_m2 =  straight_line(real_point(7,0),real_point(8+7*z, 2));
	//straight_line cl_m3 =  straight_line(real_point(8+7*z, 2),real_point(1+7*z, 1+7*z));

	//list<straight_line*> cl_lst;
	//cl_lst.append(&cl_m1);
	//cl_lst.append(&cl_m2);
	//cl_lst.append(&cl_m3);

	//straight_line sl_r1 = straight_line(real_point(-1,5+z*2),real_point(7,5+z*2));
	//straight_line sl_r2 =  straight_line(real_point(7,5+z*2),real_point(8+z*8, 7));
	//straight_line sl_r3 =  straight_line(real_point(8+z*8, 7),real_point(4, 7+z*3));

	//list<straight_line*> sl_lst;
	//sl_lst.append(&sl_r1);
	//sl_lst.append(&sl_r2);
	//sl_lst.append(&sl_r3);

	//real l = 5+z*5;
	//free_space<straight_line, straight_line> fs2 = free_space<straight_line, straight_line>(sl_lst, cl_lst);
	//fs2.plot_curves();
	//fs2.print(real(5.81652 )); 
	//cout << "\n";
	//system("pause");

	//fs2.print(real(6.05392 )); 
	//system("pause");
	//t1 = clock();
	//fs2.optimization();
	//t2 = clock();
	//cout << "it took " << (t2-t1)/1000.0 << " seconds\n";
	//system("pause");

	//#11

	//straight_line sl_r1 = straight_line(real_point(0,0),real_point(z*5,z*7));
	//straight_line sl_r2 = straight_line(real_point(z*5,z*7),real_point(z*6, z*5));
	//straight_line sl_r3 = straight_line(real_point(z*6, z*5),real_point(z*7, z*9));
	//straight_line sl_r4 = straight_line(real_point(z*7, z*9),real_point(z*8, z*4+u));
	//straight_line sl_r5 = straight_line(real_point(z*8, z*4+u),real_point(z*8+u, z*7));
	//straight_line sl_r6 = straight_line(real_point(z*8+u, z*7),real_point(z*9, z*4));
	//straight_line sl_r7 = straight_line(real_point(z*9, z*4),real_point(1, z*6));
	//straight_line sl_r8 = straight_line(real_point(1, z*6),real_point(1+z*2, z));
	//straight_line sl_r9 = straight_line(real_point(1+z*2, z),real_point(1+z*6, z*3));



	//straight_line sl_k1 = straight_line(real_point(0, z*2),real_point(z*4, z*7+u));
	//straight_line sl_k2 = straight_line(real_point(z*4, z*7+u),real_point(z*7, z*5));
	//straight_line sl_k3 = straight_line(real_point(z*7, z*5),real_point(1+z*2, z*4));
	//straight_line sl_k4 = straight_line(real_point(1+z*2, z*4),real_point(1+z*6, z*2));

	//list<straight_line*> sl_lst1;
	//sl_lst1.append(&sl_r1);
	//sl_lst1.append(&sl_r2);
	//sl_lst1.append(&sl_r3);
	//sl_lst1.append(&sl_r4);
	//sl_lst1.append(&sl_r5);


	//list<straight_line*> sl_lst2;
	//sl_lst2.append(&sl_k1);
	//sl_lst2.append(&sl_k2);
	//sl_lst2.append(&sl_k3);


	//real l = z*3+u/6;
	//free_space<straight_line, straight_line> fs2 = free_space<straight_line, straight_line>(sl_lst1, sl_lst2);
	//fs2.plot_curves();
	//fs2.print(real(0.451471));
	//system("pause");
	//fs2.print(real(0.460977));
	////0.278994 - 0.307289
	//// 0.451471 - 0.460977
	//system("pause");
	//t1 = clock();
	//fs2.optimization();
	//t2 = clock();
	//cout << "it took " << (t2-t1)/1000.0 << " seconds\n";
	//system("pause");


	// #12
	//straight_line sl_r1 = straight_line(real_point(1,0.4),real_point(0,0));
	//straight_line sl_r2 = straight_line(real_point(0,0),real_point(1,0));
	//straight_line sl_r3 = straight_line(real_point(1,0),real_point(0,0.4));

	//straight_line sl_k2 = straight_line(real_point(1, 0.4),real_point(0, 0.4));


	//list<straight_line*> sl_lst1;
	//sl_lst1.append(&sl_r1);
	//sl_lst1.append(&sl_r2);
	//sl_lst1.append(&sl_r3);


	//list<straight_line*> sl_lst2;
	//sl_lst2.append(&sl_k2);


	//real l = z*3+u/6;
	//free_space<straight_line, straight_line> fs2 = free_space<straight_line, straight_line>(sl_lst1, sl_lst2);


	//circular_line cl_o1 =  circular_line(1, real_point(0,0), real_point(-1,1), 1);

	//list<circular_line*> cl_lst1;
	//cl_lst1.append(&cl_o1);


	//circular_line cl_z1 =  circular_line(1, real_point(0,-1), real_point(-1,1), 1);
	//list<circular_line*> cl_lst2;
	//cl_lst2.append(&cl_z1);
	//
	//
	//l = 1;
	//free_space<circular_line, circular_line> fs6 = free_space<circular_line, circular_line>(cl_lst2, cl_lst1);
	//fs6.print(l, cout, 400);
	//fs6.solve(l);
	//system("pause");
	//system("pause");

	fs2.plot_cells(l);
	system("pause");

	t1 = clock();
	//cout << "optimal value: " << fs2.optimization().to_double() << "\n";
	cout << "solution: " << fs2.solve(l) << "\n";
	t2 = clock();
	cout << "it took " << (t2-t1)/1000.0 << " seconds\n";
	system("pause");
	return 0;


	cout << "curve x plot ======================================================================================\n";
	cout << "curve x plot ======================================================================================\n";
	real cumt = -1;
	//if ( fs2.cumulative_t_y(&cl_m4, 0.4, cumt))
	//cout << "t = " << cumt.to_double() << "\n";

	fs2.plot_curves();
	system("pause");

	fs2.plot_cells(l);

	system("pause");

	t1 = clock();
	fs2.solve(l);
	t2 = clock();
		
	cout << "it took " << (t2-t1)/1000.0 << " seconds\n";

	//free_space_cell<circular_line, straight_line> fscc5 = fs2.cell;
	//free_space_cell<circular_line, straight_line> fscc5 = free_space_cell<circular_line,straight_line>(&sl_w3, &cl_m3, 1.9);
	//cout << fscc5.matlab_plot_cell(0.01) << "\n";

	//lp1b = true; lp1 = 0;
	//lp2b = false;
	//bp1b = false;
	//bp2b = false;

		//fscc5.solve(
		//	lp1b, lp1,
		//	lp2b, lp2,
		//	bp1b, bp1,
		//	bp2b, bp2,

		//	tp1b, tp1,
		//	tp2b, tp2,
		//	rp1b, rp1,
		//	rp2b, rp2);

	//fscc5.left2right(lp1, rp1b, rp1, rp2b, rp2);

	//cout << "results for top axis: ";
	//if (tp1b) cout << tp1.to_double() << " | ";
	//else cout << "no tp1 | ";
	//if (tp2b) cout << tp2.to_double() << "\n";
	//else cout << "no tp2\n";

	//cout << "results for right axis: ";
	//if (rp1b) cout << rp1.to_double() << " | ";
	//else cout << "no tp1 | ";
	//if (rp2b) cout << rp2.to_double() << "\n";
	//else cout << "no tp2\n";




	//my_curve* mc_1 = new straight_line2(real_point(-1,0),real_point(0,1));
	//my_curve* mc_2 = new straight_line2(real_point(-1,0.4),real_point(0,0.8));

	//free_space<my_curve, my_curve> fs_mc = free_space<my_curve, my_curve>(mc_1, mc_2, 1);
	//fs_mc.solve();


	system("pause");
	return 0;
}

