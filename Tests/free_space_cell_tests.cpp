#include "stdafx.h"

#include "C:\Program Files\UnitTest++\src\UnitTest++.h"
#include "../FrechetDistance/curve_segment.h"
#include "../FrechetDistance/free_space_cell.h"
#include "../FrechetDistance/transition_manager.h"
#include "../FrechetDistance/interval.h"

#include "../FrechetDistanceImplementation/straight_line.cpp"
#include "../FrechetDistanceImplementation/circular_line.cpp"
#include "../FrechetDistanceImplementation/test_line.h"


template<class T>
bool compare(list<T> lst1, list<T> lst2) {
	if (lst1.size() != lst2.size()) return false;

	for (int i = 0; i < lst1.size(); i++) {
		if (lst1.contents(lst1[i]) != lst2.contents(lst2[i])) return false;
	}
	return true;
}
/*
TEST(control_points)
{
	straight_line sl2 = straight_line(real_point(-3,-3), real_point(2,2));
	straight_line sl1 = straight_line(real_point(-4,0), real_point(4,0));
	free_space_cell<straight_line, straight_line> fsc = free_space_cell<straight_line, straight_line>(&sl1, &sl2, 1);

	list<real> result = fsc.control_points<straight_line, straight_line>(fsc.segment_x, fsc.segment_y, -4, 4, false);
	list<real> expected; expected.append(-4); expected.append(-sqrt(real(2))); expected.append(-1); expected.append(1); expected.append(sqrt(real(2))); expected.append(4);
	CHECK(compare<real>(result, expected));

	result = fsc.control_points(fsc.segment_x, fsc.segment_y, -sqrt(real(2)), 4, false);
	expected.clear(); expected.append(-sqrt(real(2))); expected.append(-1); expected.append(1); expected.append(sqrt(real(2))); expected.append(4);
	CHECK(compare<real>(result, expected));
	
	result = fsc.control_points(fsc.segment_x, fsc.segment_y, -1.2, 4, false);
	expected.clear(); expected.append(-1.2); expected.append(-1); expected.append(1); expected.append(sqrt(real(2))); expected.append(4);
	CHECK(compare<real>(result, expected));

	result = fsc.control_points(fsc.segment_x, fsc.segment_y, -1, 4, false);
	expected.clear(); expected.append(-1); expected.append(1); expected.append(sqrt(real(2))); expected.append(4);
	CHECK(compare<real>(result, expected));

	result = fsc.control_points(fsc.segment_x, fsc.segment_y, 0, 4, false);
	expected.clear(); expected.append(0); expected.append(1); expected.append(sqrt(real(2))); expected.append(4);
	CHECK(compare<real>(result, expected));

	result = fsc.control_points(fsc.segment_x, fsc.segment_y, 1, 4, false);
	expected.clear(); expected.append(1); expected.append(sqrt(real(2))); expected.append(4);
	CHECK(compare<real>(result, expected));

	result = fsc.control_points(fsc.segment_x, fsc.segment_y, sqrt(real(2)), 4, false);
	expected.clear(); expected.append(sqrt(real(2))); expected.append(4);
	CHECK(compare<real>(result, expected));

	result = fsc.control_points(fsc.segment_x, fsc.segment_y, 3, 4, false);
	expected.clear(); expected.append(3); expected.append(4);
	CHECK(compare<real>(result, expected));

	//----------------------------------------------------------------------------------------------------------------------------//

	sl2 = straight_line(real_point(-3,-3), real_point(1,1));
	sl1 = straight_line(real_point(-4,0), real_point(4,0));
	fsc = free_space_cell<straight_line, straight_line>(&sl1, &sl2, 1);

	result = fsc.control_points(fsc.segment_x, fsc.segment_y, -2, 4, false);
	expected.clear(); expected.append(-2); expected.append(-sqrt(real(2))); expected.append(-1); expected.append(1); expected.append(1); expected.append(sqrt(real(2))); expected.append(4);
	CHECK(compare<real>(result, expected));

	result = fsc.control_points(fsc.segment_x, fsc.segment_y, 1, 4, false);
	expected.clear(); expected.append(1); expected.append(sqrt(real(2))); expected.append(4);
	CHECK(compare<real>(result, expected));


	//----------------------------------------------------------------------------------------------------------------------------//

	sl2 = straight_line(real_point(-3,-3), real_point(sqrt(real(2))/2,sqrt(real(2))/2));
	sl1 = straight_line(real_point(-4,0), real_point(4,0));
	fsc = free_space_cell<straight_line, straight_line>(&sl1, &sl2, 1);

	result = fsc.control_points(fsc.segment_x, fsc.segment_y, -2, 4, false);
	expected.clear(); expected.append(-2); expected.append(-sqrt(real(2))); expected.append(-1); expected.append(0); expected.append(sqrt(real(2))); expected.append(4);
	CHECK(compare<real>(result, expected));

	result = fsc.control_points(fsc.segment_x, fsc.segment_y, 1, 4, false);
	expected.clear(); expected.append(1); expected.append(sqrt(real(2))); expected.append(4);
	CHECK(compare<real>(result, expected));

	//----------------------------------------------------------------------------------------------------------------------------//
	// parallel lines

	sl1 = straight_line(real_point(-15,0), real_point(20,0));
	sl2 = straight_line(real_point(-10,1), real_point(10,1));
	fsc = free_space_cell<straight_line, straight_line>(&sl1, &sl2, 1);

	result = fsc.control_points(fsc.segment_x, fsc.segment_y, -9, 20, false);
	expected.clear(); expected.append(-9); expected.append(10); expected.append(20);
	CHECK(compare<real>(result, expected));

	result = fsc.control_points(fsc.segment_x, fsc.segment_y, -15, 20, false);
	expected.clear(); expected.append(-15); expected.append(-10); expected.append(10); expected.append(20);
	CHECK(compare<real>(result, expected));
}
*/

//TEST(check_intersection_x) {
//
//	//========================================================================================================================
//
//	// special case, should be 0
//	straight_line sl2 = straight_line(real_point(-1,-1), real_point(2,2));
//	straight_line sl1 = straight_line(real_point(-1.4,0), real_point(4,0));
//	free_space_cell<straight_line, straight_line> fsc = free_space_cell<straight_line, straight_line>(&sl1, &sl2, 1);
//	list<real> lst = fsc.control_points<straight_line, straight_line>(fsc.segment_x, fsc.segment_y, fsc.segment_x->min_t(), fsc.segment_x->max_t(), true);
//	int i = 0;
//	//list<interval> intv_p = fsc.y_intersections_int(lst.contents(lst[i]));
//	list<interval> intv_p = fsc.intersection_int<straight_line, straight_line>(fsc.segment_x, lst.contents(lst[i]), fsc.segment_y);
//	
//	bool rp_; real extreme_point = -1;  
//	
//	two_tuple<real, side> check_point = two_tuple<real, side>(-0.9, DOWN);
//	//int result = fsc.check_intersection_x(intv_p, lst.pop_front(), lst, 1, check_point, rp_, extreme_point);
//	int result = fsc.check_intersection<straight_line, straight_line>(fsc.segment_x, fsc.segment_y, intv_p, lst.pop_front(), lst, 1, check_point, rp_, extreme_point);
//	CHECK(result == 1);
//
//	// x axis just touches the bottom half of obstacle
//	check_point = two_tuple<real, side>(-1, DOWN);
//	//result = fsc.check_intersection_x(intv_p, lst.pop_front(), lst, 1, check_point, rp_, extreme_point);
//	result = fsc.check_intersection<straight_line, straight_line>(fsc.segment_x, fsc.segment_y, intv_p, lst.pop_front(), lst, 1, check_point, rp_, extreme_point);
//	CHECK(result == 0);
//
//	//========================================================================================================================
//
//	sl2 = straight_line(real_point(-0.99,-0.99), real_point(2,2));
//	sl1 = straight_line(real_point(-1.4,0), real_point(4,0));
//	fsc = free_space_cell<straight_line, straight_line>(&sl1, &sl2, 1);
//	lst = fsc.control_points<straight_line, straight_line>(fsc.segment_x, fsc.segment_y, fsc.segment_x->min_t(), fsc.segment_x->max_t(), true);
//	//intv_p = fsc.y_intersections_int(lst.contents(lst[i]));
//	intv_p = fsc.intersection_int<straight_line, straight_line>(fsc.segment_x, lst.contents(lst[i]), fsc.segment_y);
//
//	// x axis intersects the obstacle
//	check_point = two_tuple<real, side>(-0.98, DOWN);
//	//result = fsc.check_intersection_x(intv_p, lst.pop_front(), lst, 1, check_point, rp_, extreme_point);
//	result = fsc.check_intersection<straight_line, straight_line>(fsc.segment_x, fsc.segment_y,intv_p, lst.pop_front(), lst, 1, check_point, rp_, extreme_point);
//	CHECK(result == 1);
//
//	check_point = two_tuple<real, side>(-0.99, DOWN);
//	//result = fsc.check_intersection_x(intv_p, lst.pop_front(), lst, 1, check_point, rp_, extreme_point);
//	result = fsc.check_intersection<straight_line, straight_line>(fsc.segment_x, fsc.segment_y, intv_p, lst.pop_front(), lst, 1, check_point, rp_, extreme_point);
//	CHECK(result == 1);
//
//	//========================================================================================================================
//
//	sl2 = straight_line(real_point(-2,-2), real_point(0.99,0.99));
//	sl1 = straight_line(real_point(-1.4,0), real_point(4,0));
//	fsc = free_space_cell<straight_line, straight_line>(&sl1, &sl2, 1);
//	lst = fsc.control_points<straight_line, straight_line>(fsc.segment_x, fsc.segment_y, fsc.segment_x->min_t(), fsc.segment_x->max_t(), true);
//	//intv_p = fsc.y_intersections_int(lst.contents(lst[i]));
//	intv_p = fsc.intersection_int<straight_line, straight_line>(fsc.segment_x, lst.contents(lst[i]), fsc.segment_y);
//
//	// x axis intersects the obstacle
//	check_point = two_tuple<real, side>(0.98, UP);
//	//result = fsc.check_intersection_x(intv_p, lst.pop_front(), lst, 1, check_point, rp_, extreme_point);
//	result = fsc.check_intersection<straight_line, straight_line>(fsc.segment_x, fsc.segment_y,intv_p, lst.pop_front(), lst, 1, check_point, rp_, extreme_point);
//	CHECK(result == 1);
//
//	check_point = two_tuple<real, side>(0.99, UP);
//	//result = fsc.check_intersection_x(intv_p, lst.pop_front(), lst, 1, check_point, rp_, extreme_point);
//	result = fsc.check_intersection<straight_line, straight_line>(fsc.segment_x, fsc.segment_y, intv_p, lst.pop_front(), lst, 1, check_point, rp_, extreme_point);
//	CHECK(result == 1);
//
//	//========================================================================================================================
//	// check right points
//
//	sl2 = straight_line(real_point(-30,1), real_point(10,1));
//	sl1 = straight_line(real_point(-20,0), real_point(20,0));
//	fsc = free_space_cell<straight_line, straight_line>(&sl1, &sl2, 2);
//	lst = fsc.control_points<straight_line, straight_line>(fsc.segment_x, fsc.segment_y, fsc.segment_x->min_t(), fsc.segment_x->max_t(), true);
//	//intv_p = fsc.y_intersections_int(lst.contents(lst[i]));
//	intv_p = fsc.intersection_int<straight_line, straight_line>(fsc.segment_x, lst.contents(lst[i]), fsc.segment_y);
//
//	check_point = two_tuple<real, side>(0, UP);
//
//	result = fsc.check_intersection<straight_line, straight_line>(fsc.segment_x, fsc.segment_y, intv_p, lst.pop_front(), lst, 1, check_point, rp_, extreme_point);
//	CHECK(result == 1);
//	CHECK(rp_  && (extreme_point == (10 + sqrt(real(3)))));
//
//
//	fsc = free_space_cell<straight_line, straight_line>(&sl1, &sl2, 1);
//	lst = fsc.control_points<straight_line, straight_line>(fsc.segment_x, fsc.segment_y, fsc.segment_x->min_t(), fsc.segment_x->max_t(), true);
//	//intv_p = fsc.y_intersections_int(lst.contents(lst[i]));
//	intv_p = fsc.intersection_int<straight_line, straight_line>(fsc.segment_x, lst.contents(lst[i]), fsc.segment_y);
//	check_point = two_tuple<real, side>(0, UP);
//	try {
//		result = fsc.check_intersection<straight_line, straight_line>(fsc.segment_x, fsc.segment_y, intv_p, lst.pop_front(), lst, 1, check_point, rp_, extreme_point);
//	} catch (unfollowable_interval& e) {
//		CHECK(true);
//	} catch (...) {
//		CHECK(false);
//	}
//}

TEST(adjacent_opposite) {

	bool r1e, r2e;
	real p1, r1, r2;


	circular_line cln6 = circular_line(2, real_point(0,1), real_point(-2,2), 1);
	straight_line sl2 = straight_line(real_point(-1,0), real_point(1,0));
	free_space_cell<circular_line, straight_line> fs_sc = free_space_cell<circular_line, straight_line>(&sl2, &cln6, 3);

	p1 = -0.9;

	circular_line cln7 = circular_line(1, real_point(0,0), real_point(-1,1), 1);
	fs_sc = free_space_cell<circular_line, straight_line>(&sl2, &cln7, 1);

	fs_sc.left2top(p1, r1e, r1, r2e, r2);

	CHECK(r1e && r1 == 0);
	CHECK(!r2e);

	fs_sc.left2right(p1, r1e, r1, r2e, r2);

	CHECK(r1e && r1 == 0.5);
	CHECK(!r2e);
}