#include "stdafx.h"

#include "C:\Program Files\UnitTest++\src\UnitTest++.h"
#include "../FrechetDistanceImplementation/circular_line.h"


TEST(circle_intersection)
{
	circular_line cl; list<real> res;

	cl = circular_line(1, real_point(-6,-6),real_point(-1,1), 1);
	res = cl.circle_intersection(real_point(-4, -5), 2);
	CHECK(res.size() == 1);
	CHECK(res.front() == 0);

	res = cl.circle_intersection(real_point(-5, -6), 2);
	CHECK(res.size() == 2);
	CHECK(res.pop() == -1);
	CHECK(res.pop() == -1);


	cl = circular_line(1, real_point(-6,-6),real_point(1,-1), 1);
	res = cl.circle_intersection(real_point(-5, -6), 2);
	CHECK(res.size() == 2);
	CHECK(res.pop() == 1);
	CHECK(res.pop() == 1);
	
	cl = circular_line(1, real_point(-6,-6),real_point(-1,1), 1);
	res = cl.circle_intersection(real_point(-5, -5), 1);
	CHECK(res.size() == 2);
	CHECK(res.pop() == 0);
	CHECK(res.pop() == 1);

	cl = circular_line(1, real_point(-6,-6),real_point(1,-1), 1);
	res = cl.circle_intersection(real_point(-5, -5), 1);
	CHECK(res.size() == 2);
	CHECK(res.pop() == 0);
	CHECK(res.pop() == -1);


	cl = circular_line(1, real_point(-6,-6),real_point(1,-1), 1);
	res = cl.circle_intersection(real_point(-6, -6), 1);
	CHECK(res.size() == 0);
}